var app = angular.module('omnitechApp', ['localytics.directives']);

app.controller('savingsCtrl', function ($scope, $http) {
    $scope.calcId = "";
    $scope.products = [];
    $scope.calculater = {};
    $scope.variables = [];
    $scope.scope = {};
    $scope.selectedproducts = [];

    loadProducts();

    $scope.$watch('calcId',function () {
        console.log($scope.calcId);
        loadAvailableVariables($scope.calcId)
    });

    $scope.addRow = function () {
        $scope.variables.push({'text':$scope.text,'variable':$scope.variable,'type':$scope.type,
                                'formular':$scope.formular,'orderOfDisplay':$scope.orderOfDisplay,'selected':$scope.selected});
        $scope.text = '';
        $scope.variable = '';
        $scope.type = '';
        $scope.formular = '';
        $scope.orderOfDisplay = '';
        $scope.trid = '';
        $scope.selected = false;
    }

    $scope.removeRow = function (variable) {
        debugger;
        $scope.variables = $scope.variables.filter(function (i) {
            return i.variable != variable;
        })
    }

    $scope.testCalculator = function () {
        debugger;
        var inputs = $scope.variables.filter(getInputs);
        inputs.forEach(function (entry) {
            $scope.scope[entry.variable] = entry.answer;
        });
        var outputs = $scope.variables.filter(getOuputs);
        debugger;
        outputs.forEach(function (entry) {
            entry.answer = math.eval(entry.formular,$scope.scope);
        });
        console.log("finished testing");
    }

    $scope.saveCalculator = function () {
        debugger;
        $scope.calculater.variables = $scope.variables;
        $scope.calculater.products = $("#products").val();
        console.log($scope.calculater);
        var url = omnitechBase+'/savingsCalculator/saveCalculator';
        return $http.post(url,$scope.calculater)
            .success(function (data,status,headers,config) {
                debugger;
                window.location =  omnitechBase+'/savingsCalculator/index'
            }).error(function (data, status, headers, config) {
                window.alert('Error: Unable to save calculator,Please check your fields');
            });
    }

     function loadProducts() {
        var url = omnitechBase+'/savingsCalculator/loadProducts';
        return $http.get(url).then(function (res) {
            $scope.products = res.data;
        });
    }

    function getInputs(variable) {
        return variable.hasOwnProperty('answer');
    }
    function getOuputs(variable) {
        return (variable.type === 'output');
    }

    function loadAvailableVariables(calcId) {
        var url = omnitechBase+'/savingsCalculator/loadVariables?id='+calcId;
        return $http.get(url).then(function (res) {
            if(res.data.code){
                console.log(res.data)
            }else{
                $scope.variables = res.data.variables;
                $scope.calculater = res.data;
            }
        });
    }

});