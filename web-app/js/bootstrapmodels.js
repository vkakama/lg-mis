/**
 * Created by kakavi on 4/6/2016.
 */
$(document).ready(function(){
    $('.btn-delete').click(function(){
        var itemId = $(this).data('delete-id');
        var itemName = $(this).data('field-name');
        var itemTitle = $(this).data('field-title');
        $('#field-item-id').val(itemId);
        $('#comment-box').text(itemName);
        $('#deleteModalLabel').text(itemTitle);
    });
    $('.select-question').click(function () {
        var selected = $(this).text();
        //alert(selected);
        $('#selected-columns').prepend($('<div class="col-md-12"><div class="col-md-8">' +
            '<a href="javascript:void(0)" style="color:#333333;border-bottom: dashed 1px;">'+selected+'</a></div>' +
            '<div class="col-md-4">' +
            '<a href="javascript:void(0)" style="color:#333333;" title="Change to aggregate Column"><i class="glyphicon glyphicon-plus-sign"/> </a> '+
            '<a href="javascript:void(0)" style="color:#333333;" title="Remove Column"><i class="glyphicon glyphicon-remove"/> </a> '+
            '</div></div> '));

    });
});
