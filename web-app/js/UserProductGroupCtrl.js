var app = angular.module('omnitechApp', []);

app.controller('userProductGroupCtrl', function ($scope, $http) {
    $scope.grpId = "";
    $scope.products = [];
    $scope.productPrices = [];
    $scope.selectedProduct = {};
    $scope.userProductGroup = {};

    loadProducts();

    $scope.$watch('grpId',function () {
        debugger
        console.log($scope.grpId);
        init($scope.grpId)
    });

     function init(groupId) {
         var url = omnitechBase+'/userProductGroup/loadUserproductGroup?id='+groupId;
         return $http.get(url).then(function (res) {
             debugger;
             if(res.data.code){
                 console.log(res.data)
             }else{
                 $scope.productPrices = res.data.productPrices;
                 //set -1 prices to empty
                 $scope.productPrices.forEach(function (d) {
                    if(d.retailPrice == -1)d.retailPrice ='';
                    if(d.wholeSalePrice == -1)d.wholeSalePrice = '';
                 } );
                 $scope.userProductGroup = res.data;
             }
         });
    }

    function loadProducts() {
        debugger;
        var url = omnitechBase+'/userProductGroup/loadProducts';
        return $http.get(url).then(function (res) {
            $scope.products = res.data;
        });
    }

    $scope.addRow = function () {
        //TO DO check that you are not adding a duplicate price
        if(!$scope.wholeSalePrice){
            $scope.wholeSalePrice = '';
        }
        if(!$scope.retailPrice){
            $scope.retailPrice = '';
        }
        $scope.productPrices.push({'name':$scope.userProductGroup.name,'product':$scope.selectedProduct,'wholeSalePrice':$scope.wholeSalePrice,'retailPrice':$scope.retailPrice});
        $scope.wholeSalePrice = '';
        $scope.retailPrice = '';
    }

    $scope.removeRow = function (prodId) {
        $scope.productPrices = $scope.productPrices.filter(function (i) {
            return i.product.id != prodId;
        })
    }

    $scope.saveUserProductGroup = function () {
        debugger;
        $scope.userProductGroup.groupProductPrices = $scope.productPrices;
        $scope.userProductGroup.users = $("#users").val();
        $scope.userProductGroup.branches = $("#branches").val();
        $scope.userProductGroup.tags = $("#tags").val();
        console.log($scope.userProductGroup);
        var url = omnitechBase+'/userProductGroup/saveUserProductGroup';
        return $http.post(url,$scope.userProductGroup)
            .success(function (data,status,headers,config) {
                debugger;
                window.location =  omnitechBase+'/userProductGroup/index'
            }).error(function (data, status, headers, config) {
                window.alert('Error: Unable to save User Product Group,Please check your fields');
            });
    }

    $scope.updatePrices = function () {
         debugger;
        $scope.wholeSalePrice = $scope.selectedProduct.wholeSalePrice;
        $scope.retailPrice = $scope.selectedProduct.retailPrice;
    }

});