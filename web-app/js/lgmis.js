d3.select("#btnProduct")
    .on("click", function () {
        d3.select("#product-container").style("display", "block");
        d3.select("#faq-container").style("display", "none");


        // how do I get the charts to reset all of their filters and transition during the redraw?
        if ((productChart.filters().length |
                productCategoryChart.filters().length |
                productBranchChart.filters().length |
                productFilterTimeLineChart.filters().length |
                productTimeLineChart.filters().length) > 0) {
            dc.redrawAll("product");
        }
        else {
            dc.renderAll("product");
        }
    });

d3.select("#btnFaq")
    .on("click", function () {
        d3.select("#faq-container").style("display", "block");
        d3.select("#product-container").style("display", "none");

        var questionChart = dc.rowChart("#question-chart-row", "faq");
        var qnCategoryChart = dc.pieChart("#question-chart-pie", "faq");
        var qnBranchChart = dc.barChart("#question-chart-bar", "faq");
        var qnTimeLineChart = dc.lineChart("#question-chart-line", "faq");
        var qnFilterTimeLineChart = dc.barChart("#question-filter-chart-line", "faq");


        var dateFormat = d3.time.format("%Y-%m-%dT%H:%M:%SZ").parse;
        var dateFormat2 = d3.time.format("%a %e %b %Y %I:%M");

        var dataURL = omnitechBase + '/home/queryFaqData';
        console.log(dataURL)
        d3.json(dataURL, function (error, data){

            if (data.length === 0) {
                console.log("No data available")
            }

            data.forEach(function (d) {
                d.branch = d.branch;
                d.question = d.question;
                d.category = d.category;
                d.count = +d.count;
                d.date = dateFormat(d.date);

            });
            var today = d3.time.day.offset(new Date(),1)
            // set crossfilter
            var ndx = crossfilter(data), dateDim,
                questionDim = ndx.dimension(function (d) {
                    return d.question;
                }),
                categoryDim = ndx.dimension(function (d) {
                    return d.category
                }),
                branchDim = ndx.dimension(function (d) {
                    return d.branch
                }),
                cntPerQuestion = remove_empty_bins(questionDim.group().reduceSum(function (d) {
                    return +d.count
                })),
                cntPerCategory = remove_empty_bins(categoryDim.group().reduceSum(function (d) {
                    return +d.count
                })),
                cntPerBranch = remove_empty_bins(branchDim.group().reduceSum(function (d) {
                    return +d.count;
                }));

            var rowTip = d3.tip()
                .attr("class", 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<span> Question : " + d.key + "</span> <br /> No. of Logs  :  " + d.value;
                })

            var barTip = d3.tip()
                .attr("class", 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<span> Branch : " + d.data.key + "</span> <br /> No. of Logs  :  " + d.data.value;
                })

            var pieTip = d3.tip()
                .attr("class", 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<span> Category : " + d.data.key + "</span> <br /> No. of Logs  :  " + d.value;
                });

            var timeTip = d3.tip()
                .attr("class", 'd3-tip')
                .offset([-10, 0])
                .html(function (d) {
                    return "<span> On " + dateFormat2(d.data.key) + "</span> <br /> No. of Logs collected : " + d.data.value;
                });
            /*===========TiME LINE ACTIVITY CHAT===========*/


            var intervals = {
                Days: d3.time.day,
                Weeks: d3.time.week,
                Months: d3.time.month
            };

            var initialSelection = 'Days';
            d3.select('.question-interval').selectAll('option')
                .data(Object.keys(intervals))
                .enter().append('option')
                .text(function (d) {
                    return d;
                })
                .attr('selected', function (d) {
                    return d === initialSelection ? '' : null;
                });

            function setup() {
                if (dateDim) {
                    dateDim.dispose();
                    group.dispose();
                }
                var interval = intervals[d3.select('.question-interval')[0][0].value];
                dateDim = ndx.dimension(function (d) {
                    return interval(d.date);
                });
                qnTimeLineChart.xUnits(interval.range)
                    .round(interval.round);
                qnFilterTimeLineChart.xUnits(interval.range)
                    .round(interval.round);
                group = dateDim
                    .group().reduceSum(function (d) {
                        return +d.count;
                    });
                qnTimeLineChart.dimension(dateDim).group(group, "Number of Records", function (d) {
                    return d.value;
                })
                    .render();
                qnFilterTimeLineChart.dimension(dateDim).group(group)
                    .render();

            }


            qnTimeLineChart
                .width(768)
                .height(250)
                .ordinalColors(["#56B2EA", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"])
                .renderArea(true)
                // .mouseZoomable(true)
                .rangeChart(qnFilterTimeLineChart)
                .x(d3.time.scale().domain([new Date(2017, 7, 1),today]))
                .xUnits(d3.time.day)
                .margins({left: 50, top: 20, right: 0, bottom: 20})
                .renderHorizontalGridLines(true)
                .elasticY(true)
                .brushOn(false)
                .clipPadding(10)
                .title(function (d) {
                    return ""
                });
            qnTimeLineChart.yAxis();


            qnFilterTimeLineChart
                .margins({top: 10, right: 0, bottom: 20, left: 50})
                .width(768)
                .height(70)
                .ordinalColors(["#56B2EA", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"])
                .x(d3.time.scale().domain([new Date(2017, 7, 1), today]))
                .xUnits(d3.time.day)
                .gap(10)
                .elasticY(true)
                .alwaysUseRounding(true);
            d3.select('.question-interval').on('change', function () {
                setup();
            });
            setup();

            /*===========PRODUCT ROW CHAT===========*/
            questionChart
                .width(350)
                .height(1000)
                .margins({top: 20, left: 10, right: 10, bottom: 20})
                .transitionDuration(750)
                .dimension(questionDim)
                .group(cntPerQuestion)
                .renderLabel(true)
                .gap(5)
                .title(function (d) {
                    return "";
                })
                .elasticX(true);

            /*===========BRANCH PIE CHAT===========*/

            qnCategoryChart.transitionDuration(750)
                .width(300)
                .height(200)
                .dimension(categoryDim)
                .group(cntPerCategory)
                .innerRadius(60)
                .title(function (d) {
                    return "";
                })
                .legend(dc.legend().x(270).y(0).itemHeight(15).gap(5))
                .renderLabel(false);


            /*===========CATEGORY BAR CHAT===========*/
            qnBranchChart
                .transitionDuration(750)
                .width(750)
                .height(300)
                .ordinalColors(["#56B2EA", "#E064CD", "#F8B700", "#78CC00", "#7B71C5"])
                .dimension(branchDim)
                .group(cntPerBranch)
                .margins({top: 10, right: 10, bottom: 30, left: 40})
                .barPadding(0.1)
                .outerPadding(0.05)
                .title(function (d) {
                    return "";
                })
                .elasticX(true);


            qnBranchChart
                .x(d3.scale.ordinal())
                .xUnits(dc.units.ordinal)
                .elasticX(true)
                .xAxisLabel('Branch')
                .yAxisLabel('Views Per Branch')
                .alwaysUseRounding(true)
                .renderHorizontalGridLines(true)
                .renderVerticalGridLines(true)
                .elasticY(true);


            // To blank the the values entirely, though not the actual ticks marks themselves
            qnBranchChart.xAxis().tickFormat(function (v) {
                var length = cntPerCategory.all().length;
                return length < 60 ? v : "";
            });
            qnBranchChart.renderlet(function (chart) {
                chart.selectAll("g.x text").attr('dx', '-12').attr(
                    'dy', '0').attr('transform', "rotate(-45)").attr("text-anchor", "start");
            })

            qnBranchChart.renderlet(function (chart) {
                chart.selectAll("g text.x-axis-label").attr('dx', '-12').attr(
                    'dy', '10');
            });

            questionChart.on('renderlet', function (chart) {
                chart.selectAll("g.row")
                    .call(rowTip)
                    .on('mouseover', rowTip.show)
                    .on('mouseout', rowTip.hide)
            });
            qnBranchChart.on('pretransition', function (chart) {
                chart.selectAll('.bar')
                    .call(barTip)
                    .on('mouseover', barTip.show)
                    .on('mouseout', barTip.hide)
            });
            qnCategoryChart.on('pretransition', function (chart) {
                chart.selectAll('g.pie-slice')
                    .call(pieTip)
                    .on('mouseover', pieTip.show)
                    .on('mouseout', pieTip.hide)
            });
            qnTimeLineChart.on('pretransition', function (chart) {
                chart.selectAll('.dot')
                    .call(timeTip)
                    .on('mouseover.something', timeTip.show)
                    .on('mouseout.something', timeTip.hide)
            });


            dc.renderAll("faq");

            //REACT TO THE RESET ALL BUTTON
            d3.select('#question-refresh-all').on('click', function () {
                questionChart.filterAll("faq");
                qnCategoryChart.filterAll("faq");
                qnTimeLineChart.filterAll("faq");
                qnBranchChart.filterAll("faq");
                qnFilterTimeLineChart.filterAll("faq");
                dc.redrawAll("faq");
            });
        });


        function trunc(s, num) {
            if (!s) return "";
            var temp = '' + s;
            if (num >= temp.length)
                return temp;
            return temp.substr(0, num - 3) + '...';
        }

        var fnFixAxisItem = function (chart) {
            // rotate x-axis labels
            chart.select('g').attr('transform', 'translate(20,0)');
            chart.selectAll('g.x text').attr('transform', ' rotate(340)');
        };


        function remove_empty_bins(source_group) {
            return {
                all: function () {
                    return source_group.all().filter(function (d) {
                        return d.value != 0;
                    });
                }
            };
        }

        // use workaround to check for # of filters present & either redrawAll or renderAll
        if ((questionChart.filters().length |
                qnCategoryChart.filters().length |
                qnBranchChart.filters().length |
                qnFilterTimeLineChart.filters().length |
                qnTimeLineChart.filters().length) > 0) {
            dc.redrawAll("faq");
        }
        else {
            dc.renderAll("faq");
        }
    });