var districts = [
		{
			"district": "Buikwe",
			"region": "Central"
		},
		{
			"district": "Bukomansimbi",
			"region": "Central"
		},
		{
			"district": "Butambala",
			"region": "Central"
		},
		{
			"district": "Buvuma",
			"region": "Central"
		},
		{
			"district": "Gomba",
			"region": "Central"
		},
		{
			"district": "Kalangala",
			"region": "Central"
		},
		{
			"district": "Kalungu",
			"region": "Central"
		},
		{
			"district": "Kampala",
			"region": "Central"
		},
		{
			"district": "Kayunga",
			"region": "Central"
		},
		{
			"district": "Kiboga",
			"region": "Central"
		},
		{
			"district": "Kyankwanzi",
			"region": "Central"
		},
		{
			"district": "Luweero",
			"region": "Central"
		},
		{
			"district": "Lwengo",
			"region": "Central"
		},
		{
			"district": "Lyantonde",
			"region": "Central"
		},
		{
			"district": "Masaka",
			"region": "Central"
		},
		{
			"district": "Mityana",
			"region": "Central"
		},
		{
			"district": "Mpigi",
			"region": "Central"
		},
		{
			"district": "Mubende",
			"region": "Central"
		},
		{
			"district": "Mukono",
			"region": "Central"
		},
		{
			"district": "Nakaseke",
			"region": "Central"
		},
		{
			"district": "Nakasongola",
			"region": "Central"
		},
		{
			"district": "Rakai",
			"region": "Central"
		},
		{
			"district": "Ssembabule",
			"region": "Central"
		},
		{
			"district": "Wakiso",
			"region": "Central"
		},
		{
			"district": "Amuria",
			"region": "Eastern"
		},
		{
			"district": "Budaka",
			"region": "Eastern"
		},
		{
			"district": "Bududa",
			"region": "Eastern"
		},
		{
			"district": "Bugiri",
			"region": "Eastern"
		},
		{
			"district": "Bukedea",
			"region": "Eastern"
		},
		{
			"district": "Bukwa",
			"region": "Eastern"
		},
		{
			"district": "Bulambuli",
			"region": "Eastern"
		},
		{
			"district": "Busia",
			"region": "Eastern"
		},
		{
			"district": "Butaleja",
			"region": "Eastern"
		},
		{
			"district": "Buyende",
			"region": "Eastern"
		},
		{
			"district": "Iganga",
			"region": "Eastern"
		},
		{
			"district": "Jinja",
			"region": "Eastern"
		},
		{
			"district": "Kaberamaido",
			"region": "Eastern"
		},
		{
			"district": "Kaliro",
			"region": "Eastern"
		},
		{
			"district": "Kamuli",
			"region": "Eastern"
		},
		{
			"district": "Kapchorwa",
			"region": "Eastern"
		},
		{
			"district": "Katakwi",
			"region": "Eastern"
		},
		{
			"district": "Kibuku",
			"region": "Eastern"
		},
		{
			"district": "Kumi",
			"region": "Eastern"
		},
		{
			"district": "Kween",
			"region": "Eastern"
		},
		{
			"district": "Luuka",
			"region": "Eastern"
		},
		{
			"district": "Manafwa",
			"region": "Eastern"
		},
		{
			"district": "Mayuge",
			"region": "Eastern"
		},
		{
			"district": "Mbale",
			"region": "Eastern"
		},
		{
			"district": "Namayingo",
			"region": "Eastern"
		},
		{
			"district": "Namutumba",
			"region": "Eastern"
		},
		{
			"district": "Ngora",
			"region": "Eastern"
		},
		{
			"district": "Pallisa",
			"region": "Eastern"
		},
		{
			"district": "Serere",
			"region": "Eastern"
		},
		{
			"district": "Sironko",
			"region": "Eastern"
		},
		{
			"district": "Soroti",
			"region": "Eastern"
		},
		{
			"district": "Tororo",
			"region": "Eastern"
		},
		{
			"district": "Abim",
			"region": "Northern"
		},
		{
			"district": "Adjumani",
			"region": "Northern"
		},
		{
			"district": "Agago",
			"region": "Northern"
		},
		{
			"district": "Alebtong",
			"region": "Northern"
		},
		{
			"district": "Amolatar",
			"region": "Northern"
		},
		{
			"district": "Amudat",
			"region": "Northern"
		},
		{
			"district": "Amuru",
			"region": "Northern"
		},
		{
			"district": "Apac",
			"region": "Northern"
		},
		{
			"district": "Arua",
			"region": "Northern"
		},
		{
			"district": "Dokolo",
			"region": "Northern"
		},
		{
			"district": "Gulu",
			"region": "Northern"
		},
		{
			"district": "Kaabong",
			"region": "Northern"
		},
		{
			"district": "Kitgum",
			"region": "Northern"
		},
		{
			"district": "Koboko",
			"region": "Northern"
		},
		{
			"district": "Kole",
			"region": "Northern"
		},
		{
			"district": "Kotido",
			"region": "Northern"
		},
		{
			"district": "Lamwo",
			"region": "Northern"
		},
		{
			"district": "Lira",
			"region": "Northern"
		},
		{
			"district": "Maracha",
			"region": "Northern"
		},
		{
			"district": "Moroto",
			"region": "Northern"
		},
		{
			"district": "Moyo",
			"region": "Northern"
		},
		{
			"district": "Nakapiripirit",
			"region": "Northern"
		},
		{
			"district": "Napak",
			"region": "Northern"
		},
		{
			"district": "Nebbi",
			"region": "Northern"
		},
		{
			"district": "Nwoya",
			"region": "Northern"
		},
		{
			"district": "Otuke",
			"region": "Northern"
		},
		{
			"district": "Oyam",
			"region": "Northern"
		},
		{
			"district": "Pader",
			"region": "Northern"
		},
		{
			"district": "Yumbe",
			"region": "Northern"
		},
		{
			"district": "Zombo",
			"region": "Northern"
		},
		{
			"district": "Buhweju",
			"region": "Western"
		},
		{
			"district": "Buliisa",
			"region": "Western"
		},
		{
			"district": "Bundibugyo",
			"region": "Western"
		},
		{
			"district": "Bushenyi",
			"region": "Western"
		},
		{
			"district": "Hoima",
			"region": "Western"
		},
		{
			"district": "Ibanda",
			"region": "Western"
		},
		{
			"district": "Isingiro",
			"region": "Western"
		},
		{
			"district": "Kabale",
			"region": "Western"
		},
		{
			"district": "Kabarole",
			"region": "Western"
		},
		{
			"district": "Kamwenge",
			"region": "Western"
		},
		{
			"district": "Kanungu",
			"region": "Western"
		},
		{
			"district": "Kasese",
			"region": "Western"
		},
		{
			"district": "Kibaale",
			"region": "Western"
		},
		{
			"district": "Kiruhura",
			"region": "Western"
		},
		{
			"district": "Kiryandongo",
			"region": "Western"
		},
		{
			"district": "Kisoro",
			"region": "Western"
		},
		{
			"district": "Kyegegwa",
			"region": "Western"
		},
		{
			"district": "Kyenjojo",
			"region": "Western"
		},
		{
			"district": "Masindi",
			"region": "Western"
		},
		{
			"district": "Mbarara",
			"region": "Western"
		},
		{
			"district": "Mitooma",
			"region": "Western"
		},
		{
			"district": "Ntoroko",
			"region": "Western"
		},
		{
			"district": "Ntungamo",
			"region": "Western"
		},
		{
			"district": "Rubirizi",
			"region": "Western"
		},
		{
			"district": "Rukungiri",
			"region": "Western"
		},
		{
			"district": "Sheema",
			"region": "Western"
		},
		{
			"district": ""
		}];