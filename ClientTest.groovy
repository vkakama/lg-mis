import com.omnitech.lgprotobuf.ClientRequest
import com.omnitech.lgprotobuf.ServerResponse
import groovyx.net.http.HTTPBuilder

import static groovyx.net.http.ContentType.BINARY

@Grapes(
        [
                @Grab('com.omnitech.lg:mobile-protobuf:0.3-RC10-SNAPSHOT'),
                @Grab(group = 'org.codehaus.gpars', module = 'gpars', version = '1.2.1'),
                @Grab(group = 'org.codehaus.groovy.modules.http-builder', module = 'http-builder', version = '0.7.1'),
                @GrabResolver(name = 'repo2', root = 'http://omnitech.co.ug/m2/releases/', m2Compatible = true),
                @GrabResolver(name = 'repo2', root = 'http://omnitech.co.ug/m2/snapshots/', m2Compatible = true)
        ]

)
public class ClientTest extends Script {

    @Override
    Object run() {
//        pullCategories()
//        pullFaqCategories()
//        pullEvents()
//        pullCalculators()
        pullResources()
    }

    def pullCategories() {
        def line = 'http://test.omnitech.co.ug:8080/lg-mis/wire/index'
        def local = 'http://localhost:8091/lg/wire/index'
        def http = new HTTPBuilder(local)

        def request = new ClientRequest.Builder().request(ClientRequest.RequestType.CATEGORY_LIST)
                .username('0789576387')
                .build()

        http.post(body: ClientRequest.ADAPTER.encode(request), requestContentType: BINARY) { response, input ->
            def categoryList = ServerResponse.ADAPTER.decode(input)
            println(categoryList)
        }
    }

    def pullFaqCategories(){
        def http = new HTTPBuilder('http://localhost:8090/lg/wire/index')

        def request = new ClientRequest.Builder().request(ClientRequest.RequestType.FAQ_LIST)
                .username('lg1')
                .build()

        http.post(body: ClientRequest.ADAPTER.encode(request), requestContentType: BINARY) { response, input ->
            def categoryList = ServerResponse.ADAPTER.decode(input)
            println(categoryList)
        }
    }

    def pullEvents(){
        def http = new HTTPBuilder('http://localhost:8091/lg/wire/index')

        def request = new ClientRequest.Builder().request(ClientRequest.RequestType.EVENT_LIST)
                .username('0789576387')
                .build()

        http.post(body: ClientRequest.ADAPTER.encode(request), requestContentType: BINARY) { response, input ->
            def eventList = ServerResponse.ADAPTER.decode(input)
            println(eventList)
        }
    }

    def pullResources(){
        def http = new HTTPBuilder('http://localhost:8091/lg/wire/index')

        def request = new ClientRequest.Builder().request(ClientRequest.RequestType.MEDIA_RESOURCE_LIST)
                .username('0789576387')
                .build()


        http.post(body: ClientRequest.ADAPTER.encode(request), requestContentType: BINARY) { response, input ->
            def resourcesList = ServerResponse.ADAPTER.decode(input)
            println(resourcesList)
        }
    }

    def pullCalculators(){
        def line = 'http://test.omnitech.co.ug:8080/lg-mis/wire/index'
        def local = 'http://localhost:8090/lg/wire/index'
        def http = new HTTPBuilder(line)

        def request = new ClientRequest.Builder().request(ClientRequest.RequestType.SAVING_CALCULATOR_LIST)
                .username('0778922055')
                .build()

        http.post(body: ClientRequest.ADAPTER.encode(request), requestContentType: BINARY) { response, input ->
            def calculator = ServerResponse.ADAPTER.decode(input)
            println(calculator)
        }
    }
}







