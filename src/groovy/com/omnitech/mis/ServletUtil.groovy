package com.omnitech.mis

import eu.bitwalker.useragentutils.Browser
import fuzzycsv.FuzzyCSVWriter
import org.apache.commons.io.FilenameUtils
import org.codehaus.groovy.grails.commons.GrailsApplication

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * Created by victor on 10/24/14.
 */
class ServletUtil {
    static FileNameMap fileNameMap
    static {
        fileNameMap = URLConnection.getFileNameMap();
    }

    public
    static void setAttachment(HttpServletResponse response, String attachmentName, HttpServletRequest request = null) {
        setHeaders(response, attachmentName, 0)
        response.setHeader("Cache-Control", "no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", -1);
        response.setHeader("Cache-Control", "no-store");

        def userAgent = request.getHeader('User-Agent')

        def encodedName = URLEncoder.encode(attachmentName, 'UTF-8')


        if (Browser.FIREFOX.isInUserAgentString(userAgent)) {
            response.setHeader("Content-Disposition", "attachment; filename=\"$attachmentName\"")
        } else if (Browser.IE.isInUserAgentString(userAgent)) {
            response.setHeader("Content-Disposition", "attachment; filename=" + attachmentName);
        } else if (Browser.CHROME.isInUserAgentString(userAgent)) {
            response.setHeader("Content-Disposition", "attachment; filename=" + attachmentName);
        } else {
            response.setHeader("Content-Disposition", """attachment; filename="$attachmentName"; filename*=UTF-8''$encodedName """);
        }

    }

    public static setHeaders(HttpServletResponse response, String attachmentName, long size = 0) {
        if (size) {
            response.setContentLength(size.intValue())
        }
        response.setHeader("Content-Type", getMimeType(attachmentName));
    }

    public static setHeaders(HttpServletResponse response, File file) {
        setHeaders(response, file.name, file.size())
        def lastModified = new Date(file.lastModified())
        response.setHeader('Last-Modified', lastModified.format('EEE, dd MMM yyyy HH:mm:ss z'))


    }
    static def GRAILS_MIME_TYPES = AppHolder.bean(GrailsApplication).config.grails.mime.types

    public static String getMimeType(String fileUrl) {
        String type = fileNameMap.getContentTypeFor(fileUrl);
        if (type)
            return type;

        final def defaultMime = "application/binary"

        def extension = FilenameUtils.getExtension(fileUrl)

        if (!extension) return defaultMime

        def types = GRAILS_MIME_TYPES[extension]

        if (!types) return defaultMime

        if (types instanceof List) return types.first()

        return types as String
    }


    static void exportCSV(HttpServletResponse response, String fileName, List data, HttpServletRequest request) {
        setAttachment(response, fileName, request)
        def cSVWriter = new FuzzyCSVWriter(response.writer)
        cSVWriter.writeAll(data)
    }

    static boolean hasFilters(Map params) {
        params?.any { it.key?.toString()?.startsWith('filter|') || it.key == 'dateFrom' || it.key == 'dateTo' }
    }
}
