package com.omnitech.mis

import java.util.concurrent.Executor
import java.util.concurrent.Executors

/**
 * Created by kay on 4/16/2015.
 */
class TaskUtil {

    static Executor qExecutor = Executors.newSingleThreadExecutor()

    static queue(Closure code) {
        qExecutor.execute {
            try {
                code()
            } catch (Exception x) {
                x.printStackTrace()
            }
        }
    }

}
