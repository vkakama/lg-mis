package com.omnitech.mis

import fuzzycsv.FuzzyCSV
import groovy.util.logging.Slf4j

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Slf4j
class DataExporter {

    static def exportToCSV(String fileName,String tableName,
                    HttpServletResponse response,
                    HttpServletRequest request) {
        ServletUtil.setAttachment(response, fileName, request)
        AppHolder.withMisSqlNonTx {
            AppHolder.makeStreaming(it)
            def writer = response.outputStream.newPrintWriter()
            FuzzyCSV.writeCsv(it, "SELECT * FROM $tableName", writer)
            writer.flush()
        }
        response.outputStream.flush()
    }

    static def exportToCsvByQuery(String fileName,String query,
                                  HttpServletResponse response,
                                  HttpServletRequest request){
        ServletUtil.setAttachment(response, fileName, request)
        AppHolder.withMisSqlNonTx {
            AppHolder.makeStreaming(it)
            def writer = response.outputStream.newPrintWriter()
            FuzzyCSV.writeCsv(it, query, writer)
            writer.flush()
        }
        response.outputStream.flush()

    }
}
