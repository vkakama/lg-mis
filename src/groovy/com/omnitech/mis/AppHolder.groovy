package com.omnitech.mis

import grails.util.Holders
import groovy.sql.Sql
import groovy.util.logging.Log4j
import org.springframework.beans.factory.support.AbstractBeanDefinition

import javax.sql.DataSource
import java.sql.ResultSet
import java.sql.Statement

/**
 * Created by kay on 4/16/2015.
 */
@Log4j
class AppHolder {

    private static DataSource _misDataSource
    private static DataSource _kengaDataSource
    private static DataSource _kengaDataUnproxied


    static def <T> T bean(Class<T> c) {
        return Holders.applicationContext.getBean(c)
    }

    static def bean(String name) {
        return Holders.applicationContext.getBean(name)
    }

    static <T> T autowire(T bean) {
        Holders.applicationContext.autowireCapableBeanFactory.autowireBeanProperties(bean, AbstractBeanDefinition.AUTOWIRE_BY_TYPE, false)
        return bean
    }

    static def <T> T withMisSql(@DelegatesTo(Sql) Closure<T> code) {
        def sql = new Sql(_misDataSource)
        try {
            code.delegate = sql
            return code(sql)
        } finally {
            TaskUtil.queue { sql.close() }
        }
    }

    static def makeStreaming(Sql sql) {
        sql.setResultSetHoldability(ResultSet.HOLD_CURSORS_OVER_COMMIT)
        sql.withStatement { Statement stmt -> stmt.setFetchSize(Integer.MIN_VALUE) }
    }

    static def <T> T withKengaSql(@DelegatesTo(Sql) Closure<T> code) {
        def sql = new Sql(_kengaDataSource)
        try {
            code.delegate = sql
            return code(sql)
        } finally {
            TaskUtil.queue { sql.close() }
        }
    }

    static def <T> T withMisSqlNonTx(@DelegatesTo(Sql) Closure<T> code) {
        def sql = getTransactionLessInstance()
        try {
            code.delegate = sql
            return code(sql)
        } finally {
            TaskUtil.queue { sql.close() }
        }
    }

    static def <T> T withKengaSqlNonTx(@DelegatesTo(Sql) Closure<T> code) {
        def sql = getTransactionLessKengaInstance()
        try {
            code.delegate = sql
            return code(sql)
        } finally {
            TaskUtil.queue { sql.close() }
        }
    }

    static void setMisDataSource(misDataSource) {
        _misDataSource = misDataSource
    }

    static void setKengaDataSource(kengaDataSource) {
        _kengaDataSource = kengaDataSource
    }


    private static def getTransactionLessInstance() {
        return new Sql((bean('dataSourceUnproxied') as DataSource).connection)
//        return Sql.newInstance("${Holders.grailsApplication.config.dataSource.url}",
//                "${Holders.grailsApplication.config.dataSource.username}",
//                "${Holders.grailsApplication.config.dataSource.password}", 'com.mysql.jdbc.Driver')
    }

    private static def getTransactionLessKengaInstance() {
        return new Sql((bean('dataSourceUnproxied_oxd') as DataSource).connection)

//        return Sql.newInstance("${Holders.grailsApplication.config.dataSource_oxd.url}",
//                "${Holders.grailsApplication.config.dataSource_oxd.username}",
//                "${Holders.grailsApplication.config.dataSource_oxd.password}", 'com.mysql.jdbc.Driver')
    }
}

