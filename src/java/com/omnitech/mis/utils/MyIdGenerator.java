package com.omnitech.mis.utils;

import org.hibernate.id.IdentifierGenerator;
import org.hibernate.engine.spi.SessionImplementor;

import java.io.Serializable;

/**
 * User: victor
 * Date: 8/7/14
 */
public class MyIdGenerator implements IdentifierGenerator {
    public Serializable generate(SessionImplementor session, Object object) {
        return java.util.UUID.randomUUID().toString().replace("-", "");
    }
}