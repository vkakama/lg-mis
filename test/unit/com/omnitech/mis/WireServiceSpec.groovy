package com.omnitech.mis

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(WireService)
class WireServiceSpec extends Specification {

    User user = new User(username: 'lg1',telephone: '0777505033',roles:[])

    def setup() {
    }

    def cleanup() {
    }

    void "test a product should appear to all users if has no group"() {
    }

    void "test a product should appear to all users if has no group with default price"() {
    }

    void "test a product should appear to all users if it belongs to a non exclusive group"() {
    }
}
