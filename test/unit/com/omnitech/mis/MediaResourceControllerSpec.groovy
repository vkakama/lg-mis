package com.omnitech.mis



import grails.test.mixin.*
import spock.lang.*

@TestFor(MediaResourceController)
@Mock(MediaResource)
class MediaResourceControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.mediaResourceInstanceList
            model.mediaResourceInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.mediaResourceInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def mediaResource = new MediaResource()
            mediaResource.validate()
            controller.save(mediaResource)

        then:"The create view is rendered again with the correct model"
            model.mediaResourceInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            mediaResource = new MediaResource(params)

            controller.save(mediaResource)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/mediaResource/show/1'
            controller.flash.message != null
            MediaResource.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def mediaResource = new MediaResource(params)
            controller.show(mediaResource)

        then:"A model is populated containing the domain instance"
            model.mediaResourceInstance == mediaResource
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def mediaResource = new MediaResource(params)
            controller.edit(mediaResource)

        then:"A model is populated containing the domain instance"
            model.mediaResourceInstance == mediaResource
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/mediaResource/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def mediaResource = new MediaResource()
            mediaResource.validate()
            controller.update(mediaResource)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.mediaResourceInstance == mediaResource

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            mediaResource = new MediaResource(params).save(flush: true)
            controller.update(mediaResource)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/mediaResource/show/$mediaResource.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/mediaResource/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def mediaResource = new MediaResource(params).save(flush: true)

        then:"It exists"
            MediaResource.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(mediaResource)

        then:"The instance is deleted"
            MediaResource.count() == 0
            response.redirectedUrl == '/mediaResource/index'
            flash.message != null
    }
}
