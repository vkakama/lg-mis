package org.codehaus.groovy.grails.plugins.orm.auditable



import grails.test.mixin.*
import spock.lang.*

@TestFor(AuditLogEventController)
@Mock(AuditLogEvent)
class AuditLogEventControllerSpec extends Specification {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.auditLogEventInstanceList
            model.auditLogEventInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.auditLogEventInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            def auditLogEvent = new AuditLogEvent()
            auditLogEvent.validate()
            controller.save(auditLogEvent)

        then:"The create view is rendered again with the correct model"
            model.auditLogEventInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            auditLogEvent = new AuditLogEvent(params)

            controller.save(auditLogEvent)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/auditLogEvent/show/1'
            controller.flash.message != null
            AuditLogEvent.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def auditLogEvent = new AuditLogEvent(params)
            controller.show(auditLogEvent)

        then:"A model is populated containing the domain instance"
            model.auditLogEventInstance == auditLogEvent
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def auditLogEvent = new AuditLogEvent(params)
            controller.edit(auditLogEvent)

        then:"A model is populated containing the domain instance"
            model.auditLogEventInstance == auditLogEvent
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/auditLogEvent/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def auditLogEvent = new AuditLogEvent()
            auditLogEvent.validate()
            controller.update(auditLogEvent)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.auditLogEventInstance == auditLogEvent

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            auditLogEvent = new AuditLogEvent(params).save(flush: true)
            controller.update(auditLogEvent)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/auditLogEvent/show/$auditLogEvent.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/auditLogEvent/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def auditLogEvent = new AuditLogEvent(params).save(flush: true)

        then:"It exists"
            AuditLogEvent.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(auditLogEvent)

        then:"The instance is deleted"
            AuditLogEvent.count() == 0
            response.redirectedUrl == '/auditLogEvent/index'
            flash.message != null
    }
}
