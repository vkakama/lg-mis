package com.omnitech.mis

import fuzzycsv.FuzzyCSV
import grails.transaction.Transactional

@Transactional
class UserService {


    def processContacts(String text) {
        def csv = FuzzyCSV.toListOfLists(FuzzyCSV.parseCsv(text))
        csv.eachWithIndex { user, index ->
            println user
            if (index != 0) {
                try {
                    def branch = Branch.findByBranchName(user.get(3))?:new Branch(branchName: user.get(3)).save(flush: true)
                    def role = Role.findByAuthority("ROLE_CHP")
                    def existingUser = User.findByUsername(user.get(2))
                    if(!existingUser){
                        def chp = new User(username: user.get(2), password: 'pass', name: user.get(4),
                                telephone: user.get(5), branch: branch).save(flush: true,failOnError: true)

                        UserRole.create(chp, role, true).save(flush: true)
                    }
                } catch (Exception ex) {
                    ex.printStackTrace()
                }
            }
        }

    }
}
