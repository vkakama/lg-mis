package com.omnitech.mis

import grails.transaction.Transactional

@Transactional
class UserPromotionGroupService {


    UserPromotionGroup addUsersToGroup(UserPromotionGroup userPromotionGroup,List<User> userList){
        userList.each{
            if(!userPromotionGroup.users.contains(it)){
                userPromotionGroup.users.add(it)
            }
        }
        return userPromotionGroup
    }
}
