package com.omnitech.mis

import grails.transaction.Transactional

@Transactional
class UserGroupService {

    Tag addUsersToGroup(Tag userGroup, List<User> userList){
        userList.each{
            if(!userGroup.users.contains(it)){
                userGroup.users.add(it)
            }
        }
        return userGroup
    }
}
