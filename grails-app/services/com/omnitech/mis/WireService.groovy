package com.omnitech.mis

import com.omnitech.lgprotobuf.*
import grails.transaction.Transactional
import groovy.json.JsonOutput
import org.codehaus.groovy.grails.web.mapping.LinkGenerator

import javax.print.attribute.standard.Media

@Transactional
class WireService {
    LinkGenerator grailsLinkGenerator
    static final String DEFAULT_DATE_FORMAT = 'yyyy-MM-dd HH:mm'
    static final String DEFAULT_DATE_FORMAT_WITH_SOCONDS = 'yyyy-MM-dd HH:mm:ss'

    public User findChpUser(String username) {
        def user = User.findByTelephoneOrUsername(username, username)
        if (user) {
            return user
        }
        return null
    }

    def buildProductCategories(User user) {
        def userTags = UserTag.findAllByUser(user)
        List<Product> products
        if (!userTags.isEmpty()) {
            def tags = userTags.tag
            products = ProductTag.findAllByTagInList(tags).product.unique()
        }


        if (!products) {
            throw new RuntimeException("You Currently Do Not Have Any Products Assigned To You")
        }


        def categories = products.groupBy { it.category }
        def categoryMsgs = categories.collect { category ->

            new MsgProductCategory.Builder().uuid(category.key.id)
                    .name(category.key.categoryName)
                    .products(category.value.collect { product -> toProductMsg(product, user) })
                    .build()
        }

        def categoryListMsg = new MsgProductCategoryList.Builder().categories(categoryMsgs)
                .build()

        return categoryListMsg
    }

    private MsgProduct toProductMsg(Product product, User user) {
        def groupProductPrice = GroupProductPrice.findGroupProductPrice(user, product)

        new MsgProduct.Builder().uuid(product.id)
                .name(product.name)
                .desc(product.description)
                .image_url(generateProductionImgUrl(product))
                .video_url(product.videoUrl)
                .buying_price(groupProductPrice?.resolveWholeSalePrice() ?: product.wholeSalePrice)
                .selling_price(groupProductPrice?.resolveRetailPrice() ?: product.retailPrice)
                .saving_calculator_id(product?.savingsCalculator?.id)
                .promotion(getPromotion(product, user))
                .build()
    }

    private MsgPromotion getPromotion(Product product, User user) {
        def promotion = product.runningPromotion
        if (promotion) {
            def promotionGroup = promotion.getUserPromotionGroup(user)
            if (promotionGroup) {
                return new MsgPromotion.Builder().uuid(promotion.id)
                        .name(promotion.promotionName)
                        .desc(promotion.description)
                        .buying_price(promotionGroup?.buyingPrice?.toString())
                        .selling_price(promotionGroup?.sellingPrice?.toString())
                        .build()
            }
        }
        return null
    }

    private def getProductSellingPrice(UserProductGroup group, Product product) {
        def groupProductPrice = GroupProductPrice.findByProductAndChpGroup(product, group)
        if (groupProductPrice) {
            return groupProductPrice.retailPrice
        }
        return product.retailPrice
    }

    private def getProductBuyingPrice(UserProductGroup group, Product product) {
        def groupProductPrice = GroupProductPrice.findByProductAndChpGroup(product, group)
        if (groupProductPrice) {
            return groupProductPrice.wholeSalePrice
        }
        return product.wholeSalePrice
    }

    def buildFaqCategories(User user) {
        def categories = FaqCategory.list()

        def categgoryMsgs = categories.collect { category ->
            new MsgFaqCategory.Builder().uuid(category.id)
                    .name(category.name)
                    .description(category.description)
                    .faqs(category.faqs.collect { faq -> toFaqMsg(faq) })
                    .build()
        }

        def categoryListMsg = new MsgFaqCategoryList.Builder().categories(categgoryMsgs)
                .build()

        return categoryListMsg
    }

    def toFaqMsg(Faq faq) {
        new MsgFaq.Builder().uuid(faq.id)
                .question(faq.question)
                .answer(faq.answer)
                .build()
    }

    def buildEvents(User user) {

        def eventTags = EventTag.findUserEventTags(user)

        def events = eventTags.groupBy { it.event }

        def eventMsgs = events.collect { event ->
            new MsgEvent.Builder().uuid(event.key.id)
                    .title(event.key.title)
                    .description(event.key.description)
                    .venue(event.key.venue)
                    .host(event.key.host)
                    .expiry_date(event.key.expiryDate?.format(DEFAULT_DATE_FORMAT))
                    .start_date(event.key.startDate?.format(DEFAULT_DATE_FORMAT))
                    .notification_date(event.key.notificationDate?.format(DEFAULT_DATE_FORMAT))
                    .build()
        }

        def eventListMsg = new MsgEventList.Builder().events(eventMsgs).build()

        return eventListMsg
    }

    def generateProductionImgUrl(Product product) {
        if (product.imagePath) {
            return grailsLinkGenerator.link(controller: 'product', action: 'showImg', id: product.id, absolute: true)
        }
        return null
    }

    def generateResourceUrl(MediaResource resource){
        if(resource.resourceUrl){
            return grailsLinkGenerator.link(controller: 'mediaResource',action: 'showResourceUrl',id: resource.id,absolute: true)
        }
        return null
    }

    def buildSavingCalculators(User user) {
        def calcs = SavingsCalculator.list()
        def calcMsg = calcs.collect { calc ->
            new MsgSavingCalculator.Builder().uuid(calc.id)
                    .name(calc.title)
                    .description(calc.notes)
                    .variables(calc.variables.collect { variable -> toCalcVariableMsg(variable) })
                    .build()
        }

        def calcList = new MsgSavingCalculatorList.Builder().saving_calculator_list(calcMsg).build()

        return calcList
    }

    def toCalcVariableMsg(CalculatorVariable variable) {
        new MsgCalculatorVariable.Builder().uuid(variable.id)
                .variable_name(variable.variable)
                .variable_text(variable.text)
                .type(getTypeValue(variable.type))
                .order_of_display(variable.orderOfDisplay)
                .formula(variable.formular)
                .build()
    }

    MsgCalculatorVariable.Type getTypeValue(String s) {
        switch (s) {
            case 'input':
                return MsgCalculatorVariable.Type.INPUT
            case 'output':
                return MsgCalculatorVariable.Type.OUTPUT
            default:
                return null
        }
    }

    def processProductLogs(ClientRequest clientRequest, User user) {

        def productLogs = clientRequest.view_product_log_list.view_product_log_list

        for (mobileLog in productLogs) {
            try {
                def misLog = toProductLog(mobileLog, user)
                if (!LogViewProduct.findById(misLog.id)) {
                    misLog.save(flush: true, failOnError: true)
                } else {
                    throw new RuntimeException("Duplicate Product Log")
                }
            } catch (Exception x) {
                log.warn("Failed to Save ProductLog: Reason($x.message)")
                new LogFailed(
                        type: clientRequest.request.toString(),
                        msg: JsonOutput.toJson(toFieldMap(mobileLog)),
                        errorMsg: x.message
                ).save()
            }

        }
    }

    def processFaqLogs(ClientRequest clientRequest, User user) {

        def mobileLogs = clientRequest.view_faq_log_list.view_faq_log_list

        for (mobileLog in mobileLogs) {
            try {
                def misLog = toFaqLog(mobileLog, user)
                if (!LogViewProduct.findById(misLog.id)) {
                    misLog.save(flush: true, failOnError: true)
                } else {
                    throw new RuntimeException("Duplicate FAQ Log")
                }
            } catch (Exception x) {
                log.warn("Failed to Save FAQLog: Reason($x.message)")
                new LogFailed(
                        type: clientRequest.request.toString(),
                        msg: JsonOutput.toJson(toFieldMap(mobileLog)),
                        errorMsg: x.message
                ).save()
            }

        }
    }

    def processFaqCategoryLogs(ClientRequest clientRequest, User user) {

        def mobileLogs = clientRequest.view_faq_category_list.view_faq_category_list

        for (pLog in mobileLogs) {
            try {
                def misLog = toFaqCategoryLog(pLog, user)
                if (!LogViewProduct.findById(misLog.id)) {
                    misLog.save(flush: true, failOnError: true)
                } else {
                    throw new RuntimeException("Duplicate FaqCategory Log")
                }
            } catch (Exception x) {
                log.warn("Failed to Save FAQCategoryLog: Reason($x.message)")
                new LogFailed(
                        type: clientRequest.request.toString(),
                        msg: JsonOutput.toJson(toFieldMap(pLog)),
                        errorMsg: x.message
                ).save()
            }

        }
    }

    LogViewProduct toProductLog(MsgViewProductLog productLog, User user) {
        def product = new LogViewProduct(
                id: productLog.uuid,
                username: user.username,
                userId: user.id,
                logDate: parseDate(productLog.date_created),
                branch: user.branch?.branchName,
                groups: user.commaSeparatedTags,

                categoryId: productLog.category_uuid,
                categoryName: productLog.category_name,
                productId: productLog.product_uuid,
                productName: productLog.product_name
        )
        product.id = productLog.uuid
        return product
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private Date parseDate(String productLog) {
        try {
            return Date.parse(DEFAULT_DATE_FORMAT, productLog)
        } catch (x) {
            log.warn("Failed to parse date: [$productLog]: $x.message")
            return new Date()

        }
    }

    @SuppressWarnings("GrMethodMayBeStatic")
    private Date parseDateWithSeconds(String date) {
        try {
            return Date.parse(DEFAULT_DATE_FORMAT_WITH_SOCONDS, date)
        } catch (x) {
            log.warn("Failed to parse date with seconds: [$date]: $x.message")
            return new Date()

        }
    }

    LogViewFaq toFaqLog(MsgViewFaQLog msqFaqLog, User user) {
        def faq = new LogViewFaq(
                id: msqFaqLog.uuid,
                username: user.username,
                userId: user.id,
                logDate: parseDate(msqFaqLog.date_created),
                branch: user.branch?.branchName,
                groups: user.commaSeparatedTags,

                categoryId: msqFaqLog.category_uuid,
                categoryName: msqFaqLog.category_name,
                questionId: msqFaqLog.question_uuid,
                question: msqFaqLog.question
        )
        faq.id = msqFaqLog.uuid
        return faq
    }

    LogViewFaqCategory toFaqCategoryLog(MsgViewFAQCategoryLog msqFaqCategoryLog, User user) {
        def category = new LogViewFaqCategory(
                id: msqFaqCategoryLog.uuid,
                username: user.username,
                userId: user.id,
                logDate: parseDate(msqFaqCategoryLog.date_created),
                branch: user.branch?.branchName,
                groups: user.commaSeparatedTags,

                categoryId: msqFaqCategoryLog.category_uuid,
                categoryName: msqFaqCategoryLog.category_name,
        )
        category.id = msqFaqCategoryLog.uuid
        return category
    }

    //Used to field map because JSONOutput only supports getters and setters
    Map<String, Object> toFieldMap(def entity) {
        def fields = entity.getClass().getFields()
        def map = new HashMap<String, Object>()
        for (f in fields)
            map.put(f.getName(), f.get(entity))
        return map
    }

    def processViewVideoLogs(ClientRequest clientRequest, User user) {
        log.info("Processing view video logs")
        def mobileLogs = clientRequest.view_video_log_list.view_video_log_list
        log.info(mobileLogs.size())
        for (mobileLog in mobileLogs) {
            try {
                def misLog = toLogViewVideo(mobileLog, user)
                if (!LogViewVideo.findById(misLog.id)) {
                    misLog.save(flush: true, failOnError: true)
                } else {
                    throw new RuntimeException("Duplicate View Video Log")
                }
            } catch (Exception x) {
                log.warn("Failed to Save View Video Log: Reason($x.message)")
                new LogFailed(
                        type: clientRequest.request.toString(),
                        msg: JsonOutput.toJson(toFieldMap(mobileLog)),
                        errorMsg: x.message
                ).save()
            }
        }
    }

    LogViewVideo toLogViewVideo(MsgViewVideoLog msgViewVideoLog, User user) {
        def logViewVideo = new LogViewVideo(
                id: msgViewVideoLog.uuid,
                username: msgViewVideoLog.username,
                userId: msgViewVideoLog.user_id,
                videoTitle: msgViewVideoLog.video_title,
                videoUUID: msgViewVideoLog.video_uuid,
                dateCreated: parseDate(msgViewVideoLog.date_created)
        )
        logViewVideo.id = msgViewVideoLog.uuid
        return logViewVideo
    }

    def processVideoPlaybackLogs(ClientRequest clientRequest, User user) {
        log.info("Processing videoplayback")
        def mobileLogs = clientRequest.video_playback_list.video_playback_list
        log.info(mobileLogs.size())
        for (mobileLog in mobileLogs) {
            try {
                def misLog = toLogVideoPlayback(mobileLog, user)
                if (!LogVideoPlayback.findById(misLog.id)) {
                    misLog.save(flush: true, failOnError: true)
                } else {
                    throw new RuntimeException("Duplicate Playback Log")
                }
            } catch (Exception x) {
                log.warn("Failed to Save Video Playback Log: Reason($x.message)")
                new LogFailed(
                        type: clientRequest.request.toString(),
                        msg: JsonOutput.toJson(toFieldMap(mobileLog)),
                        errorMsg: x.message
                ).save()
            }
        }
    }

    LogVideoPlayback toLogVideoPlayback(MsgVideoPlayback msgVideoPlayback, User user) {
        def logPlayback = new LogVideoPlayback(
                id: msgVideoPlayback.uuid,
                username: msgVideoPlayback.username,
                userId: msgVideoPlayback.user_id,
                videoUUID: msgVideoPlayback.video_uuid,
                videoTitle: msgVideoPlayback.video_title,
                sessionId: msgVideoPlayback.session_id,
                actionEvent: msgVideoPlayback.action_event,
                currentTimeInSeconds: Long.valueOf(msgVideoPlayback.current_time_in_seconds),
                dateCreated: parseDate(msgVideoPlayback.date_created),
                timePlayed: parseDateWithSeconds(msgVideoPlayback.date_created)
        )
        logPlayback.id = msgVideoPlayback.uuid
        return logPlayback
    }

    def buildResources(User user) {
        def resources = MediaResource.list()
        def resourceMsgs = resources.collect{resource->
            new MsgMediaResource.Builder().uuid(resource.id)
            .title(resource.title)
            .description(resource.description)
            .resourceUrl(generateResourceUrl(resource))
            .resourceType(resource.resourceType)
            .expiry_date(resource.expiryDate?.format(DEFAULT_DATE_FORMAT))
            .build()
        }

        def resourcesListMsg = new MsgMediaResourceList.Builder().resources(resourceMsgs).build()
        return resourcesListMsg
    }
}
