package com.omnitech.mis

import grails.transaction.Transactional

@Transactional
class EventService {

    Event addUserEvents(Event event, List<User> userList) {
        userList.each{
            if(!event.hasUser(it)){
              event.addToUserEvents(new UserEvent(user: it,event: event))
            }
        }
        return event
    }
}
