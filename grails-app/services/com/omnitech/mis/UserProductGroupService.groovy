package com.omnitech.mis

import grails.transaction.Transactional

@Transactional
class UserProductGroupService {

   UserProductGroup addUsersToGroup(UserProductGroup userProductGroup,List<User> userList){
       userList.each{
           if(!userProductGroup?.users?.contains(it)){
               userProductGroup.users.add(it)
           }
       }
       return userProductGroup
   }
}
