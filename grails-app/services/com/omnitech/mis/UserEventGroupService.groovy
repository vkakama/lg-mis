package com.omnitech.mis

import grails.transaction.Transactional

@Transactional
class UserEventGroupService {



    EventTag addUsersToGroup(EventTag userEventGroup, List<User> userList){
        userList.each{
            if(!userEventGroup.users.contains(it)){
                userEventGroup.users.add(it)
            }
        }
        return userEventGroup
    }
}
