package com.omnitech.mis

import grails.transaction.Transactional

@Transactional
class ProductService {

    SavingsCalculator mapSavingCalculatorToModel(def price,def monthProfit,def threeMonthsProfit
                                                 ,def oneYearProfit,def title,def notes){


        def oneMonthRate = (monthProfit/price)*100
        def threeMonthRate = (threeMonthsProfit/price)*100
        def oneYearRate = (oneYearProfit/price)*100

        SavingsCalculator calculator = new SavingsCalculator(title: title,notes: notes,
        oneMonthPercentage: oneMonthRate,threeMonthsPercentage: threeMonthRate,
        oneYearPercentage: oneYearRate)

        return calculator
    }
}
