modules = {
    application {
        resource url:'js/application.js'
    }


    chosen {
        dependsOn 'jquery'
        resource url: 'js/lib/chosen/chosen.jquery.min.js'
        resource url: 'js/chosen.proto.min.js'
        resource url: 'js/lib/chosen/chosen.css'
    }

    angular_chosen{
        dependsOn('chosen')
        resource url: 'js/lib/angular-chosen.min.js'
    }

    exprEngine{
        resource url: 'js/lib/math.min.js'
    }

    bootstrap_toggle{
        dependsOn('bootstrap')
        resource url: 'js/lib/bootstrap-toggle/bootstrap-toggle.min.css'
        resource url: 'js/lib/bootstrap-toggle/bootstrap-toggle.min.js'
    }
    d3 {
        resource url: 'js/lib/d3-3.5.17/d3.min.js'
    }
    d3Tip {
        resource url: 'js/lib/d3-tip/d3.tip.min.js'
    }
    dc {
        dependsOn('d3', 'crossfilter','d3Tip')
        resource url: 'js/lib/dc-js/dc.min.css'
        resource url: 'js/lib/dc-js/dc.js'
    }
    crossfilter {
        resource url: 'js/lib/crossfilter-1.3.14/crossfilter.min.js'
    }

    dashboardGraphs {
        dependsOn('dc')
        resource url: 'js/product.js'
        resource url: 'js/lgmis.js'
    }





}