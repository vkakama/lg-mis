import com.github.javafaker.Faker
import com.omnitech.mis.*
import fuzzycsv.FuzzyCSV

class BootStrap {

    def init = { servletContext ->
//        initdata()
//        uploadUsers()
        createDefaultDescriminationGroup()
        createDefaultUserGroup()
    }
    def destroy = {
    }

    def initdata() {
        Faker faker = new Faker()

        def category = new FaqCategory(name: faker.commerce().department(), description: faker.chuckNorris().fact())
        category.save(flush: true)
        def category1 = new FaqCategory(name: faker.commerce().department(), description: faker.chuckNorris().fact())
        category1.save(flush: true)
        def category2 = new FaqCategory(name: faker.commerce().department(), description: faker.chuckNorris().fact())
        category2.save(flush: true)

        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category).save(flush: true)

        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category1).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category1).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category1).save(flush: true)


        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category2).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category2).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category2).save(flush: true)


        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category3).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category3).save(flush: true)
        new Faq(question: faker.chuckNorris().fact(), answer: faker.chuckNorris().fact(), faqCategory: category3).save(flush: true)
    }

    def uploadUsers() {
        def url = new File("D:\\work\\omni\\katogo\\src\\main\\groovy\\org\\kakav\\lg\\CHP_List_Living_Goods.csv")
        def csvFile = url.text
        def lists = FuzzyCSV.toListOfLists(FuzzyCSV.parseCsv(csvFile))

        println lists
        lists.eachWithIndex { user, index ->
            println user
            if (index != 0) {
                try {
                    def branch = new Branch(branchName: user.get(3)).save(flush: true)
                    def role = Role.findByAuthority("ROLE_CHP")
                    def chp = new User(username: user.get(2), password: 'pass', name: user.get(4),
                            telephone: user.get(5), branch: branch).save(flush: true)
                    UserRole.create(chp, role, true).save(flush: true)
                } catch (Exception ex) {
                    ex.printStackTrace()
                }
            }
        }

        /*     AppHolder.withMisSqlNonTx {
                 withBatch(100) { stmt ->
                     lists.eachWithIndex { user, index ->
                         if (index != 0) {
                             def branch = new Branch(branchName: user.get(3)).save(flush: true)
                             def username = new User(username: user.get(2), password: 'pass', name: user.get(4),
                                     telephone: user.get(5), branch: branch).save(flush: true)
                         }
                     }
                 }
             }*/


    }


    def createDefaultDescriminationGroup() {

        def defaultGrp = UserProductGroup.findByName("Default")
        if (!defaultGrp) {
            def uuid = UUID.randomUUID().toString().replaceAll("-", "")
            AppHolder.withMisSqlNonTx {
                execute("INSERT INTO user_product_group(id,date_created,last_updated,name) VALUES ('${uuid}',now(),now(),'Default')".toString())
                execute("UPDATE user SET user_product_group_id = ${uuid}")

            }

            println("finishing saving users")

            Product.list().each { product ->
                def groupId = UUID.randomUUID().toString().replaceAll("-", "")
                AppHolder.withMisSqlNonTx {
                    execute("""INSERT INTO group_product_price(id, chp_group_id, date_created, last_updated, name, product_id, retail_price, whole_sale_price) 
                            VALUES (${groupId},${uuid},now(),now(),"default",${product.id},-1,-1)""")
                }

            }

        }


    }

    def createDefaultUserGroup() {
        def defaultGrp = Tag.findByName("All Users")
        if (!defaultGrp) {
            def tag = new Tag(name: "All Users")
            tag.save(flush: true)
            User.list().each { user ->
                def id = UUID.randomUUID().toString().replaceAll("-", "")
                AppHolder.withMisSqlNonTx {
                    execute("""INSERT INTO user_tag(id, date_created, last_updated, tag_id, user_id)
                                    VALUES(${id},now(),now(),${tag.id},${user.id}) """)
                }
            }
//            add all products to this group
            Product.list().each {prod->
                def id = UUID.randomUUID().toString().replaceAll("-", "")
                AppHolder.withMisSqlNonTx {
                    execute("""INSERT INTO product_tag(id, date_created, last_updated, product_id, tag_id) 
                    VALUES (${id},now(),now(),${prod.id},${tag.id})""")
                }

            }
        }
    }
}
