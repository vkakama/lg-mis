/**
 * User: victor
 * Date: 8/7/14
 */
databaseChangeLog = {
    include(file: "tables.xml")
    include(file: "data.xml")
}
