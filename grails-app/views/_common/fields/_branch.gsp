

<div>
    <label for="branches" class="control-label"><g:message code="userProductGroup.users.label" default="Select Branches" /></label>
    <div>
        <g:select class="form-control chzn-select" style="width: 50%;" name="branches" from="${com.omnitech.mis.Branch.list(sort: 'branchName',order: 'asc')}"
                  multiple="multiple" optionKey="id" size="5" value="" id="branches"/>
    </div>
</div>