

<div>
    <label for="tags" class="control-label"><g:message code="userProductGroup.users.label" default="Select From CHP Groups" /></label>
    <div>
        <g:select class="form-control chzn-select" style="width: 50%;" name="tags" from="${com.omnitech.mis.Tag.list(sort: 'name',order: 'asc')}"
                  multiple="multiple" optionKey="id" size="5" value="" id="tags"/>
    </div>
</div>