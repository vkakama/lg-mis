
<%@ page import="com.omnitech.mis.LogViewFaq" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewFaq.label', default: 'LogViewFaq')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-logViewFaq" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.question.label" default="Question" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "question")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.questionId.label" default="Question Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "questionId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.categoryId.label" default="Category Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "categoryId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.categoryName.label" default="Category Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "categoryName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.branch.label" default="Branch" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "branch")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewFaqInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.groups.label" default="Groups" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "groups")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewFaqInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.logDate.label" default="Log Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewFaqInstance?.logDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.userId.label" default="User Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "userId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaq.username.label" default="Username" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqInstance, field: "username")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
