
<%@ page import="com.omnitech.mis.LogViewFaq" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewFaq.label', default: 'LogViewFaq')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-logViewFaq" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="question" title="${message(code: 'logViewFaq.question.label', default: 'Question')}" />
			
			<g:sortableColumn property="questionId" title="${message(code: 'logViewFaq.questionId.label', default: 'Question Id')}" />
			
			<g:sortableColumn property="categoryId" title="${message(code: 'logViewFaq.categoryId.label', default: 'Category Id')}" />
			
			<g:sortableColumn property="categoryName" title="${message(code: 'logViewFaq.categoryName.label', default: 'Category Name')}" />
			
			<g:sortableColumn property="branch" title="${message(code: 'logViewFaq.branch.label', default: 'Branch')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'logViewFaq.dateCreated.label', default: 'Date Created')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${logViewFaqInstanceList}" status="i" var="logViewFaqInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${logViewFaqInstance.id}">${fieldValue(bean: logViewFaqInstance, field: "question")}</g:link></td>
				
				<td>${fieldValue(bean: logViewFaqInstance, field: "questionId")}</td>
				
				<td>${fieldValue(bean: logViewFaqInstance, field: "categoryId")}</td>
				
				<td>${fieldValue(bean: logViewFaqInstance, field: "categoryName")}</td>
				
				<td>${fieldValue(bean: logViewFaqInstance, field: "branch")}</td>
				
				<td><g:formatDate date="${logViewFaqInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${logViewFaqInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${logViewFaqInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${logViewFaqInstanceCount}" />
	</div>
</section>

</body>

</html>
