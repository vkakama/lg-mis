<%@ page import="com.omnitech.mis.LogViewFaq" %>



<div class="${hasErrors(bean: logViewFaqInstance, field: 'question', 'error')} ">
	<label for="question" class="control-label"><g:message code="logViewFaq.question.label" default="Question" /></label>
	<div>
		<g:textField class="form-control" name="question" value="${logViewFaqInstance?.question}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'question', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'questionId', 'error')} ">
	<label for="questionId" class="control-label"><g:message code="logViewFaq.questionId.label" default="Question Id" /></label>
	<div>
		<g:textField class="form-control" name="questionId" value="${logViewFaqInstance?.questionId}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'questionId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'categoryId', 'error')} ">
	<label for="categoryId" class="control-label"><g:message code="logViewFaq.categoryId.label" default="Category Id" /></label>
	<div>
		<g:textField class="form-control" name="categoryId" value="${logViewFaqInstance?.categoryId}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'categoryId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'categoryName', 'error')} ">
	<label for="categoryName" class="control-label"><g:message code="logViewFaq.categoryName.label" default="Category Name" /></label>
	<div>
		<g:textField class="form-control" name="categoryName" value="${logViewFaqInstance?.categoryName}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'categoryName', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'branch', 'error')} ">
	<label for="branch" class="control-label"><g:message code="logViewFaq.branch.label" default="Branch" /></label>
	<div>
		<g:textField class="form-control" name="branch" value="${logViewFaqInstance?.branch}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'branch', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'groups', 'error')} ">
	<label for="groups" class="control-label"><g:message code="logViewFaq.groups.label" default="Groups" /></label>
	<div>
		<g:textField class="form-control" name="groups" value="${logViewFaqInstance?.groups}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'groups', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'logDate', 'error')} required">
	<label for="logDate" class="control-label"><g:message code="logViewFaq.logDate.label" default="Log Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="logDate" precision="day"  value="${logViewFaqInstance?.logDate}"  />
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'logDate', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'userId', 'error')} ">
	<label for="userId" class="control-label"><g:message code="logViewFaq.userId.label" default="User Id" /></label>
	<div>
		<g:textField class="form-control" name="userId" value="${logViewFaqInstance?.userId}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'userId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqInstance, field: 'username', 'error')} ">
	<label for="username" class="control-label"><g:message code="logViewFaq.username.label" default="Username" /></label>
	<div>
		<g:textField class="form-control" name="username" value="${logViewFaqInstance?.username}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqInstance, field: 'username', 'error')}</span>
	</div>
</div>

