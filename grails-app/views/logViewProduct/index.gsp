
<%@ page import="com.omnitech.mis.LogViewProduct" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewProduct.label', default: 'LogViewProduct')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-logViewProduct" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="categoryId" title="${message(code: 'logViewProduct.categoryId.label', default: 'Category Id')}" />
			
			<g:sortableColumn property="categoryName" title="${message(code: 'logViewProduct.categoryName.label', default: 'Category Name')}" />
			
			<g:sortableColumn property="productName" title="${message(code: 'logViewProduct.productName.label', default: 'Product Name')}" />
			
			<g:sortableColumn property="productId" title="${message(code: 'logViewProduct.productId.label', default: 'Product Id')}" />
			
			<g:sortableColumn property="branch" title="${message(code: 'logViewProduct.branch.label', default: 'Branch')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'logViewProduct.dateCreated.label', default: 'Date Created')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${logViewProductInstanceList}" status="i" var="logViewProductInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${logViewProductInstance.id}">${fieldValue(bean: logViewProductInstance, field: "categoryId")}</g:link></td>
				
				<td>${fieldValue(bean: logViewProductInstance, field: "categoryName")}</td>
				
				<td>${fieldValue(bean: logViewProductInstance, field: "productName")}</td>
				
				<td>${fieldValue(bean: logViewProductInstance, field: "productId")}</td>
				
				<td>${fieldValue(bean: logViewProductInstance, field: "branch")}</td>
				
				<td><g:formatDate date="${logViewProductInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${logViewProductInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${logViewProductInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${logViewProductInstanceCount}" />
	</div>
</section>

</body>

</html>
