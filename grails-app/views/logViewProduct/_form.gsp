<%@ page import="com.omnitech.mis.LogViewProduct" %>



<div class="${hasErrors(bean: logViewProductInstance, field: 'categoryId', 'error')} ">
	<label for="categoryId" class="control-label"><g:message code="logViewProduct.categoryId.label" default="Category Id" /></label>
	<div>
		<g:textField class="form-control" name="categoryId" value="${logViewProductInstance?.categoryId}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'categoryId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'categoryName', 'error')} ">
	<label for="categoryName" class="control-label"><g:message code="logViewProduct.categoryName.label" default="Category Name" /></label>
	<div>
		<g:textField class="form-control" name="categoryName" value="${logViewProductInstance?.categoryName}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'categoryName', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'productName', 'error')} ">
	<label for="productName" class="control-label"><g:message code="logViewProduct.productName.label" default="Product Name" /></label>
	<div>
		<g:textField class="form-control" name="productName" value="${logViewProductInstance?.productName}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'productName', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'productId', 'error')} ">
	<label for="productId" class="control-label"><g:message code="logViewProduct.productId.label" default="Product Id" /></label>
	<div>
		<g:textField class="form-control" name="productId" value="${logViewProductInstance?.productId}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'productId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'branch', 'error')} ">
	<label for="branch" class="control-label"><g:message code="logViewProduct.branch.label" default="Branch" /></label>
	<div>
		<g:textField class="form-control" name="branch" value="${logViewProductInstance?.branch}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'branch', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'groups', 'error')} ">
	<label for="groups" class="control-label"><g:message code="logViewProduct.groups.label" default="Groups" /></label>
	<div>
		<g:textField class="form-control" name="groups" value="${logViewProductInstance?.groups}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'groups', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'logDate', 'error')} required">
	<label for="logDate" class="control-label"><g:message code="logViewProduct.logDate.label" default="Log Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="logDate" precision="day"  value="${logViewProductInstance?.logDate}"  />
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'logDate', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'userId', 'error')} ">
	<label for="userId" class="control-label"><g:message code="logViewProduct.userId.label" default="User Id" /></label>
	<div>
		<g:textField class="form-control" name="userId" value="${logViewProductInstance?.userId}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'userId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewProductInstance, field: 'username', 'error')} ">
	<label for="username" class="control-label"><g:message code="logViewProduct.username.label" default="Username" /></label>
	<div>
		<g:textField class="form-control" name="username" value="${logViewProductInstance?.username}"/>
		<span class="help-inline">${hasErrors(bean: logViewProductInstance, field: 'username', 'error')}</span>
	</div>
</div>

