
<%@ page import="com.omnitech.mis.LogViewProduct" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewProduct.label', default: 'LogViewProduct')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-logViewProduct" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.categoryId.label" default="Category Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "categoryId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.categoryName.label" default="Category Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "categoryName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.productName.label" default="Product Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "productName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.productId.label" default="Product Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "productId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.branch.label" default="Branch" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "branch")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewProductInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.groups.label" default="Groups" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "groups")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewProductInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.logDate.label" default="Log Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewProductInstance?.logDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.userId.label" default="User Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "userId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewProduct.username.label" default="Username" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewProductInstance, field: "username")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
