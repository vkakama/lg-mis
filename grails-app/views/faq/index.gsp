
<%@ page import="com.omnitech.mis.Faq" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faq.label', default: 'Faq')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-faq" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="question" title="${message(code: 'faq.question.label', default: 'Question')}" />
			
			<g:sortableColumn property="answer" title="${message(code: 'faq.answer.label', default: 'Answer')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'faq.dateCreated.label', default: 'Date Created')}" />
			
			<th><g:message code="faq.faqCategory.label" default="Faq Category" /></th>
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'faq.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${faqInstanceList}" status="i" var="faqInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${faqInstance.id}">${fieldValue(bean: faqInstance, field: "question")}</g:link></td>
				
				<td>${fieldValue(bean: faqInstance, field: "answer")}</td>
				
				<td><g:formatDate date="${faqInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td>${fieldValue(bean: faqInstance, field: "faqCategory")}</td>
				
				<td><g:formatDate date="${faqInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${faqInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${faqInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${faqInstance?.question}"
					   data-field-title="Delete FAQ"
					   title="Delete FAQ">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${faqInstanceCount}" />
	</div>
</section>
<g:render template="/_common/modals/deleteDialog"/>

</body>

</html>
