<%@ page import="com.omnitech.mis.Faq" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faq.label', default: 'Faq')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<section id="edit-faq" class="first">

	<g:hasErrors bean="${faqInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${faqInstance}" as="list" />
		</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" role="form" >
	<g:hiddenField name="id" value="${faqInstance?.id}" />
	<g:hiddenField name="version" value="${faqInstance?.version}" />
	<g:hiddenField name="_method" value="PUT" />

	<g:render template="form"/>

	<div class="form-actions margin-top-medium">
		<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
		<button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	</div>
	</g:form>

</section>

</body>

</html>
