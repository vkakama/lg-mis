
<%@ page import="com.omnitech.mis.Faq" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faq.label', default: 'Faq')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-faq" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faq.question.label" default="Question" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: faqInstance, field: "question")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faq.answer.label" default="Answer" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: faqInstance, field: "answer")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faq.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${faqInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faq.faqCategory.label" default="Faq Category" /></td>
			
			<td valign="top" class="value"><g:link controller="faqCategory" action="show" id="${faqInstance?.faqCategory?.id}">${faqInstance?.faqCategory?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faq.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${faqInstance?.lastUpdated}" /></td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
