<%@ page import="com.omnitech.mis.Faq" %>



<div class="${hasErrors(bean: faqInstance, field: 'question', 'error')} ">
	<label for="question" class="control-label"><g:message code="faq.question.label" default="Question" /></label>
	<div>
		<g:textField style="width: 50%;"  class="form-control" name="question" value="${faqInstance?.question}"/>
		<span class="help-inline">${hasErrors(bean: faqInstance, field: 'question', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: faqInstance, field: 'answer', 'error')} ">
	<label for="answer" class="control-label"><g:message code="faq.answer.label" default="Answer" /></label>
	<div>
		<g:textField style="width: 50%;"  class="form-control" name="answer" value="${faqInstance?.answer}"/>
		<span class="help-inline">${hasErrors(bean: faqInstance, field: 'answer', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: faqInstance, field: 'faqCategory', 'error')} required">
	<label for="faqCategory" class="control-label"><g:message code="faq.faqCategory.label" default="Faq Category" /><span class="required-indicator">*</span></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" id="faqCategory" name="faqCategory.id" from="${com.omnitech.mis.FaqCategory.list()}" optionKey="id" required="" value="${faqInstance?.faqCategory?.id}"/>
		<span class="help-inline">${hasErrors(bean: faqInstance, field: 'faqCategory', 'error')}</span>
	</div>
</div>

