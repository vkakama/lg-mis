<%@ page import="com.omnitech.mis.EventTag" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userEventGroup.label', default: 'EventTag')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="edit-userEventGroup" class="first">

	<g:hasErrors bean="${userEventGroupInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${userEventGroupInstance}" as="list" />
		</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" role="form" >
	<g:hiddenField name="id" value="${userEventGroupInstance?.id}" />
	<g:hiddenField name="version" value="${userEventGroupInstance?.version}" />
	<g:hiddenField name="_method" value="PUT" />

	<g:render template="form"/>

	<div class="form-actions margin-top-medium">
		<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
		<button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	</div>
	</g:form>

</section>

<r:require modules="chosen"/>
<g:javascript >
	$(".chzn-select").chosen();
</g:javascript>
</body>

</html>
