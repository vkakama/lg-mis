<%@ page import="com.omnitech.mis.EventTag" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userEventGroup.label', default: 'EventTag')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="create-userEventGroup" class="first">

	<g:hasErrors bean="${userEventGroupInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${userEventGroupInstance}" as="list" />
		</div>
	</g:hasErrors>

	<g:form action="save" class="form-horizontal" role="form" >
	<g:render template="form"/>

	<div class="form-actions margin-top-medium">
		<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
		<button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	</div>
	</g:form>

</section>

<r:require modules="chosen"/>
<g:javascript >
	$(".chzn-select").chosen();
</g:javascript>
</body>

</html>
