<%@ page import="com.omnitech.mis.EventTag" %>



<div class="${hasErrors(bean: userEventGroupInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="userEventGroup.name.label" default="Name" /></label>
	<div>
		<g:textField class="form-control"  style="width: 50%;" name="name" value="${userEventGroupInstance?.name}"/>
		<span class="help-inline">${hasErrors(bean: userEventGroupInstance, field: 'name', 'error')}</span>
	</div>
</div>


<div class="${hasErrors(bean: userEventGroupInstance, field: 'event', 'error')} required">
	<label for="event" class="control-label"><g:message code="userEventGroup.event.label" default="Event" /><span class="required-indicator">*</span></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" id="event" name="event.id" from="${com.omnitech.mis.Event.list()}" optionKey="id" required="" value="${userEventGroupInstance?.event?.id}"/>
		<span class="help-inline">${hasErrors(bean: userEventGroupInstance, field: 'event', 'error')}</span>
	</div>
</div>
<div class="${hasErrors(bean: userEventGroupInstance, field: 'users', 'error')} ">
	<label for="users" class="control-label"><g:message code="userEventGroup.users.label" default="Users" /></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" name="users" from="${com.omnitech.mis.User.list()}" multiple="multiple" optionKey="id" size="5" value="${userEventGroupInstance?.users*.id}"/>
		<span class="help-inline">${hasErrors(bean: userEventGroupInstance, field: 'users', 'error')}</span>
	</div>
</div>


<div>
	<g:render template="../_common/fields/branch"/>
</div>


