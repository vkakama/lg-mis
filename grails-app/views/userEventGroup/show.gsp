
<%@ page import="com.omnitech.mis.EventTag" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userEventGroup.label', default: 'EventTag')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="show-userEventGroup" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEventGroup.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userEventGroupInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEventGroup.event.label" default="Event" /></td>
			
			<td valign="top" class="value"><g:link controller="event" action="show" id="${userEventGroupInstance?.event?.id}">${userEventGroupInstance?.event?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEventGroup.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userEventGroupInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEventGroup.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userEventGroupInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEventGroup.users.label" default="Users" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${userEventGroupInstance.users}" var="u">
						<li><g:link controller="user" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
