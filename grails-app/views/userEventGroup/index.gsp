
<%@ page import="com.omnitech.mis.EventTag" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userEventGroup.label', default: 'EventTag')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-userEventGroup" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>

			<g:sortableColumn property="name" title="${message(code: 'userEventGroup.name.label', default: 'Name')}" />

			<th><g:message code="userEventGroup.event.label" default="Event" /></th>

			<g:sortableColumn property="dateCreated" title="${message(code: 'userEventGroup.dateCreated.label', default: 'Date Created')}" />

			<g:sortableColumn property="lastUpdated" title="${message(code: 'userEventGroup.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${userEventGroupInstanceList}" status="i" var="userEventGroupInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

				<td><g:link action="show" id="${userEventGroupInstance.id}">${fieldValue(bean: userEventGroupInstance, field: "name")}</g:link></td>

				<td>${fieldValue(bean: userEventGroupInstance, field: "event")}</td>

				<td>${fieldValue(bean: userEventGroupInstance, field: "dateCreated")}</td>

				<td><g:formatDate date="${userEventGroupInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>

				<td>
					<g:link action="edit" id="${userEventGroupInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${userEventGroupInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${userEventGroupInstanceCount}" />
	</div>
</section>

</body>

</html>
