



			<div class="${hasErrors(bean: registrationCodeInstance, field: 'token', 'error')} ">
				<label for="token" class="control-label"><g:message code="registrationCode.token.label" default="Token" /></label>
				<div>
					<g:textField class="form-control" name="token" value="${registrationCodeInstance?.token}"/>
					<span class="help-inline">${hasErrors(bean: registrationCodeInstance, field: 'token', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: registrationCodeInstance, field: 'username', 'error')} ">
				<label for="username" class="control-label"><g:message code="registrationCode.username.label" default="Username" /></label>
				<div>
					<g:textField class="form-control" name="username" value="${registrationCodeInstance?.username}"/>
					<span class="help-inline">${hasErrors(bean: registrationCodeInstance, field: 'username', 'error')}</span>
				</div>
			</div>

