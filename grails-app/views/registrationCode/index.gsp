

<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'registrationCode.label', default: 'RegistrationCode')}" />
    <title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-registrationCode" class="first">

    <table class="table table-bordered margin-top-medium">
        <thead>
        <tr>
            
            <g:sortableColumn property="dateCreated" title="${message(code: 'registrationCode.dateCreated.label', default: 'Date Created')}" />
            
            <g:sortableColumn property="token" title="${message(code: 'registrationCode.token.label', default: 'Token')}" />
            
            <g:sortableColumn property="username" title="${message(code: 'registrationCode.username.label', default: 'Username')}" />
            
            <td>
                Action
            </td>
        </tr>
        </thead>
        <tbody>
        <g:each in="${registrationCodeInstanceList}" status="i" var="registrationCodeInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                
                <td><g:link action="show" id="${registrationCodeInstance.id}">${fieldValue(bean: registrationCodeInstance, field: "dateCreated")}</g:link></td>
                
                <td>${fieldValue(bean: registrationCodeInstance, field: "token")}</td>
                
                <td>${fieldValue(bean: registrationCodeInstance, field: "username")}</td>
                
                <td>
                    <g:link action="edit" id="${registrationCodeInstance.id}"><i
                            class="glyphicon glyphicon-pencil"></i></g:link>
                    <g:link action="delete" id="${registrationCodeInstance.id}"><i
                            class="glyphicon glyphicon-remove"></i></g:link>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div>
        <bs:paginate total="${registrationCodeInstanceCount}" />
    </div>
</section>

</body>

</html>
