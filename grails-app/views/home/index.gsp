<html>

<head>
    <title><g:message code="default.welcome.title" args="[meta(name: 'app.name')]"/></title>
    <meta name="layout" content="kickstart"/>
    <link rel="stylesheet" href="${resource(dir: 'css', file: 'graph.css')}" type="text/css">
</head>

<body>
<div class="row">
    <div style="background-color: #f5f5f5" class="row">
        <div style="padding-bottom: 10px;padding-top: 10px" class="col-md-12">
            <div class="row">
                <div style="text-align: center">
                    <span>
                        <button type="button" class="btn btn-primary active bttn" id="btnProduct">PRODUCTS</button>
                        <button type="button" class="btn btn-info bttn" id="btnFaq">FAQ'S</button>
                    </span>
                </div>
            </div>

            <div class="container" id="product-container">
                <div class="row">
                    <section class="col-lg-4 connectedSortable">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Products</h3>
                            </div>
                            <!-- /.box-header-->
                            <div class="box-body">
                                <div class="row">
                                    <div id="product-chart-row"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="col-lg-6 connectedSortable">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Branches</h3>

                                <div class="box-tools pull-right">
                                    <button id="product-refresh-all" type="button"
                                            style="color: blue; font-size: 14px;text-decoration: underline;"
                                            class="btn btn-box-tool"><i>Reset All</i></button>
                                </div>
                            </div>
                            <!-- /.box-header-->
                            <div class="box-body">
                                <div class="row">
                                    <div id="product-chart-bar"></div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Product Categories</h3>
                            </div>
                            <!-- /.box-header-->
                            <div class="box-body">
                                <div class="row">
                                    <div id="product-chart-pie" class="pie"></div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Logging Timeline
                                    <select class="interval">
                                        <div class="clearfix"></div>
                                    </select>
                                </h3>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div id="product-chart-line"><span class="reset">Selected range:<span
                                            class="filter"></span>
                                    </span>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="product-filter-chart-line"  class="line"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- /.col-->
            </div>

            <div class="container" id="faq-container" style="display: none;">
                <div class="row">
                    <section class="col-lg-4 connectedSortable">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Questions</h3>
                            </div>
                            <!-- /.box-header-->
                            <div class="box-body">
                                <div class="row">
                                    <div id="question-chart-row"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="col-lg-6 connectedSortable">
                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Branches</h3>

                                <div class="box-tools pull-right">
                                    <button id="question-refresh-all" type="button"
                                            style="color: blue; font-size: 14px;text-decoration: underline;"
                                            class="btn btn-box-tool"><i>Reset All</i></button>
                                </div>
                            </div>
                            <!-- /.box-header-->
                            <div class="box-body">
                                <div class="row">
                                    <div id="question-chart-bar"></div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Question Categories</h3>
                            </div>
                            <!-- /.box-header-->
                            <div class="box-body">
                                <div class="row">
                                    <div id="question-chart-pie" class="pie"></div>
                                </div>
                            </div>
                        </div>

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Logging Time Line
                                    <select class="question-interval">
                                        <div class="clearfix"></div>
                                    </select>
                                </h3>
                            </div>

                            <div class="box-body">
                                <div class="row">
                                    <div id="question-chart-line"><span class="reset">Selected range:<span
                                            class="filter"></span>
                                    </span>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div id="question-filter-chart-line"  class="line"></div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- /.col-->
            </div>
        </div>
    </div>
</div>

<!-- /.row-->

<r:require module="dashboardGraphs"/>
</body>

</html>
