<%@ page import="com.omnitech.mis.SubCounty" %>



			<div class="${hasErrors(bean: subCountyInstance, field: 'name', 'error')} required">
				<label for="name" class="control-label"><g:message code="subCounty.name.label" default="Name" /><span class="required-indicator">*</span></label>
				<div>
					<g:textField class="form-control" name="name" required="" value="${subCountyInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: subCountyInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: subCountyInstance, field: 'district', 'error')} required">
				<label for="district" class="control-label"><g:message code="subCounty.district.label" default="District" /><span class="required-indicator">*</span></label>
				<div>
					<g:select class="form-control" style="width: 50%;" id="district" name="district.id" from="${com.omnitech.mis.District.list()}" optionKey="id" required="" value="${subCountyInstance?.district?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: subCountyInstance, field: 'district', 'error')}</span>
				</div>
			</div>

