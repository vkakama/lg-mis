
<%@ page import="com.omnitech.mis.Promotion" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'promotion.label', default: 'Promotion')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-promotion" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.promotionName.label" default="Promotion Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: promotionInstance, field: "promotionName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: promotionInstance, field: "description")}</td>
			
		</tr>
%{--
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.buyingPrice.label" default="Buying Price" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: promotionInstance, field: "buyingPrice")}</td>
			
		</tr>
		--}%
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${promotionInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.endDate.label" default="End Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${promotionInstance?.endDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${promotionInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.product.label" default="Product" /></td>
			
			<td valign="top" class="value"><g:link controller="product" action="show" id="${promotionInstance?.product?.id}">${promotionInstance?.product?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
	%{--	<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.sellingPrice.label" default="Selling Price" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: promotionInstance, field: "sellingPrice")}</td>
			
		</tr>
		--}%
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.startDate.label" default="Start Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${promotionInstance?.startDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="promotion.userPromotionGroups.label" default="User Promotion Groups" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${promotionInstance.userPromotionGroups}" var="u">
						<li><g:link controller="userPromotionGroup" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
