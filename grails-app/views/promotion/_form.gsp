<%@ page import="com.omnitech.mis.Promotion" %>



<div class="${hasErrors(bean: promotionInstance, field: 'promotionName', 'error')} ">
    <label for="promotionName" class="control-label"><g:message code="promotion.promotionName.label"
                                                                default="Promotion Name"/></label>

    <div>
        <g:textField class="form-control" style="width: 50%;" name="promotionName"
                     value="${promotionInstance?.promotionName}"/>
        <span class="help-inline">${hasErrors(bean: promotionInstance, field: 'promotionName', 'error')}</span>
    </div>
</div>

<div class="${hasErrors(bean: promotionInstance, field: 'description', 'error')} ">
    <label for="description" class="control-label"><g:message code="promotion.description.label"
                                                              default="Description"/></label>

    <div>
        <g:textField class="form-control" style="width: 50%;" name="description"
                     value="${promotionInstance?.description}"/>
        <span class="help-inline">${hasErrors(bean: promotionInstance, field: 'description', 'error')}</span>
    </div>
</div>



<div class="${hasErrors(bean: promotionInstance, field: 'product', 'error')} required">
    <label for="product" class="control-label"><g:message code="promotion.product.label" default="Product"/><span
            class="required-indicator">*</span></label>

    <div>
        <g:select class="form-control chzn-select" style="width: 50%;" id="product" name="product.id"
                  from="${com.omnitech.mis.Product.list()}" optionKey="id" required=""
                  value="${promotionInstance?.product?.id}"/>
        <span class="help-inline">${hasErrors(bean: promotionInstance, field: 'product', 'error')}</span>
    </div>
</div>

<div class="${hasErrors(bean: promotionInstance, field: 'startDate', 'error')} required">
    <label for="startDate" class="control-label"><g:message code="promotion.startDate.label" default="Start Date"/><span
            class="required-indicator">*</span></label>

    <div>
        <bs:datePicker name="startDate" precision="day" value="${promotionInstance?.startDate}"/>
        <span class="help-inline">${hasErrors(bean: promotionInstance, field: 'startDate', 'error')}</span>
    </div>
</div>


<div class="${hasErrors(bean: promotionInstance, field: 'endDate', 'error')} required">
    <label for="endDate" class="control-label"><g:message code="promotion.endDate.label" default="End Date"/><span
            class="required-indicator">*</span></label>

    <div>
        <bs:datePicker name="endDate" precision="day" value="${promotionInstance?.endDate}"/>
        <span class="help-inline">${hasErrors(bean: promotionInstance, field: 'endDate', 'error')}</span>
    </div>
</div>
