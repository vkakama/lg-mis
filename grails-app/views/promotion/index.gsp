
<%@ page import="com.omnitech.mis.Promotion" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'promotion.label', default: 'Promotion')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-promotion" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="promotionName" title="${message(code: 'promotion.promotionName.label', default: 'Promotion Name')}" />
			
			<g:sortableColumn property="description" title="${message(code: 'promotion.description.label', default: 'Description')}" />

			<g:sortableColumn property="startDate" title="${message(code: 'promotion.dateCreated.label', default: 'Start Date')}" />
			
			<g:sortableColumn property="endDate" title="${message(code: 'promotion.endDate.label', default: 'End Date')}" />

			<g:sortableColumn property="dateCreated" title="${message(code: 'promotion.dateCreated.label', default: 'Date Created')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${promotionInstanceList}" status="i" var="promotionInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${promotionInstance.id}">${fieldValue(bean: promotionInstance, field: "promotionName")}</g:link></td>
				
				<td>${fieldValue(bean: productInstance, field: "description")}</td>
				
				<td><g:formatDate date="${promotionInstance.startDate}" format="dd-MMM-yyyy" /></td>

				<td><g:formatDate date="${promotionInstance.endDate}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${promotionInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${promotionInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${promotionInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${promotionInstance?.promotionName}"
					   data-field-title="Delete Promotion"
					   title="Delete Promotion">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${promotionInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
