
<%@ page import="com.omnitech.mis.User" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}" />
    <g:set var="layout_nojambotron" value="true" scope="request"/>
    <title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-user" class="first">

    <table class="table table-bordered margin-top-medium">
        <thead>
        <tr>

            <g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}" />

            <g:sortableColumn property="name" title="${message(code: 'user.accountExpired.label', default: 'Names')}" />

            <g:sortableColumn property="telephone" title="${message(code: 'user.username.label', default: 'Telephone')}" />

            <g:sortableColumn property="dateCreated" title="${message(code: 'user.dateCreated.label', default: 'Date Created')}" />

            <g:sortableColumn property="enabled" title="${message(code: 'user.enabled.label', default: 'Enabled')}" />

        </tr>
        </thead>
        <tbody>
        <g:each in="${userInstanceList}" status="i" var="userInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

                <td>
                    ${(params.offset ? (params.offset as Long) + 1 : 1) + i}.
                    <g:link action="editChp" id="${userInstance.id}">${fieldValue(bean: userInstance, field: "username")}</g:link></td>

                <td>${fieldValue(bean: userInstance, field: "name")}</td>

                <td>${fieldValue(bean: userInstance, field: "telephone")}</td>

                <td><g:formatDate date="${userInstance.dateCreated}" format="dd-MMM-yyyy" /></td>

                <td><g:formatBoolean boolean="${userInstance.enabled}" /></td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div>
        <bs:paginate total="${userInstanceCount}" />
    </div>
</section>

</body>

</html>
