<%@ page import="com.omnitech.mis.Branch; com.omnitech.mis.User" %>
<div class="user-table">
    <div class="user-row">
        <div class="user-column">

            <div class="${hasErrors(bean: userInstance, field: 'username', 'error')} required">
                <label for="username" class="control-label"><g:message code="user.username.label" default="Username" /><span class="required-indicator">*</span></label>
                <div>
                    <g:textField class="form-control" name="username" required="" value="${userInstance?.username}"/>
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'username', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: userInstance, field: 'name', 'error')} required">
                <label for="name" class="control-label"><g:message code="user.name.label" default="Name" /><span class="required-indicator">*</span></label>
                <div>
                    <g:textField class="form-control" name="name" required="" value="${userInstance?.name}"/>
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'name', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: userInstance, field: 'name', 'telephon')} required">
                <label for="telephone" class="control-label"><g:message code="user.telephone.label" default="Telephone" /><span class="required-indicator">*</span></label>
                <div>
                    <g:textField class="form-control" name="telephone" required="" value="${userInstance?.telephone}"/>
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'telephone', 'error')}</span>
                </div>
            </div>

            <div>
                <label for="branch" class="control-label"><g:message code="user.branch.label" default="Select Branch" /></label>
                <div>
                    <g:select class="form-control chzn-select" style="width: 50%;" id="branch" name="branch.id" from="${Branch.list(sort: 'branchName',order: 'asc')}"
                              optionKey="id"  value="${userInstance?.branch?.id}"/>
                </div>
            </div>


            <div class="${hasErrors(bean: userInstance, field: 'lgUuid', 'error')} required">
                <label for="lgUuid" class="control-label"><g:message code="user.lgUuid.label" default="MM UUID" /><span class="required-indicator">*</span></label>
                <div>
                    <g:textField class="form-control" name="lgUuid" required="" value="${userInstance?.lgUuid}"/>
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'lgUuid', 'error')}</span>
                </div>
            </div>

            <g:hiddenField name="password" value="pass"/>
            <div class="${hasErrors(bean: userInstance, field: 'accountExpired', 'error')} ">
                <label for="accountExpired" class="control-label"><g:message code="user.accountExpired.label" default="Account Expired" /></label>
                <div>
                    <bs:checkBox name="accountExpired" value="${userInstance?.accountExpired}" />
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'accountExpired', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: userInstance, field: 'accountLocked', 'error')} ">
                <label for="accountLocked" class="control-label"><g:message code="user.accountLocked.label" default="Account Locked" /></label>
                <div>
                    <bs:checkBox name="accountLocked" value="${userInstance?.accountLocked}" />
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'accountLocked', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: userInstance, field: 'enabled', 'error')} ">
                <label for="enabled" class="control-label"><g:message code="user.enabled.label" default="Enabled" /></label>
                <div>
                    <bs:checkBox name="enabled" value="${userInstance?.enabled}" />
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'enabled', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: userInstance, field: 'passwordExpired', 'error')} ">
                <label for="passwordExpired" class="control-label"><g:message code="user.passwordExpired.label" default="Password Expired" /></label>
                <div>
                    <bs:checkBox name="passwordExpired" value="${userInstance?.passwordExpired}" />
                    <span class="help-inline">${hasErrors(bean: userInstance, field: 'passwordExpired', 'error')}</span>
                </div>
            </div>

        </div>


%{--        <div class="user-column">
            <div>
                <div class="user-table">
                    <div class="user-row">

                        <g:each in="${roles}" var="role" status="i">
                            <div class="user-column">
                                <g:checkBox name="roles" value="${role.id}"
                                            checked="${userInstance.hasRole(role)}"/>
                                <label for="roles[${i}]">${role.authority}</label>
                            </div>

                        </g:each>
                    </div>
                </div>
            </div>

        </div>--}%


    </div>

</div>



