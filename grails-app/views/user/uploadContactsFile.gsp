<%--
  Created by IntelliJ IDEA.
  User: kakavi
  Date: 9/11/2017
  Time: 11:34 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Upload Data</title>
    <meta name="layout" content="kickstart" />
    <g:set var="layout_nosecondarymenu" value="true" scope="request"/>
</head>

<body>

<div class="col-lg-12">

    <g:uploadForm action="importUsersFrmCSV">
        <table class="table">
            <tr>
                <td>
                    <h3>Upload Users From CSV</h3>
                </td>
            </tr>
            <tr>
                <td>
                    <span class="btn btn-default btn-file">
                        <input type="file" name="userFile"  style="width: 450px;"/>
                    </span>
                </td>
            </tr>
            <tr>
                <td>
                    <fieldset class="buttons">
                        <g:submitButton name="upload" class="save" value="Upload"/>
                    </fieldset>
                </td>
            </tr>
            <g:if test="${flash.message}">
                <tr>
                    <td>
                        <div class="message" role="status" style="color: #ff0000">${flash.message}</div>
                    </td>
                </tr>
            </g:if>

        </table>
    </g:uploadForm>

</div>
</body>
</html>