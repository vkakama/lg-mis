
<%@ page import="com.omnitech.mis.District" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'district.label', default: 'District')}" />
    <title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-district" class="first">

    <table class="table table-bordered margin-top-medium">
        <thead>
        <tr>
            
            <g:sortableColumn property="name" title="${message(code: 'district.name.label', default: 'Name')}" />
            
            <g:sortableColumn property="dateCreated" title="${message(code: 'district.dateCreated.label', default: 'Date Created')}" />
            
            <g:sortableColumn property="lastUpdated" title="${message(code: 'district.lastUpdated.label', default: 'Last Updated')}" />
            
            <td>
                Action
            </td>
        </tr>
        </thead>
        <tbody>
        <g:each in="${districtInstanceList}" status="i" var="districtInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                
                <td><g:link action="show" id="${districtInstance.id}">${fieldValue(bean: districtInstance, field: "name")}</g:link></td>
                
                <td><g:formatDate date="${districtInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
                
                <td><g:formatDate date="${districtInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
                
                <td>
                    <g:link action="edit" id="${districtInstance.id}"><i
                            class="glyphicon glyphicon-pencil"></i></g:link>


                    <a href="#deleteModal" role="button"
                       class="delete-btnz btn-delete"
                       data-target="#deleteModal"
                       data-delete-id="${districtInstance?.id}"
                       data-toggle="modal"
                       data-field-name=" Are you sure you want to delete ${districtInstance?.name}"
                       data-field-title="Delete District"
                       title="Delete District">
                        <i class="glyphicon glyphicon-remove"></i>
                    </a>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div>
        <bs:paginate total="${districtInstanceCount}" />
    </div>
    <g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
