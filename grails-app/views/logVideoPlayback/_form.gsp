<%@ page import="com.omnitech.mis.LogVideoPlayback" %>



<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'userId', 'error')} ">
	<label for="userId" class="control-label"><g:message code="logVideoPlayback.userId.label" default="User Id" /></label>
	<div>
		<g:textField class="form-control" name="userId" value="${logVideoPlaybackInstance?.userId}"/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'userId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'username', 'error')} ">
	<label for="username" class="control-label"><g:message code="logVideoPlayback.username.label" default="Username" /></label>
	<div>
		<g:textField class="form-control" name="username" value="${logVideoPlaybackInstance?.username}"/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'username', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'videoTitle', 'error')} ">
	<label for="videoTitle" class="control-label"><g:message code="logVideoPlayback.videoTitle.label" default="Video Title" /></label>
	<div>
		<g:textField class="form-control" name="videoTitle" value="${logVideoPlaybackInstance?.videoTitle}"/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'videoTitle', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'videoUUID', 'error')} ">
	<label for="videoUUID" class="control-label"><g:message code="logVideoPlayback.videoUUID.label" default="Video UUID" /></label>
	<div>
		<g:textField class="form-control" name="videoUUID" value="${logVideoPlaybackInstance?.videoUUID}"/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'videoUUID', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'sessionId', 'error')} ">
	<label for="sessionId" class="control-label"><g:message code="logVideoPlayback.sessionId.label" default="Session Id" /></label>
	<div>
		<g:textField class="form-control" name="sessionId" value="${logVideoPlaybackInstance?.sessionId}"/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'sessionId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'actionEvent', 'error')} ">
	<label for="actionEvent" class="control-label"><g:message code="logVideoPlayback.actionEvent.label" default="Action Event" /></label>
	<div>
		<g:textField class="form-control" name="actionEvent" value="${logVideoPlaybackInstance?.actionEvent}"/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'actionEvent', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logVideoPlaybackInstance, field: 'currentTimeInSeconds', 'error')} required">
	<label for="currentTimeInSeconds" class="control-label"><g:message code="logVideoPlayback.currentTimeInSeconds.label" default="Current Time In Seconds" /><span class="required-indicator">*</span></label>
	<div>
		<g:field class="form-control" style="width: 50%;" name="currentTimeInSeconds" type="number" value="${logVideoPlaybackInstance.currentTimeInSeconds}" required=""/>
		<span class="help-inline">${hasErrors(bean: logVideoPlaybackInstance, field: 'currentTimeInSeconds', 'error')}</span>
	</div>
</div>

