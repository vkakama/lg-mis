
<%@ page import="com.omnitech.mis.LogVideoPlayback" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logVideoPlayback.label', default: 'LogVideoPlayback')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-logVideoPlayback" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.userId.label" default="User Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "userId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.username.label" default="Username" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "username")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.videoTitle.label" default="Video Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "videoTitle")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.videoUUID.label" default="Video UUID" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "videoUUID")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.sessionId.label" default="Session Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "sessionId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.actionEvent.label" default="Action Event" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "actionEvent")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.currentTimeInSeconds.label" default="Current Time In Seconds" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logVideoPlaybackInstance, field: "currentTimeInSeconds")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logVideoPlaybackInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logVideoPlayback.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logVideoPlaybackInstance?.lastUpdated}" /></td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
