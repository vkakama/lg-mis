
<%@ page import="com.omnitech.mis.LogVideoPlayback" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logVideoPlayback.label', default: 'LogVideoPlayback')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-logVideoPlayback" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>

			<g:sortableColumn property="username" title="${message(code: 'logVideoPlayback.username.label', default: 'Username')}" />
			
			<g:sortableColumn property="videoTitle" title="${message(code: 'logVideoPlayback.videoTitle.label', default: 'Video Title')}" />

			<g:sortableColumn property="currentTimeInSeconds" title="${message(code: 'logVideoPlayback.actionEvent.label', default: 'Position in Video(Seconds)')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'logVideoPlayback.videoUUID.label', default: 'Date Uploaded')}" />
			
			<g:sortableColumn property="timePlayed" title="${message(code: 'logVideoPlayback.sessionId.label', default: 'Time Of Action')}" />

			<g:sortableColumn property="sessionId" title="${message(code: 'logVideoPlayback.sessionId.label', default: 'Session')}" />
			
			<g:sortableColumn property="actionEvent" title="${message(code: 'logVideoPlayback.actionEvent.label', default: 'Action Event')}" />

			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${logVideoPlaybackInstanceList}" status="i" var="logVideoPlaybackInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td>
					${(params.offset ? (params.offset as Long) + 1 : 1) + i}.
					<g:link action="show" id="${logVideoPlaybackInstance.id}">${fieldValue(bean: logVideoPlaybackInstance, field: "username")}</g:link></td>

				<td>${fieldValue(bean: logVideoPlaybackInstance, field: "videoTitle")}</td>

				<td>${fieldValue(bean: logVideoPlaybackInstance, field: "currentTimeInSeconds")}</td>
				
				<td><g:formatDate date="${logVideoPlaybackInstance.dateCreated}" format="dd-MMM-yyyy hh:mm ss" /></td>
				
				<td><g:formatDate date="${logVideoPlaybackInstance.timePlayed}" format="dd-MMM-yyyy hh:mm ss" /></td>

				<td>${fieldValue(bean: logVideoPlaybackInstance, field: "sessionId")}</td>
				
				<td>${fieldValue(bean: logVideoPlaybackInstance, field: "actionEvent")}</td>
				
				<td>
					<g:link action="edit" id="${logVideoPlaybackInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${logVideoPlaybackInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${logVideoPlaybackInstanceCount}" />
	</div>
</section>

</body>

</html>
