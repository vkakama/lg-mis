
<%@ page import="com.omnitech.mis.LogViewVideo" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewVideo.label', default: 'LogViewVideo')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-logViewVideo" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="videoTitle" title="${message(code: 'logViewVideo.videoTitle.label', default: 'Video Title')}" />
			
			<g:sortableColumn property="videoUUID" title="${message(code: 'logViewVideo.videoUUID.label', default: 'Video UUID')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'logViewVideo.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'logViewVideo.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>CHP Name</td>
			
			<g:sortableColumn property="username" title="${message(code: 'logViewVideo.username.label', default: 'Username')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${logViewVideoInstanceList}" status="i" var="logViewVideoInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td>
                    ${(params.offset ? (params.offset as Long) + 1 : 1) + i}.
                    <g:link action="show" id="${logViewVideoInstance.id}">${fieldValue(bean: logViewVideoInstance, field: "videoTitle")}</g:link></td>
				
				<td>${fieldValue(bean: logViewVideoInstance, field: "videoUUID")}</td>
				
				<td><g:formatDate date="${logViewVideoInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${logViewVideoInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>${logViewVideoInstance.chpName}</td>
				
				<td>${fieldValue(bean: logViewVideoInstance, field: "username")}</td>
				
				<td>
					<g:link action="edit" id="${logViewVideoInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${logViewVideoInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${logViewVideoInstanceCount}" />
	</div>
</section>

</body>

</html>
