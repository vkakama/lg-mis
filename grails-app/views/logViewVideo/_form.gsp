<%@ page import="com.omnitech.mis.LogViewVideo" %>



<div class="${hasErrors(bean: logViewVideoInstance, field: 'videoTitle', 'error')} ">
	<label for="videoTitle" class="control-label"><g:message code="logViewVideo.videoTitle.label" default="Video Title" /></label>
	<div>
		<g:textField class="form-control" name="videoTitle" value="${logViewVideoInstance?.videoTitle}"/>
		<span class="help-inline">${hasErrors(bean: logViewVideoInstance, field: 'videoTitle', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewVideoInstance, field: 'videoUUID', 'error')} ">
	<label for="videoUUID" class="control-label"><g:message code="logViewVideo.videoUUID.label" default="Video UUID" /></label>
	<div>
		<g:textField class="form-control" name="videoUUID" value="${logViewVideoInstance?.videoUUID}"/>
		<span class="help-inline">${hasErrors(bean: logViewVideoInstance, field: 'videoUUID', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewVideoInstance, field: 'userId', 'error')} ">
	<label for="userId" class="control-label"><g:message code="logViewVideo.userId.label" default="User Id" /></label>
	<div>
		<g:textField class="form-control" name="userId" value="${logViewVideoInstance?.userId}"/>
		<span class="help-inline">${hasErrors(bean: logViewVideoInstance, field: 'userId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewVideoInstance, field: 'username', 'error')} ">
	<label for="username" class="control-label"><g:message code="logViewVideo.username.label" default="Username" /></label>
	<div>
		<g:textField class="form-control" name="username" value="${logViewVideoInstance?.username}"/>
		<span class="help-inline">${hasErrors(bean: logViewVideoInstance, field: 'username', 'error')}</span>
	</div>
</div>

