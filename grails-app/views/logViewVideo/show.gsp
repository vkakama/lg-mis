
<%@ page import="com.omnitech.mis.LogViewVideo" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewVideo.label', default: 'LogViewVideo')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-logViewVideo" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewVideo.videoTitle.label" default="Video Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewVideoInstance, field: "videoTitle")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewVideo.videoUUID.label" default="Video UUID" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewVideoInstance, field: "videoUUID")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewVideo.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewVideoInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewVideo.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewVideoInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewVideo.userId.label" default="User Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewVideoInstance, field: "userId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewVideo.username.label" default="Username" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewVideoInstance, field: "username")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
