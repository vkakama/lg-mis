
<%@ page import="com.omnitech.mis.SavingsCalculator" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'savingsCalculator.label', default: 'SavingsCalculator')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-savingsCalculator" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="title" title="${message(code: 'savingsCalculator.title.label', default: 'Title')}" />
			
			<g:sortableColumn property="notes" title="${message(code: 'savingsCalculator.notes.label', default: 'Notes')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'savingsCalculator.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'savingsCalculator.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${savingsCalculatorInstanceList}" status="i" var="savingsCalculatorInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${savingsCalculatorInstance.id}">${fieldValue(bean: savingsCalculatorInstance, field: "title")}</g:link></td>
				
				<td>${fieldValue(bean: savingsCalculatorInstance, field: "notes")}</td>
				
				<td><g:formatDate date="${savingsCalculatorInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${savingsCalculatorInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${savingsCalculatorInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${savingsCalculatorInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${savingsCalculatorInstance?.title}"
					   data-field-title="Delete Calculator"
					   title="Delete Calculator">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${savingsCalculatorInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
