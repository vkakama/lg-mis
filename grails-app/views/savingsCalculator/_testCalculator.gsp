<!-- Modal dialog -->
<div id="testCalculator" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="testCalculatorModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="modal-title">Test Calculator</h3>
            </div>
            <div class="modal-body">

                <div style="margin-bottom: 15px;" ng-repeat="variable in variables|filter:{type:'input'}">

                    <div>
                        <label for="{{variable.variable}}" class="control-label">{{variable.text}}</label>

                        <div>
                            <input type="text" id="{{variable.variable}}" class="varinput" ng-model="variable.answer"/>
                        </div>
                    </div>
                </div>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Label</th>
                            <th>Answer</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr  ng-repeat="variable in variables|filter:{type:'output'}">
                        <td>
                             {{variable.text}}
                        </td>
                        <td>
                             {{variable.answer}}
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" ng-click="testCalculator()">Evaluate</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>