<%@ page import="com.omnitech.mis.SavingsCalculator" %>
<div class="col-md-12">

    <div class="col-md-6" style="margin-bottom: 15px;">
        <div class="col-md-12" style="background-color: #356377;color: #fff;">
            <h4>Basic Info</h4>
        </div>

        <div class="col-md-12">

            <div class="${hasErrors(bean: savingsCalculatorInstance, field: 'title', 'error')} ">
                <label for="title" class="control-label"><g:message code="savingsCalculator.title.label"
                                                                    default="Title"/></label>

                <div>
                    <g:textField class="form-control" name="title"
                                 value="${savingsCalculatorInstance?.title}" ng-model="calculater.title"/>
                    <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'title', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: savingsCalculatorInstance, field: 'notes', 'error')} ">
                <label for="notes" class="control-label"><g:message code="savingsCalculator.notes.label"
                                                                    default="Notes"/></label>

                <div>
                    <g:textField class="form-control" name="notes"
                                 value="${savingsCalculatorInstance?.notes}" ng-model="calculater.notes"/>
                    <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'notes', 'error')}</span>
                </div>
            </div>


            <div>
                <label for="products" class="control-label"><g:message code="savingsCalculatorInstance.products.label" default="Add Products" /></label>
                <div>
                    <g:select class="form-control chzn-select" name="products" from="${com.omnitech.mis.Product.list()}"
                              multiple="multiple" optionKey="id" size="5" value="${savingsCalculatorInstance?.products}" id="products"/>
                    <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'products', 'error')}</span>


                </div>
            </div>


        </div>
    </div>

    <div class="col-md-6">
        <div class="col-md-12" style="background-color: #356377;color: #fff;">
            <h4>Enter input information</h4>
        </div>

        <div class="col-md-12">
            <div class="col-md-6">
                <div>
                    <label for="variable" class="control-label">Variable</label>

                    <div>
                        <input type="text" id="variable" placeholder="Variable" ng-model="variable" class="varinput">
                    </div>
                </div>

                <div>
                    <label for="text" class="control-label">Label</label>

                    <div>
                        <textarea type="text" id="text" placeholder="Label" ng-model="text" class="varinput"></textarea>
                    </div>
                </div>

                <div>
                    <label for="type" class="control-label">Type</label>

                    <div>
                        <select id="type" ng-model="type" class="varinput">
                            <option value="input">Input</option>
                            <option value="output">Output</option>
                            <option value="price">cost of product</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6">

                <div>
                    <label for="formular" class="control-label">Formular</label>

                    <div>
                        <input type="text" id="formular" placeholder="Formular" ng-model="formular" class="varinput">
                    </div>
                </div>

                <div>
                    <label for="orderOfDisplay" class="control-label">Order of display</label>

                    <div>
                        <input type="number" id="orderOfDisplay" placeholder="Order Of Display" ng-model="orderOfDisplay" class="varinput">
                    </div>
                </div>

                <div>
                    <label for="submit" class="control-label"></label>

                    <div>
                        <input id="submit" type="button" class="add-row" value="Add Variable" ng-click="addRow()">
                    </div>
                </div>
            </div>
        </div>

    </div>

<div class="col-md-12" style="margin-bottom: 25px;">

</div>
    <div class="col-md-12">
        <div class="col-md-12" style="background-color: #356377;color: #fff;margin-bottom: 15px;">
            <h4>Calculator Variables</h4>
        </div>

        <div class="col-md-12">
            <div class="col-md-12 variable">

                <table>
                    <thead>
                    <tr>
                        <th>Variable</th>
                        <th>Text</th>
                        <th>Input type</th>
                        <th>Formular</th>
                        <th>Order of Display</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="variable in variables">
                        <td>{{variable.variable}}</td>
                        <td>{{variable.text}}</td>
                        <td>{{variable.type}}</td>
                        <td>{{variable.formular}}</td>
                        <td>{{variable.orderOfDisplay}}</td>
                        <td><button type="button" class="delete-row"
                                    ng-click="removeRow(variable.variable)">Delete Row</button></td>
                    </tr>
                    </tbody>
                </table>

            </div>
            <div class="col-md-12">
                <div class="col-md-2">
                    <input id="test" type="button" class="add-row" value="Test Calculator" data-toggle="modal" data-target="#testCalculator">
                </div>
                <div class="col-md-3">
                    <input id="save-calculator" type="button" class="add-row" value="Save Variables" ng-click="saveCalculator()">
                </div>
            </div>
        </div>
    </div>
</div>