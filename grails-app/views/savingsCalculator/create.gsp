<%@ page import="com.omnitech.mis.SavingsCalculator" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart"/>
    <g:set var="entityName" value="${message(code: 'savingsCalculator.label', default: 'SavingsCalculator')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <g:set var="layout_nojambotron" value="true" scope="request"/>
    <r:require module="angular"/>
    <r:require module="exprEngine"/>
    <g:javascript src="SavingsCalculatorCtrl.js"/>

    <style type="text/css">
    form {
        margin: 20px 0;
    }

    form input, button,select {
        padding: 5px;
    }

    .varinput{
        width: 100%;
    }

    table {
        width: 100%;
        margin-bottom: 20px;
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid #cdcdcd;
    }

    table th, table td {
        padding: 10px;
        text-align: left;
    }
    </style>
</head>

<body>
<div ng-app="omnitechApp"  ng-controller="savingsCtrl">
    <section id="create-savingsCalculator" class="first">

        <g:hasErrors bean="${savingsCalculatorInstance}">
            <div class="alert alert-danger">
                <g:renderErrors bean="${savingsCalculatorInstance}" as="list"/>
            </div>
        </g:hasErrors>

        <g:form action="save" class="form-horizontal" role="form">
            <g:render template="form"/>

           %{-- <div class="form-actions margin-top-medium">
                <g:submitButton name="create" class="btn btn-primary"
                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset"/></button>
            </div>--}%
        </g:form>

    </section>
    <g:render template="testCalculator"/>
</div>

<r:require modules="chosen,angular_chosen"/>
<g:javascript >
    $(".chzn-select").chosen();
</g:javascript>
</body>

</html>
