
<%@ page import="com.omnitech.mis.SavingsCalculator" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'savingsCalculator.label', default: 'SavingsCalculator')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="show-savingsCalculator" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="savingsCalculator.title.label" default="Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: savingsCalculatorInstance, field: "title")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="savingsCalculator.notes.label" default="Notes" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: savingsCalculatorInstance, field: "notes")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="savingsCalculator.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${savingsCalculatorInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="savingsCalculator.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${savingsCalculatorInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="savingsCalculator.productCategory.label" default="Product Category" /></td>
			
			<td valign="top" class="value"><g:link controller="productCategory" action="show" id="${savingsCalculatorInstance?.productCategory?.id}">${savingsCalculatorInstance?.productCategory?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="savingsCalculator.variables.label" default="Variables" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${savingsCalculatorInstance.variables}" var="v">
						<li><g:link controller="calculatorVariable" action="show" id="${v.id}">${v?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
