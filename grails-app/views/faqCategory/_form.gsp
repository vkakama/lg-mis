<%@ page import="com.omnitech.mis.FaqCategory" %>



<div class="${hasErrors(bean: faqCategoryInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="faqCategory.name.label" default="Name" /></label>
	<div>
		<g:textField style="width: 50%;"  class="form-control" name="name" value="${faqCategoryInstance?.name}"/>
		<span class="help-inline">${hasErrors(bean: faqCategoryInstance, field: 'name', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: faqCategoryInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="faqCategory.description.label" default="Description" /></label>
	<div>
		<g:textField style="width: 50%;"  class="form-control" name="description" value="${faqCategoryInstance?.description}"/>
		<span class="help-inline">${hasErrors(bean: faqCategoryInstance, field: 'description', 'error')}</span>
	</div>
</div>

