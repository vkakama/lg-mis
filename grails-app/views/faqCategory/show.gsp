
<%@ page import="com.omnitech.mis.FaqCategory" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faqCategory.label', default: 'FaqCategory')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-faqCategory" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faqCategory.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: faqCategoryInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faqCategory.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: faqCategoryInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faqCategory.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${faqCategoryInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faqCategory.faqs.label" default="Faqs" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${faqCategoryInstance.faqs}" var="f">
						<li><g:link controller="faq" action="show" id="${f.id}">${f?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="faqCategory.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${faqCategoryInstance?.lastUpdated}" /></td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
