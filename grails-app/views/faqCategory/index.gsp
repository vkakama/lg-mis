
<%@ page import="com.omnitech.mis.FaqCategory" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'faqCategory.label', default: 'FaqCategory')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-faqCategory" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="name" title="${message(code: 'faqCategory.name.label', default: 'Name')}" />
			
			<g:sortableColumn property="description" title="${message(code: 'faqCategory.description.label', default: 'Description')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'faqCategory.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'faqCategory.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${faqCategoryInstanceList}" status="i" var="faqCategoryInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${faqCategoryInstance.id}">${fieldValue(bean: faqCategoryInstance, field: "name")}</g:link></td>
				
				<td>${fieldValue(bean: faqCategoryInstance, field: "description")}</td>
				
				<td><g:formatDate date="${faqCategoryInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${faqCategoryInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${faqCategoryInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>



					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${faqCategoryInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${faqCategoryInstance?.name}"
					   data-field-title="Delete Category"
					   title="Delete Category">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${faqCategoryInstanceCount}" />
	</div>
</section>
<g:render template="/_common/modals/deleteDialog"/>

</body>

</html>
