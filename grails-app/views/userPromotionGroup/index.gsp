
<%@ page import="com.omnitech.mis.UserPromotionGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userPromotionGroup.label', default: 'CHPPromotionGroup')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-userPromotionGroup" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>

			<g:sortableColumn property="name" title="${message(code: 'userPromotionGroup.name.label', default: 'Name')}" />

			<th><g:message code="userPromotionGroup.promotion.label" default="Promotion" /></th>
			
			<g:sortableColumn property="sellingPrice" title="${message(code: 'userPromotionGroup.sellingPrice.label', default: 'Selling Price')}" />
			
			<g:sortableColumn property="buyingPrice" title="${message(code: 'userPromotionGroup.buyingPrice.label', default: 'Buying Price')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'userPromotionGroup.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'userPromotionGroup.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${userPromotionGroupInstanceList}" status="i" var="userPromotionGroupInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">

				<td><g:link action="show" id="${userPromotionGroupInstance.id}">${fieldValue(bean: userPromotionGroupInstance, field: "name")}</g:link></td>

				<td>${fieldValue(bean: userPromotionGroupInstance, field: "promotion")}</td>
				
				<td>${fieldValue(bean: userPromotionGroupInstance, field: "sellingPrice")}</td>
				
				<td>${fieldValue(bean: userPromotionGroupInstance, field: "buyingPrice")}</td>
				
				<td><g:formatDate date="${userPromotionGroupInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${userPromotionGroupInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${userPromotionGroupInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${userPromotionGroupInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${userPromotionGroupInstance?.name}"
					   data-field-title="Delete Group"
					   title="Delete Group">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${userPromotionGroupInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
