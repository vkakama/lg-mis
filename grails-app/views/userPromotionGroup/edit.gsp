<%@ page import="com.omnitech.mis.UserPromotionGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userPromotionGroup.label', default: 'UserPromotionGroup')}" />
	<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>

<body>

<section id="edit-userPromotionGroup" class="first">

	<g:hasErrors bean="${userPromotionGroupInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${userPromotionGroupInstance}" as="list" />
		</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" role="form" >
	<g:hiddenField name="id" value="${userPromotionGroupInstance?.id}" />
	<g:hiddenField name="version" value="${userPromotionGroupInstance?.version}" />
	<g:hiddenField name="_method" value="PUT" />

	<g:render template="form"/>

	<div class="form-actions margin-top-medium">
		<g:actionSubmit class="btn btn-primary" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />
		<button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	</div>
	</g:form>

</section>

<r:require modules="chosen"/>
<g:javascript >
	$(".chzn-select").chosen();
</g:javascript>
</body>

</html>
