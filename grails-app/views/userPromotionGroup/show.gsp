
<%@ page import="com.omnitech.mis.UserPromotionGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userPromotionGroup.label', default: 'UserPromotionGroup')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-userPromotionGroup" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.sellingPrice.label" default="Selling Price" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userPromotionGroupInstance, field: "sellingPrice")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.buyingPrice.label" default="Buying Price" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userPromotionGroupInstance, field: "buyingPrice")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userPromotionGroupInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userPromotionGroupInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userPromotionGroupInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.promotion.label" default="Promotion" /></td>
			
			<td valign="top" class="value"><g:link controller="promotion" action="show" id="${userPromotionGroupInstance?.promotion?.id}">${userPromotionGroupInstance?.promotion?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userPromotionGroup.users.label" default="Users" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${userPromotionGroupInstance.users}" var="u">
						<li><g:link controller="user" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
