<%@ page import="com.omnitech.mis.UserPromotionGroup" %>




<div class="${hasErrors(bean: userPromotionGroupInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="userPromotionGroup.name.label" default="Name" /></label>
	<div>
		<g:textField  style="width: 50%;" class="form-control" name="name" value="${userPromotionGroupInstance?.name}"/>
		<span class="help-inline">${hasErrors(bean: userPromotionGroupInstance, field: 'name', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: userPromotionGroupInstance, field: 'promotion', 'error')} required">
	<label for="promotion" class="control-label"><g:message code="userPromotionGroup.promotion.label" default="Promotion" /><span class="required-indicator">*</span></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" id="promotion" name="promotion.id" from="${com.omnitech.mis.Promotion.list()}" optionKey="id" required="" value="${userPromotionGroupInstance?.promotion?.id}"/>
		<span class="help-inline">${hasErrors(bean: userPromotionGroupInstance, field: 'promotion', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: userPromotionGroupInstance, field: 'buyingPrice', 'error')} required">
	<label for="buyingPrice" class="control-label"><g:message code="userPromotionGroup.buyingPrice.label" default="Wholesale Price" /><span class="required-indicator">*</span></label>
	<div>
		<g:field class="form-control" style="width: 50%;" name="buyingPrice" value="${fieldValue(bean: userPromotionGroupInstance, field: 'buyingPrice')}" required=""/>
		<span class="help-inline">${hasErrors(bean: userPromotionGroupInstance, field: 'buyingPrice', 'error')}</span>
	</div>
</div>


<div class="${hasErrors(bean: userPromotionGroupInstance, field: 'sellingPrice', 'error')} required">
	<label for="sellingPrice" class="control-label"><g:message code="userPromotionGroup.sellingPrice.label" default="Retail Price" /><span class="required-indicator">*</span></label>
	<div>
		<g:field class="form-control" style="width: 50%;" name="sellingPrice" value="${fieldValue(bean: userPromotionGroupInstance, field: 'sellingPrice')}" required=""/>
		<span class="help-inline">${hasErrors(bean: userPromotionGroupInstance, field: 'sellingPrice', 'error')}</span>
	</div>
</div>

<div>
	<g:render template="../_common/fields/branch"/>
</div>

<div class="${hasErrors(bean: userPromotionGroupInstance, field: 'users', 'error')} ">
	<label for="users" class="control-label"><g:message code="userPromotionGroup.users.label" default="Add more Users" /></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" name="users" from="${com.omnitech.mis.User.list()}" multiple="multiple" optionKey="id" size="5" value="${userPromotionGroupInstance?.users*.id}"/>
		<span class="help-inline">${hasErrors(bean: userPromotionGroupInstance, field: 'users', 'error')}</span>
	</div>
</div>

