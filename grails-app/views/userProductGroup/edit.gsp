<%@ page import="com.omnitech.mis.UserProductGroup" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart"/>
    <g:set var="entityName" value="${message(code: 'userProductGroup.label', default: 'UserProductGroup')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
    <g:set var="layout_nojambotron" value="true" scope="request"/>

    <g:javascript>
        $('#adjustprices').bootstrapToggle({
            on: 'Yes',
            off: 'No'
        });

        $(document).ready(function () {
            // $("#prices").hide();
            $("#adjustprices").change(function () {
                debugger;
                $("#prices").toggle();
            });
        });
    </g:javascript>

    <style type="text/css">
    form {
        margin: 20px 0;
    }

    form input, button, select {
        padding: 5px;
    }

    .varinput {
        width: 100%;
    }

    table {
        width: 100%;
        margin-bottom: 20px;
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid #cdcdcd;
    }

    table th, table td {
        padding: 10px;
        text-align: left;
    }
    </style>

    <r:require module="angular"/>
    <g:javascript src="UserProductGroupCtrl.js"/>
    <r:require module="bootstrap_toggle"/>
</head>

<body>
<div ng-app="omnitechApp" ng-controller="userProductGroupCtrl" ng-init="grpId = '${userProductGroupInstance.id}'" >
    <section id="edit-userProductGroup" class="first">

        <g:hasErrors bean="${userProductGroupInstance}">
            <div class="alert alert-danger">
                <g:renderErrors bean="${userProductGroupInstance}" as="list"/>
            </div>
        </g:hasErrors>

        <g:form method="post" class="form-horizontal" role="form">
            <g:hiddenField name="id" value="${userProductGroupInstance?.id}"/>
            <g:hiddenField name="version" value="${userProductGroupInstance?.version}"/>
            <g:hiddenField name="_method" value="PUT"/>

            <g:render template="form"/>

        </g:form>

    </section>
</div>
<r:require modules="chosen"/>
<g:javascript>
    $(".chzn-select").chosen();
</g:javascript>
</body>

</html>
