
<%@ page import="com.omnitech.mis.UserProductGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userProductGroup.label', default: 'UserProductGroup')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-userProductGroup" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="name" title="${message(code: 'userProductGroup.name.label', default: 'Name')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'userProductGroup.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'userProductGroup.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${userProductGroupInstanceList}" status="i" var="userProductGroupInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${userProductGroupInstance.id}">${fieldValue(bean: userProductGroupInstance, field: "name")}</g:link></td>
				
				<td><g:formatDate date="${userProductGroupInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${userProductGroupInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${userProductGroupInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>

					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${userProductGroupInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${userProductGroupInstance?.name}"
					   data-field-title="Delete Group"
					   title="Delete Group">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${userProductGroupInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
