
<%@ page import="com.omnitech.mis.UserProductGroup" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userProductGroup.label', default: 'UserProductGroup')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-userProductGroup" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userProductGroup.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: userProductGroupInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userProductGroup.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userProductGroupInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userProductGroup.groupProductPrices.label" default="Group Product Prices" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${userProductGroupInstance.groupProductPrices}" var="g">
						<li><g:link controller="groupProductPrice" action="show" id="${g.id}">${g?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userProductGroup.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userProductGroupInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userProductGroup.users.label" default="Users" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${userProductGroupInstance.users}" var="u">
						<li><g:link controller="user" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
