<%@ page import="com.omnitech.mis.SavingsCalculator" %>
%{--
users by default belong to default group
products by default belong to the default group
products by default have descriminated price equal to default prices
if price is -1 then use default price
--}%

<div class="col-md-12">

    <div class="col-md-6" style="margin-bottom: 15px;">
        <div class="col-md-12" style="background-color: #356377;color: #fff;">
            <h4>Basic Info</h4>
        </div>

        <div class="col-md-12">

            <div class="${hasErrors(bean: userProductGroupInstance, field: 'name', 'error')} ">
                <label for="title" class="control-label"><g:message code="userProductGroupInstance.title.label"
                                                                    default="Group Name"/></label>

                <div>
                    <g:textField class="form-control" name="title"
                                 value="${userProductGroupInstance?.name}" ng-model="userProductGroup.name"/>
                    <span class="help-inline">${hasErrors(bean: userProductGroupInstance, field: 'name', 'error')}</span>
                </div>
            </div>


            <div>
                <label for="users" class="control-label"><g:message code="userProductGroupInstance.users.label"
                                                                    default="Add Users"/></label>

                <div>
                    <g:select class="form-control chzn-select" name="users" from="${com.omnitech.mis.User.list()}"
                              multiple="multiple" optionKey="id" size="5" value="${userProductGroupInstance?.users}"
                              id="users"/>
                    <span class="help-inline">${hasErrors(bean: userProductGroupInstance, field: 'users', 'error')}</span>

                </div>
            </div>

            <div>
                <g:render template="../_common/fields/branch"/>
            </div>

            <div>
                <g:render template="../_common/fields/tags"/>
            </div>

        </div>
    </div>

    <div class="col-md-6">
        <div class="col-md-12" style="background-color: #356377;color: #fff;">
            <h4>Add Product Price</h4>
        </div>

        <div class="col-md-12">
            <div class="col-md-12">
                <div>
                    <label for="product" class="control-label">Product</label>

                    <div>
                        <select id="product" ng-model="selectedProduct"
                                ng-options="product as product.name for product in products">
                            <option value="">Select Product</option>
                        </select>
                    </div>
                </div>
            </div>

    %{--        <div class="col-md-12" id="adj">
                <div>
                    <label for="adjustprices" class="control-label">Set different Prices for this Product</label>

                    <div>
                        --}%%{--<bs:checkBox name="exlusive" value="" onLabel="Yes" offLabel="No"/>--}%%{--
                        <input id="adjustprices" name="test" data-toggle="toggle" type="checkbox">
                    </div>
                </div>

            </div>--}%

            <div class="col-md-12" id="prices">
                <div class="col-md-6">

                    <div>
                        <label for="wholesale" class="control-label">Wholesale Price</label>

                        <div>
                            <input type="text" id="wholesale" placeholder="Wholesale price" ng-model="wholeSalePrice"
                                   class="varinput">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">

                    <div>
                        <label for="retailprice" class="control-label">Retail Price</label>

                        <div>
                            <input type="text" id="retailprice" placeholder="Retail price" ng-model="retailPrice"
                                   class="varinput">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-md-offset-7">

                <div>
                    <label for="submit" class="control-label"></label>

                    <div>
                        <input id="submit" type="button" class="add-row" value="Add Product Price" ng-click="addRow()">
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-md-12" style="margin-bottom: 25px;">

    </div>

    <div class="col-md-12">
        <div class="col-md-12" style="background-color: #356377;color: #fff;margin-bottom: 15px;">
            <h4>Added Group Prices</h4>
        </div>

        <div class="col-md-12">
            <div class="col-md-12 variable">

                <table>
                    <thead>
                    <tr>
                        <th>Product</th>
                        <th>Wholesale Price</th>
                        <th>Retail Price</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="prod in productPrices">
                        <td>{{prod.product.name}}</td>
                        <td>{{prod.wholeSalePrice}}</td>
                        <td>{{prod.retailPrice}}</td>
                        <td><button type="button" class="delete-row"
                                    ng-click="removeRow(prod.product.id)">Delete Row</button></td>
                    </tr>
                    </tbody>
                </table>

            </div>

            <div class="col-md-12">
                <div class="col-md-3">
                    <input id="save-calculator" type="button" class="add-row" value="Save Group"
                           ng-click="saveUserProductGroup()">
                </div>
            </div>
        </div>
    </div>
</div>