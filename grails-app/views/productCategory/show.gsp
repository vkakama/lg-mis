
<%@ page import="com.omnitech.mis.ProductCategory" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'productCategory.label', default: 'ProductCategory')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-productCategory" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productCategory.categoryName.label" default="Category Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productCategoryInstance, field: "categoryName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productCategory.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productCategoryInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productCategory.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productCategoryInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productCategory.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productCategoryInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="productCategory.products.label" default="Products" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${productCategoryInstance.products}" var="p">
						<li><g:link controller="product" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
