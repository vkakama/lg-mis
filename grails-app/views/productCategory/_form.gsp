<%@ page import="com.omnitech.mis.ProductCategory" %>



<div class="${hasErrors(bean: productCategoryInstance, field: 'categoryName', 'error')} ">
	<label for="categoryName" class="control-label"><g:message code="productCategory.categoryName.label" default="Category Name" /></label>
	<div>
		<g:textField class="form-control" name="categoryName" value="${productCategoryInstance?.categoryName}"/>
		<span class="help-inline">${hasErrors(bean: productCategoryInstance, field: 'categoryName', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: productCategoryInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="productCategory.description.label" default="Description" /></label>
	<div>
		<g:textField class="form-control" name="description" value="${productCategoryInstance?.description}"/>
		<span class="help-inline">${hasErrors(bean: productCategoryInstance, field: 'description', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: productCategoryInstance, field: 'products', 'error')} ">
	<label for="products" class="control-label"><g:message code="productCategory.products.label" default="Products" /></label>
	<div>
		
<ul class="one-to-many">
<g:each in="${productCategoryInstance?.products?}" var="p">
    <li><g:link controller="product" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="product" action="create" params="['productCategory.id': productCategoryInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'product.label', default: 'Product')])}</g:link>
</li>
</ul>

		<span class="help-inline">${hasErrors(bean: productCategoryInstance, field: 'products', 'error')}</span>
	</div>
</div>

