
<%@ page import="com.omnitech.mis.ProductCategory" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'productCategory.label', default: 'ProductCategory')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-productCategory" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="categoryName" title="${message(code: 'productCategory.categoryName.label', default: 'Category Name')}" />
			
			<g:sortableColumn property="description" title="${message(code: 'productCategory.description.label', default: 'Description')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'productCategory.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'productCategory.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${productCategoryInstanceList}" status="i" var="productCategoryInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${productCategoryInstance.id}">${fieldValue(bean: productCategoryInstance, field: "categoryName")}</g:link></td>
				
				<td>${fieldValue(bean: productCategoryInstance, field: "description")}</td>
				
				<td><g:formatDate date="${productCategoryInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${productCategoryInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${productCategoryInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${productCategoryInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${productCategoryInstance?.categoryName}"
					   data-field-title="Delete Category"
					   title="Delete Category">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${productCategoryInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
