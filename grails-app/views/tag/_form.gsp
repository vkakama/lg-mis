<%@ page import="com.omnitech.mis.Tag" %>



<div class="${hasErrors(bean: tagInstance, field: 'name', 'error')} ">
	<label for="name" class="control-label"><g:message code="userGroup.name.label" default="Name" /></label>
	<div>
		<g:textField class="form-control" name="name" style="width: 50%;"  value="${tagInstance?.name}"/>
		<span class="help-inline">${hasErrors(bean: tagInstance, field: 'name', 'error')}</span>
	</div>
</div>


<div>
	<label for="users" class="control-label"><g:message code="tagInstance.users.label" default="Users" /></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" name="users" from="${com.omnitech.mis.User.list()}" multiple="multiple" optionKey="id" size="5" value="${users*.id}"/>
		<span class="help-inline">${hasErrors(bean: tagInstance, field: 'users', 'error')}</span>
	</div>
</div>


<div>
	<g:render template="../_common/fields/branch"/>
</div>


