
<%@ page import="com.omnitech.mis.Tag" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'tag.label', default: 'UserGroup')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
	<g:set var="layout_nojambotron" value="true" scope="request"/>
</head>

<body>

<section id="index-userGroup" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="name" title="${message(code: 'tag.name.label', default: 'Name')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'tag.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'tag.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${tagInstanceList}" status="i" var="tagInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${tagInstance.id}">${fieldValue(bean: tagInstance, field: "name")}</g:link></td>
				
				<td><g:formatDate date="${tagInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${tagInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${tagInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${tagInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${tagInstance?.name}"
					   data-field-title="Delete Group"
					   title="Delete Group">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${tagInstanceCount}" />
	</div>
</section>
<g:render template="/_common/modals/deleteDialog"/>

</body>

</html>
