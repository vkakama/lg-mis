<!-- 
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->

<%-- Only show the "Pills" navigation menu if a controller exists (but not for home) --%>
<g:if test="${	params.controller != null && params.controller != '' &&	params.controller != 'home'}">
    <div class="container" style="background: #eeeeee; padding: 5px; border-radius: 0px; border: 1px solid #ddd;">
   <ul id="Menu" class="nav nav-pills margin-top-small">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1) )}" />

	   <g:if test="${params.action in  ["chps","createChp","showChp"]}">

		   <li class="${ params.action == "chps" ? 'active' : '' }">
			   <g:link action="chps"><i class="glyphicon glyphicon-th-list"></i> <g:message code="default.list.label" args="['CHP']"/></g:link>
		   </li>

		   <li class="${ params.action == "createChp" ? 'active' : '' }">
			   <g:link action="createChp"><i class="glyphicon glyphicon-plus"></i> New CHP</g:link>
		   </li>

		   <li  class="${ params.action == "uploadContactsFile" ? 'active' : '' }">
			   <g:link action="uploadContactsFile"><i class="glyphicon glyphicon-upload"></i> Import Contacts</g:link>
		   </li>
	   </g:if>
	   <g:elseif test="${params.controller in ['logViewProduct','logViewFaq','logViewFaqCategory','logVideoPlayback','logViewVideo']}">
		   <li class="${ params.action == "index" ? 'active' : '' }">
			   <g:link action="index"><i class="glyphicon glyphicon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		   </li>

		   <li class="${ params.action == "exportToCSV" ? 'active' : '' }">
			   <g:link action="exportToCSV"><i class="glyphicon glyphicon-download"></i> Download CSV</g:link>
		   </li>
	   </g:elseif>
	   <g:elseif test="${params.controller in ['tag']}">
		   <li class="${ params.action == "index" ? 'active' : '' }">
			   <g:link action="index"><i class="glyphicon glyphicon-th-list"></i> <g:message code="default.list.label" args="['CHP Group']"/></g:link>
		   </li>

		   <li class="${ params.action == "create" ? 'active' : '' }">
			   <g:link action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label"  args="['CHP Group']"/></g:link>
		   </li>
	   </g:elseif>
	   <g:else>

		   <li class="${ params.action == "index" ? 'active' : '' }">
			   <g:link action="index"><i class="glyphicon glyphicon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		   </li>

		   <li class="${ params.action == "create" ? 'active' : '' }">
			   <g:link action="create"><i class="glyphicon glyphicon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
		   </li>
	   </g:else>
		
		<g:if test="${ params.action == 'show' || params.action == 'edit' }">
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link action="edit" id="${params.id}"><i class="glyphicon glyphicon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
			</li>
			<li class="">
				<g:render template="/_common/modals/deleteTextLink"/>
			</li>
		</g:if>
		
	</ul>
    </div>
</g:if>
