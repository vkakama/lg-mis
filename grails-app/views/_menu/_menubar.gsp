<nav role="navigation" class="navbar navbar-inverse" style="border-radius: 0px;">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <!-- Collection of nav links, forms, and other content for toggling -->
    <div id="navbarCollapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
            <li class="">
                <g:link controller="home" action="index">
                    <i class="glyphicon glyphicon-home" style="margin-right: 5px;"></i>Home</g:link>
            </li>

            <li>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i
                        class="glyphicon glyphicon-briefcase"></i>Products <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><g:link controller="product" action="index">Products List</g:link></li>
                    <li><g:link controller="product" action="create">Add Product</g:link></li>
                    <li><g:link controller="productCategory" action="index">Product Categories</g:link></li>
                    <li><g:link controller="userProductGroup" action="index">Price Descrimination</g:link></li>
                    <li><g:link controller="savingsCalculator" action="index">Savings Calculator List</g:link></li>
                    <li><g:link controller="mediaResource" action="index">Video Resources</g:link></li>

                </ul>
            </li>


            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i
                        class="glyphicon glyphicon-wrench"></i>Promotions <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><g:link controller="promotion" action="index">View Promotions</g:link></li>
                    <li><g:link controller="userPromotionGroup" action="index">CHP Promotion Groups</g:link></li>

                </ul>
            </li>


            <li>
            <li></li>

            <li>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="glyphicon glyphicon-tasks"></i>Events <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><g:link controller="event" action="index">Events List</g:link></li>
                    %{--<li><g:link controller="userEventGroup" action="index">CHP Event Group</g:link></li>--}%

                </ul>

            </li>

            <li>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <i class="glyphicon glyphicon-tasks"></i>CHPs <b class="caret"></b></a>
                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li><g:link controller="user" action="chps">CHP List</g:link></li>
                    <li><g:link controller="branch" action="index">Branches</g:link></li>
                    <li><g:link controller="tag" action="index">CHP Groups</g:link></li>
                </ul>

            </li>

            <li>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i
                        class="glyphicon glyphicon-dashboard"></i>FAQs <b class="caret"></b></a>

                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li>
                        <g:link controller="faqCategory" action="index">
                            FAQ Categories
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="faq" action="index">
                            FAQs
                        </g:link>
                    </li>
                </ul>

            </li>
            <li>
                <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i
                        class="glyphicon glyphicon-dashboard"></i>Logs <b class="caret"></b></a>

                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li>
                        <g:link controller="logViewProduct" action="index">
                            Product Acess
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="logViewFaq" action="index">
                            FAQs View
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="logViewFaqCategory" action="index">
                            FAQ Category View
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="logViewVideo" action="index">
                            Video View Logs
                        </g:link>
                    </li>
                    <li>
                        <g:link controller="logVideoPlayback" action="index">
                            Video Playback Logs
                        </g:link>
                    </li>
                </ul>

            </li>

        </ul>


        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle glyphicon glyphicon-user"
                   href="#">Users(root) <b
                        class="caret"></b></a>
                <ul role="menu" class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu">
                    <li>
                        <g:link controller="user" action="index">Users</g:link>
                    </li>

                    <li>
                        <g:link controller="role" action="index"><i class="glyphicon glyphicon-tags"></i>Roles</g:link>

                    </li>
                    <li>
                        <g:link controller="userRole" action="index"><i
                                class="glyphicon glyphicon-user"></i>User Roles</g:link>

                    </li>
                    <li>
                        <g:link controller="tag" action="index"><i
                                class="glyphicon glyphicon-user"></i>User Groups</g:link>

                    </li>

                    <li>
                        <g:link controller="requestMap" action="index"><i
                                class="glyphicon glyphicon-tags"></i>Access Levels</g:link>
                    </li>

                    <li>
                        <g:link controller="logout" action="index">
                            <i class="glyphicon glyphicon-off"></i>Log out(<span
                                style="color:#843534"><sec:loggedInUserInfo field="username"/></span>)
                        </g:link>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
