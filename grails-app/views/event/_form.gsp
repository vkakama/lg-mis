<%@ page import="com.omnitech.mis.Event" %>



<div class="${hasErrors(bean: eventInstance, field: 'title', 'error')} ">
	<label for="title" class="control-label"><g:message code="event.title.label" default="Title" /></label>
	<div>
		<g:textField style="width: 50%;" class="form-control" name="title" value="${eventInstance?.title}"/>
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'title', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: eventInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="event.description.label" default="Description" /></label>
	<div>
		<g:textField style="width: 50%;" class="form-control" name="description" value="${eventInstance?.description}"/>
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'description', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: eventInstance, field: 'venue', 'error')} ">
	<label for="venue" class="control-label"><g:message code="event.venue.label" default="Venue" /></label>
	<div>
		<g:textField style="width: 50%;" class="form-control" name="venue" value="${eventInstance?.venue}"/>
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'venue', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: eventInstance, field: 'host', 'error')} ">
	<label for="host" class="control-label"><g:message code="event.host.label" default="Host" /></label>
	<div>
		<g:textField style="width: 50%;" class="form-control" name="host" value="${eventInstance?.host}"/>
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'host', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: eventInstance, field: 'startDate', 'error')} required">
	<label for="startDate" class="control-label"><g:message code="event.startDate.label" default="Start Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="startDate" precision="day"  value="${eventInstance?.startDate}"  />
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'startDate', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: eventInstance, field: 'expiryDate', 'error')} required">
	<label for="expiryDate" class="control-label"><g:message code="event.expiryDate.label" default="Expiry Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="expiryDate" precision="day"  value="${eventInstance?.expiryDate}"  />
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'expiryDate', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: eventInstance, field: 'notificationDate', 'error')} required">
	<label for="notificationDate" class="control-label"><g:message code="event.notificationDate.label" default="Notification Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="notificationDate" precision="day"  value="${eventInstance?.notificationDate}"  />
		<span class="help-inline">${hasErrors(bean: eventInstance, field: 'notificationDate', 'error')}</span>
	</div>
</div>

<div>
	<label for="tags" class="control-label"><g:message code="tags.label" default="Select Groups" /></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" name="tags" from="${com.omnitech.mis.Tag.list(sort: 'name',order: 'asc')}"
				  multiple="multiple" optionKey="id" size="5" value="${tags*.id}" id="tags"/>
	</div>
</div>


