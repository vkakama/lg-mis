
<%@ page import="com.omnitech.mis.Event" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-event" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="title" title="${message(code: 'event.title.label', default: 'Title')}" />
			
			<g:sortableColumn property="description" title="${message(code: 'event.description.label', default: 'Description')}" />
			
			<g:sortableColumn property="venue" title="${message(code: 'event.venue.label', default: 'Venue')}" />

			<g:sortableColumn property="venue" title="${message(code: 'event.venue.label', default: 'Venue')}" />

			<g:sortableColumn property="dateCreated" title="${message(code: 'event.dateCreated.label', default: 'Start Date')}" />
			
			<g:sortableColumn property="expiryDate" title="${message(code: 'event.expiryDate.label', default: 'Expiry Date')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'event.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${eventInstanceList}" status="i" var="eventInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${eventInstance.id}">${fieldValue(bean: eventInstance, field: "title")}</g:link></td>
				
				<td>${fieldValue(bean: eventInstance, field: "description")}</td>
				
				<td>${fieldValue(bean: eventInstance, field: "venue")}</td>

				<td>${fieldValue(bean: eventInstance, field: "host")}</td>
				
				<td><g:formatDate date="${eventInstance.startDate}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${eventInstance.expiryDate}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${eventInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${eventInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>


					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${eventInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${eventInstance?.title}"
					   data-field-title="Delete Event"
					   title="Delete Event">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${eventInstanceCount}" />
	</div>
</section>
<g:render template="/_common/modals/deleteDialog"/>

</body>

</html>
