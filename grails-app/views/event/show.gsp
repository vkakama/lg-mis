
<%@ page import="com.omnitech.mis.Event" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'event.label', default: 'Event')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-event" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.title.label" default="Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: eventInstance, field: "title")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: eventInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.venue.label" default="Venue" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: eventInstance, field: "venue")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${eventInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.expiryDate.label" default="Expiry Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${eventInstance?.expiryDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${eventInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.notificationDate.label" default="Notification Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${eventInstance?.notificationDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="event.userEvents.label" default="CHP Groups" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${eventInstance.eventTags}" var="u">
						<li><g:link controller="eventTag" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
