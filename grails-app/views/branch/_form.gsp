<%@ page import="com.omnitech.mis.Branch" %>



<div class="${hasErrors(bean: branchInstance, field: 'branchName', 'error')} ">
	<label for="branchName" class="control-label"><g:message code="branch.branchName.label" default="Branch Name" /></label>
	<div>
		<g:textField class="form-control" style="width: 50%;"  name="branchName" value="${branchInstance?.branchName}"/>
		<span class="help-inline">${hasErrors(bean: branchInstance, field: 'branchName', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: branchInstance, field: 'location', 'error')} ">
	<label for="location" class="control-label"><g:message code="branch.location.label" default="District" /></label>
	<div>
		<g:textField  id="location" class="form-control" style="width: 50%;"  name="location" value="${branchInstance?.location}"/>
		<span class="help-inline">${hasErrors(bean: branchInstance, field: 'location', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: branchInstance, field: 'users', 'error')} ">
	<label for="users" class="control-label"><g:message code="branch.users.label" default="Users" /></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" name="users" from="${com.omnitech.mis.User.list()}" multiple="multiple" optionKey="id" size="5" value="${branchInstance?.users*.id}"/>
		<span class="help-inline">${hasErrors(bean: branchInstance, field: 'users', 'error')}</span>
	</div>
</div>

