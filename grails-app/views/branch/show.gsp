
<%@ page import="com.omnitech.mis.Branch" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'branch.label', default: 'Branch')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-branch" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="branch.branchName.label" default="Branch Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: branchInstance, field: "branchName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="branch.location.label" default="Location" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: branchInstance, field: "location")}</td>
			
		</tr>

		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="branch.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${branchInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="branch.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${branchInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="branch.users.label" default="Users" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${branchInstance.users}" var="u">
						<li><g:link controller="user" action="show" id="${u.id}">${u?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
