
<%@ page import="com.omnitech.mis.Branch" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'branch.label', default: 'Branch')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-branch" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="branchName" title="${message(code: 'branch.branchName.label', default: 'Branch Name')}" />
			
			<g:sortableColumn property="location" title="${message(code: 'branch.location.label', default: 'Location')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'branch.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'branch.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${branchInstanceList}" status="i" var="branchInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${branchInstance.id}">${fieldValue(bean: branchInstance, field: "branchName")}</g:link></td>
				
				<td>${fieldValue(bean: branchInstance, field: "location")}</td>
				
				<td><g:formatDate date="${branchInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${branchInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${branchInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>

					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${branchInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${branchInstance?.branchName}"
					   data-field-title="Delete Branch"
					   title="Delete Branch">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${branchInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
