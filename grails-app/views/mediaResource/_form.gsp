<%@ page import="com.omnitech.mis.MediaResource" %>



<div class="${hasErrors(bean: mediaResourceInstance, field: 'title', 'error')} ">
	<label for="title" class="control-label"><g:message code="mediaResource.title.label" default="Title" /></label>
	<div>
		<g:textField class="form-control" name="title" value="${mediaResourceInstance?.title}"/>
		<span class="help-inline">${hasErrors(bean: mediaResourceInstance, field: 'title', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: mediaResourceInstance, field: 'description', 'error')} ">
	<label for="description" class="control-label"><g:message code="mediaResource.description.label" default="Description" /></label>
	<div>
		<g:textField class="form-control" name="description" value="${mediaResourceInstance?.description}"/>
		<span class="help-inline">${hasErrors(bean: mediaResourceInstance, field: 'description', 'error')}</span>
	</div>
</div>

%{--<div class="${hasErrors(bean: mediaResourceInstance, field: 'resourceUrl', 'error')} ">
	<label for="resourceUrl" class="control-label"><g:message code="mediaResource.resourceUrl.label" default="Resource Url" /></label>
	<div>
		<g:textField class="form-control" name="resourceUrl" value="${mediaResourceInstance?.resourceUrl}"/>
		<span class="help-inline">${hasErrors(bean: mediaResourceInstance, field: 'resourceUrl', 'error')}</span>
	</div>
</div>--}%


<div class="${hasErrors(bean: mediaResourceInstance, field: 'resourceUrl', 'error')} ">
	<label for="resourceUrl2" class="control-label"><g:message code="product.image.label"
														default="Upload Video"/></label>

	<div>
		<input type="file" name="resourceUrl2" id="resourceUrl2"/>
		<span class="help-inline">${hasErrors(bean: mediaResourceInstance, field: 'resourceUrl', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: mediaResourceInstance, field: 'resourceType', 'error')} ">
	<label for="resourceType" class="control-label"><g:message code="mediaResource.resourceType.label" default="Resource Type" /></label>
	<div>
		<g:select class="form-control" style="width: 50%;" name="resourceType" from="${mediaResourceInstance.constraints.resourceType.inList}" value="${mediaResourceInstance?.resourceType}" valueMessagePrefix="mediaResource.resourceType" noSelection="['': '']"/>
		<span class="help-inline">${hasErrors(bean: mediaResourceInstance, field: 'resourceType', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: mediaResourceInstance, field: 'expiryDate', 'error')} required">
	<label for="expiryDate" class="control-label"><g:message code="mediaResource.expiryDate.label" default="Expiry Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="expiryDate" precision="day"  value="${mediaResourceInstance?.expiryDate}"  />
		<span class="help-inline">${hasErrors(bean: mediaResourceInstance, field: 'expiryDate', 'error')}</span>
	</div>
</div>

