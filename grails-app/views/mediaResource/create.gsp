<%@ page import="com.omnitech.mis.MediaResource" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'mediaResource.label', default: 'MediaResource')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create-mediaResource" class="first">

	<g:hasErrors bean="${mediaResourceInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${mediaResourceInstance}" as="list" />
		</div>
	</g:hasErrors>

	<g:form action="save" class="form-horizontal" enctype="multipart/form-data" >
	<g:render template="form"/>

	<div class="form-actions margin-top-medium">
		<g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
		<button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
	</div>
	</g:form>

</section>

</body>

</html>
