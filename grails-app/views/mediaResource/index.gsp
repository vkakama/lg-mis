
<%@ page import="com.omnitech.mis.MediaResource" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'mediaResource.label', default: 'MediaResource')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-mediaResource" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="title" title="${message(code: 'mediaResource.title.label', default: 'Title')}" />
			
			<g:sortableColumn property="description" title="${message(code: 'mediaResource.description.label', default: 'Description')}" />
			
			<g:sortableColumn property="resourceUrl" title="${message(code: 'mediaResource.resourceUrl.label', default: 'Resource Url')}" />
			
			<g:sortableColumn property="resourceType" title="${message(code: 'mediaResource.resourceType.label', default: 'Resource Type')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'mediaResource.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="expiryDate" title="${message(code: 'mediaResource.expiryDate.label', default: 'Expiry Date')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${mediaResourceInstanceList}" status="i" var="mediaResourceInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${mediaResourceInstance.id}">${fieldValue(bean: mediaResourceInstance, field: "title")}</g:link></td>
				
				<td>${fieldValue(bean: mediaResourceInstance, field: "description")}</td>
				
				<td>${fieldValue(bean: mediaResourceInstance, field: "resourceUrl")}</td>
				
				<td>${fieldValue(bean: mediaResourceInstance, field: "resourceType")}</td>
				
				<td><g:formatDate date="${mediaResourceInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${mediaResourceInstance.expiryDate}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${mediaResourceInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${mediaResourceInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${mediaResourceInstanceCount}" />
	</div>
</section>

</body>

</html>
