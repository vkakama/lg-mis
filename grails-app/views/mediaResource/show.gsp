
<%@ page import="com.omnitech.mis.MediaResource" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'mediaResource.label', default: 'MediaResource')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-mediaResource" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.title.label" default="Title" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: mediaResourceInstance, field: "title")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: mediaResourceInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.resourceUrl.label" default="Resource Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: mediaResourceInstance, field: "resourceUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.resourceType.label" default="Resource Type" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: mediaResourceInstance, field: "resourceType")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${mediaResourceInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.expiryDate.label" default="Expiry Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${mediaResourceInstance?.expiryDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="mediaResource.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${mediaResourceInstance?.lastUpdated}" /></td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
