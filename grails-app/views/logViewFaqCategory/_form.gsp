<%@ page import="com.omnitech.mis.LogViewFaqCategory" %>



<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'categoryId', 'error')} ">
	<label for="categoryId" class="control-label"><g:message code="logViewFaqCategory.categoryId.label" default="Category Id" /></label>
	<div>
		<g:textField class="form-control" name="categoryId" value="${logViewFaqCategoryInstance?.categoryId}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'categoryId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'categoryName', 'error')} ">
	<label for="categoryName" class="control-label"><g:message code="logViewFaqCategory.categoryName.label" default="Category Name" /></label>
	<div>
		<g:textField class="form-control" name="categoryName" value="${logViewFaqCategoryInstance?.categoryName}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'categoryName', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'branch', 'error')} ">
	<label for="branch" class="control-label"><g:message code="logViewFaqCategory.branch.label" default="Branch" /></label>
	<div>
		<g:textField class="form-control" name="branch" value="${logViewFaqCategoryInstance?.branch}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'branch', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'groups', 'error')} ">
	<label for="groups" class="control-label"><g:message code="logViewFaqCategory.groups.label" default="Groups" /></label>
	<div>
		<g:textField class="form-control" name="groups" value="${logViewFaqCategoryInstance?.groups}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'groups', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'logDate', 'error')} required">
	<label for="logDate" class="control-label"><g:message code="logViewFaqCategory.logDate.label" default="Log Date" /><span class="required-indicator">*</span></label>
	<div>
		<bs:datePicker name="logDate" precision="day"  value="${logViewFaqCategoryInstance?.logDate}"  />
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'logDate', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'userId', 'error')} ">
	<label for="userId" class="control-label"><g:message code="logViewFaqCategory.userId.label" default="User Id" /></label>
	<div>
		<g:textField class="form-control" name="userId" value="${logViewFaqCategoryInstance?.userId}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'userId', 'error')}</span>
	</div>
</div>

<div class="${hasErrors(bean: logViewFaqCategoryInstance, field: 'username', 'error')} ">
	<label for="username" class="control-label"><g:message code="logViewFaqCategory.username.label" default="Username" /></label>
	<div>
		<g:textField class="form-control" name="username" value="${logViewFaqCategoryInstance?.username}"/>
		<span class="help-inline">${hasErrors(bean: logViewFaqCategoryInstance, field: 'username', 'error')}</span>
	</div>
</div>

