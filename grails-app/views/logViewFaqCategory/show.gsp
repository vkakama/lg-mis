
<%@ page import="com.omnitech.mis.LogViewFaqCategory" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewFaqCategory.label', default: 'LogViewFaqCategory')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-logViewFaqCategory" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.categoryId.label" default="Category Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqCategoryInstance, field: "categoryId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.categoryName.label" default="Category Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqCategoryInstance, field: "categoryName")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.branch.label" default="Branch" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqCategoryInstance, field: "branch")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewFaqCategoryInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.groups.label" default="Groups" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqCategoryInstance, field: "groups")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewFaqCategoryInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.logDate.label" default="Log Date" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${logViewFaqCategoryInstance?.logDate}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.userId.label" default="User Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqCategoryInstance, field: "userId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="logViewFaqCategory.username.label" default="Username" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: logViewFaqCategoryInstance, field: "username")}</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
