
<%@ page import="com.omnitech.mis.LogViewFaqCategory" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'logViewFaqCategory.label', default: 'Faq Category Logs')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-logViewFaqCategory" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="categoryId" title="${message(code: 'logViewFaqCategory.categoryId.label', default: 'Category Id')}" />
			
			<g:sortableColumn property="categoryName" title="${message(code: 'logViewFaqCategory.categoryName.label', default: 'Category Name')}" />
			
			<g:sortableColumn property="branch" title="${message(code: 'logViewFaqCategory.branch.label', default: 'Branch')}" />
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'logViewFaqCategory.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="groups" title="${message(code: 'logViewFaqCategory.groups.label', default: 'Groups')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'logViewFaqCategory.lastUpdated.label', default: 'Last Updated')}" />
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${logViewFaqCategoryInstanceList}" status="i" var="logViewFaqCategoryInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${logViewFaqCategoryInstance.id}">${fieldValue(bean: logViewFaqCategoryInstance, field: "categoryId")}</g:link></td>
				
				<td>${fieldValue(bean: logViewFaqCategoryInstance, field: "categoryName")}</td>
				
				<td>${fieldValue(bean: logViewFaqCategoryInstance, field: "branch")}</td>
				
				<td><g:formatDate date="${logViewFaqCategoryInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td>${fieldValue(bean: logViewFaqCategoryInstance, field: "groups")}</td>
				
				<td><g:formatDate date="${logViewFaqCategoryInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>
					<g:link action="edit" id="${logViewFaqCategoryInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${logViewFaqCategoryInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${logViewFaqCategoryInstanceCount}" />
	</div>
</section>

</body>

</html>
