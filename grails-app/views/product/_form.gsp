<%@ page import="com.omnitech.mis.Product" %>
<div class="col-md-12">

    <div class="col-md-6">
        <div class="col-md-12" style="background-color: #356377;color: #fff;">
            <h4>Product Information</h4>
        </div>

        <div class="col-md-12">

            <div class="${hasErrors(bean: productInstance, field: 'name', 'error')} ">
                <label for="name" class="control-label"><g:message code="product.name.label" default="Name"/></label>

                <div>
                    <g:textField class="form-control" name="name" value="${productInstance?.name}"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'name', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'productId', 'error')} ">
                <label for="productId" class="control-label"><g:message code="product.productId.label"
                                                                        default="Product Id"/></label>

                <div>
                    <g:textField class="form-control" name="productId"
                                 value="${productInstance?.productId}"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'productId', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'description', 'error')} ">
                <label for="description" class="control-label"><g:message code="product.description.label"
                                                                          default="Description"/></label>

                <div>
                    <g:textArea class="form-control" name="description"
                                 value="${productInstance?.description}"
                        cols="4"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'description', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'wholeSalePrice', 'error')} ">
                <label for="wholeSalePrice" class="control-label"><g:message code="product.wholeSalePrice.label"
                                                                          default="Wholesale Price(UGX)"/></label>

                <div>
                    <g:textField class="form-control" name="wholeSalePrice"
                                 value="${productInstance?.wholeSalePrice}"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'wholeSalePrice', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'retailPrice', 'error')} ">
                <label for="retailPrice" class="control-label"><g:message code="product.retailPrice.label"
                                                                          default="Recommended Retail Price(UGX)"/></label>

                <div>
                    <g:textField class="form-control" name="retailPrice"
                                 value="${productInstance?.retailPrice}"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'retailPrice', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'videoUrl', 'error')} ">
                <label for="videoUrl" class="control-label"><g:message code="product.videoUrl.label"
                                                                       default="Video Url"/></label>

                <div>
                    <g:textField class="form-control" name="videoUrl"
                                 value="${productInstance?.videoUrl}"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'videoUrl', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'imagePath', 'error')} ">
                <label for="image" class="control-label"><g:message code="product.image.label"
                                                                        default="Upload image"/></label>

                <div>
                    <input type="file" name="image" id="image"/>
                    <span class="help-inline">${hasErrors(bean: documentInstance, field: 'imagePath', 'error')}</span>
                </div>
            </div>

            <div class="${hasErrors(bean: productInstance, field: 'category', 'error')} required">
                <label for="category" class="control-label"><g:message code="product.category.label"
                                                                       default="Category"/><span
                        class="required-indicator">*</span></label>

                <div>
                    <g:select class="form-control chzn-select" id="category" name="category.id"
                              from="${com.omnitech.mis.ProductCategory.list()}" optionKey="id" required=""
                              value="${productInstance?.category?.id}"/>
                    <span class="help-inline">${hasErrors(bean: productInstance, field: 'category', 'error')}</span>
                </div>
            </div>



            <div>
                <label for="tags" class="control-label"><g:message code="tags.label" default="Select Groups" /></label>
                <div>
                    <g:select class="form-control chzn-select" style="width: 50%;" name="tags" from="${com.omnitech.mis.Tag.list(sort: 'name',order: 'asc')}"
                              multiple="multiple" optionKey="id" size="5" value="${tags*.id}" id="tags"/>
                </div>
            </div>

        </div>
    </div>

</div>