
<%@ page import="com.omnitech.mis.Product" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-product" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.name.label" default="Name" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstance, field: "name")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.productId.label" default="Product Id" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstance, field: "productId")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.description.label" default="Description" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstance, field: "description")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.videoUrl.label" default="Video Url" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstance, field: "videoUrl")}</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.imagePath.label" default="Image Path" /></td>
			
			<td valign="top" class="value">${fieldValue(bean: productInstance, field: "imagePath")}</td>
			
		</tr>

		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.category.label" default="Category" /></td>
			
			<td valign="top" class="value"><g:link controller="productCategory" action="show" id="${productInstance?.category?.id}">${productInstance?.category?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${productInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.productGroups.label" default="Product Groups" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${productInstance.productTags}" var="p">
						<li><g:link controller="productTag" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="product.promotions.label" default="Promotions" /></td>
			
			<td valign="top" style="text-align: left;" class="value">
				<ul>
					<g:each in="${productInstance.promotions}" var="p">
						<li><g:link controller="promotion" action="show" id="${p.id}">${p?.encodeAsHTML()}</g:link></li>
					</g:each>
				</ul>
			</td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
