
<div class="${hasErrors(bean: savingsCalculatorInstance, field: 'title', 'error')} ">
    <label for="title" class="control-label"><g:message code="savingsCalculator.title.label" default="Title" /></label>
    <div>
        <g:textField class="form-control"  name="title" value="${savingsCalculatorInstance?.title}"/>
        <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'title', 'error')}</span>
    </div>
</div>

<div class="${hasErrors(bean: savingsCalculatorInstance, field: 'notes', 'error')} ">
    <label for="notes" class="control-label"><g:message code="savingsCalculator.notes.label" default="Notes" /></label>
    <div>
        <g:textField class="form-control"  name="notes" value="${savingsCalculatorInstance?.notes}"/>
        <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'notes', 'error')}</span>
    </div>
</div>

<div class="${hasErrors(bean: savingsCalculatorInstance, field: 'oneMonthPercentage', 'error')} required">
    <label for="oneMonthPercentage" class="control-label"><g:message code="savingsCalculator.oneMonthPercentage.label" default="One Month Percentage" /><span class="required-indicator">*</span></label>
    <div>
        <g:field class="form-control" name="oneMonthPercentage" value="${fieldValue(bean: savingsCalculatorInstance, field: 'oneMonthPercentage')}" required=""/>
        <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'oneMonthPercentage', 'error')}</span>
    </div>
</div>

<div class="${hasErrors(bean: savingsCalculatorInstance, field: 'oneYearPercentage', 'error')} required">
    <label for="oneYearPercentage" class="control-label"><g:message code="savingsCalculator.oneYearPercentage.label" default="One Year Percentage" /><span class="required-indicator">*</span></label>
    <div>
        <g:field class="form-control" name="oneYearPercentage" value="${fieldValue(bean: savingsCalculatorInstance, field: 'oneYearPercentage')}" required=""/>
        <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'oneYearPercentage', 'error')}</span>
    </div>
</div>

<div class="${hasErrors(bean: savingsCalculatorInstance, field: 'threeMonthsPercentage', 'error')} required">
    <label for="threeMonthsPercentage" class="control-label"><g:message code="savingsCalculator.threeMonthsPercentage.label" default="Three Months Percentage" /><span class="required-indicator">*</span></label>
    <div>
        <g:field class="form-control" name="threeMonthsPercentage" value="${fieldValue(bean: savingsCalculatorInstance, field: 'threeMonthsPercentage')}" required=""/>
        <span class="help-inline">${hasErrors(bean: savingsCalculatorInstance, field: 'threeMonthsPercentage', 'error')}</span>
    </div>
</div>

