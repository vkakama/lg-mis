
<%@ page import="com.omnitech.mis.Product" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
	<g:set var="layout_nojambotron" value="true" scope="request"/>
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-product" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<g:sortableColumn property="name" title="${message(code: 'product.name.label', default: 'Name')}" />
			
			<g:sortableColumn property="productId" title="${message(code: 'product.productId.label', default: 'Product Id')}" />
			
			<g:sortableColumn property="description" title="${message(code: 'product.description.label', default: 'Description')}" />
			
			<td>Image</td>

			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${productInstanceList}" status="i" var="productInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${productInstance.id}">${fieldValue(bean: productInstance, field: "name")}</g:link></td>
				
				<td>${fieldValue(bean: productInstance, field: "productId")}</td>
				
				<td>${fieldValue(bean: productInstance, field: "description")}</td>
				
				<td>
					<g:if test="${productInstance.imagePath != null}">
						<img src="${createLink(controller: 'product', action:'showImg',id: productInstance?.id)}" width="50" height="50">
					</g:if>
				</td>

				<td>
					<g:link action="edit" id="${productInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>

					<a href="#deleteModal" role="button"
					   class="delete-btnz btn-delete"
					   data-target="#deleteModal"
					   data-delete-id="${productInstance?.id}"
					   data-toggle="modal"
					   data-field-name=" Are you sure you want to delete ${productInstance?.name}"
					   data-field-title="Delete Product"
					   title="Delete Product">
						<i class="glyphicon glyphicon-remove"></i>
					</a>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${productInstanceCount}" />
	</div>
	<g:render template="/_common/modals/deleteDialog"/>
</section>

</body>

</html>
