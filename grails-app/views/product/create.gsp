<%@ page import="com.omnitech.mis.Product" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create-product" class="first">

	<g:hasErrors bean="${productInstance}">
		<div class="alert alert-danger">
			<g:renderErrors bean="${productInstance}" as="list" />
		</div>
	</g:hasErrors>

	<g:form action="save" class="form-horizontal" role="form" enctype="multipart/form-data" >
		<div class="col-md-12">
			<g:render template="form"/>
		</div>
		<div class="col-md-12 form-actions margin-top-large text-center">
			<g:submitButton name="create" class="btn btn-primary btn-lg" value="${message(code: 'default.button.create.label', default: 'Create')}" />
			<button class="btn btn-lg" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
		</div>

	</g:form>

</section>

<r:require modules="chosen"/>
<g:javascript >
	$(".chzn-select").chosen();
</g:javascript>
</body>

</html>
