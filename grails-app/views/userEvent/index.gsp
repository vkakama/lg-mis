
<%@ page import="com.omnitech.mis.UserEvent" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userEvent.label', default: 'UserEvent')}" />
	<title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-userEvent" class="first">

	<table class="table table-bordered margin-top-medium">
		<thead>
		<tr>
			
			<th><g:message code="userEvent.event.label" default="Event" /></th>
			
			<g:sortableColumn property="dateCreated" title="${message(code: 'userEvent.dateCreated.label', default: 'Date Created')}" />
			
			<g:sortableColumn property="lastUpdated" title="${message(code: 'userEvent.lastUpdated.label', default: 'Last Updated')}" />
			
			<th><g:message code="userEvent.user.label" default="User" /></th>
			
			<td>
				Action
			</td>
		</tr>
		</thead>
		<tbody>
		<g:each in="${userEventInstanceList}" status="i" var="userEventInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				
				<td><g:link action="show" id="${userEventInstance.id}">${fieldValue(bean: userEventInstance, field: "event")}</g:link></td>
				
				<td><g:formatDate date="${userEventInstance.dateCreated}" format="dd-MMM-yyyy" /></td>
				
				<td><g:formatDate date="${userEventInstance.lastUpdated}" format="dd-MMM-yyyy" /></td>
				
				<td>${fieldValue(bean: userEventInstance, field: "user")}</td>
				
				<td>
					<g:link action="edit" id="${userEventInstance.id}"><i
							class="glyphicon glyphicon-pencil"></i></g:link>
					<g:link action="delete" id="${userEventInstance.id}"><i
							class="glyphicon glyphicon-remove"></i></g:link>
				</td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div>
		<bs:paginate total="${userEventInstanceCount}" />
	</div>
</section>

</body>

</html>
