<%@ page import="com.omnitech.mis.UserEvent" %>



<div class="${hasErrors(bean: userEventInstance, field: 'event', 'error')} required">
	<label for="event" class="control-label"><g:message code="userEvent.event.label" default="Event Group" /><span class="required-indicator">*</span></label>
	<div>
		<g:select class="form-control chzn-select" style="width: 50%;" id="event" name="event.id" from="${com.omnitech.mis.Event.list()}" optionKey="id" required="" value="${userEventInstance?.event?.id}"/>
		<span class="help-inline">${hasErrors(bean: userEventInstance, field: 'event', 'error')}</span>
	</div>
</div>

<div class="required">
	<label for="users" class="control-label"><g:message code="userEvent.user.label" default="Add Users" /><span class="required-indicator">*</span></label>
	<div>
		<g:select class="form-control chzn-select" multiple="multiple"  style="width: 50%;" id="users" name="users" from="${com.omnitech.mis.User.list()}" optionKey="id" required="" value=""/>
		 </span>
	</div>
</div>

