
<%@ page import="com.omnitech.mis.UserEvent" %>
<!DOCTYPE html>
<html>

<head>
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'userEvent.label', default: 'UserEvent')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-userEvent" class="first">

	<table class="table">
		<tbody>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEvent.event.label" default="Event Group" /></td>
			
			<td valign="top" class="value"><g:link controller="event" action="show" id="${userEventInstance?.event?.id}">${userEventInstance?.event?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEvent.dateCreated.label" default="Date Created" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userEventInstance?.dateCreated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEvent.lastUpdated.label" default="Last Updated" /></td>
			
			<td valign="top" class="value"><g:formatDate date="${userEventInstance?.lastUpdated}" /></td>
			
		</tr>
		
		<tr class="prop">
			<td valign="top" class="name"><g:message code="userEvent.user.label" default="User" /></td>
			
			<td valign="top" class="value"><g:link controller="user" action="show" id="${userEventInstance?.user?.id}">${userEventInstance?.user?.encodeAsHTML()}</g:link></td>
			
		</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
