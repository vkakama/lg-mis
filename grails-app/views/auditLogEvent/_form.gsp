<%@ page import="org.codehaus.groovy.grails.plugins.orm.auditable.AuditLogEvent" %>



			<div class="${hasErrors(bean: auditLogEventInstance, field: 'actor', 'error')} ">
				<label for="actor" class="control-label"><g:message code="auditLogEvent.actor.label" default="Actor" /></label>
				<div>
					<g:textField class="form-control" name="actor" value="${auditLogEventInstance?.actor}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'actor', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'uri', 'error')} ">
				<label for="uri" class="control-label"><g:message code="auditLogEvent.uri.label" default="Uri" /></label>
				<div>
					<g:textField class="form-control" name="uri" value="${auditLogEventInstance?.uri}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'uri', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'className', 'error')} ">
				<label for="className" class="control-label"><g:message code="auditLogEvent.className.label" default="Class Name" /></label>
				<div>
					<g:textField class="form-control" name="className" value="${auditLogEventInstance?.className}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'className', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'persistedObjectId', 'error')} ">
				<label for="persistedObjectId" class="control-label"><g:message code="auditLogEvent.persistedObjectId.label" default="Persisted Object Id" /></label>
				<div>
					<g:textField class="form-control" name="persistedObjectId" value="${auditLogEventInstance?.persistedObjectId}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'persistedObjectId', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'persistedObjectVersion', 'error')} ">
				<label for="persistedObjectVersion" class="control-label"><g:message code="auditLogEvent.persistedObjectVersion.label" default="Persisted Object Version" /></label>
				<div>
					<g:field class="form-control" style="width: 50%;" name="persistedObjectVersion" type="number" value="${auditLogEventInstance.persistedObjectVersion}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'persistedObjectVersion', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'eventName', 'error')} ">
				<label for="eventName" class="control-label"><g:message code="auditLogEvent.eventName.label" default="Event Name" /></label>
				<div>
					<g:textField class="form-control" name="eventName" value="${auditLogEventInstance?.eventName}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'eventName', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'propertyName', 'error')} ">
				<label for="propertyName" class="control-label"><g:message code="auditLogEvent.propertyName.label" default="Property Name" /></label>
				<div>
					<g:textField class="form-control" name="propertyName" value="${auditLogEventInstance?.propertyName}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'propertyName', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'oldValue', 'error')} ">
				<label for="oldValue" class="control-label"><g:message code="auditLogEvent.oldValue.label" default="Old Value" /></label>
				<div>
					<g:textField class="form-control" name="oldValue" value="${auditLogEventInstance?.oldValue}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'oldValue', 'error')}</span>
				</div>
			</div>

			<div class="${hasErrors(bean: auditLogEventInstance, field: 'newValue', 'error')} ">
				<label for="newValue" class="control-label"><g:message code="auditLogEvent.newValue.label" default="New Value" /></label>
				<div>
					<g:textField class="form-control" name="newValue" value="${auditLogEventInstance?.newValue}"/>
					<span class="help-inline">${hasErrors(bean: auditLogEventInstance, field: 'newValue', 'error')}</span>
				</div>
			</div>

