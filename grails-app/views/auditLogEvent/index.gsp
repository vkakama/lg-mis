
<%@ page import="org.codehaus.groovy.grails.plugins.orm.auditable.AuditLogEvent" %>
<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="kickstart" />
    <g:set var="entityName" value="${message(code: 'auditLogEvent.label', default: 'AuditLogEvent')}" />
    <title><g:message code="default.index.label" args="[entityName]" /></title>
</head>

<body>

<section id="index-auditLogEvent" class="first">

    <table class="table table-bordered margin-top-medium">
        <thead>
        <tr>
            
            <g:sortableColumn property="actor" title="${message(code: 'auditLogEvent.actor.label', default: 'Actor')}" />
            
            <g:sortableColumn property="uri" title="${message(code: 'auditLogEvent.uri.label', default: 'Uri')}" />
            
            <g:sortableColumn property="className" title="${message(code: 'auditLogEvent.className.label', default: 'Class Name')}" />
            
            <g:sortableColumn property="persistedObjectId" title="${message(code: 'auditLogEvent.persistedObjectId.label', default: 'Persisted Object Id')}" />
            
            <g:sortableColumn property="persistedObjectVersion" title="${message(code: 'auditLogEvent.persistedObjectVersion.label', default: 'Persisted Object Version')}" />
            
            <g:sortableColumn property="eventName" title="${message(code: 'auditLogEvent.eventName.label', default: 'Event Name')}" />
            
            <td>
                Action
            </td>
        </tr>
        </thead>
        <tbody>
        <g:each in="${auditLogEventInstanceList}" status="i" var="auditLogEventInstance">
            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                
                <td><g:link action="show" id="${auditLogEventInstance.id}">${fieldValue(bean: auditLogEventInstance, field: "actor")}</g:link></td>
                
                <td>${fieldValue(bean: auditLogEventInstance, field: "uri")}</td>
                
                <td>${fieldValue(bean: auditLogEventInstance, field: "className")}</td>
                
                <td>${fieldValue(bean: auditLogEventInstance, field: "persistedObjectId")}</td>
                
                <td>${fieldValue(bean: auditLogEventInstance, field: "persistedObjectVersion")}</td>
                
                <td>${fieldValue(bean: auditLogEventInstance, field: "eventName")}</td>
                
                <td>
                    <g:link action="edit" id="${auditLogEventInstance.id}"><i
                            class="glyphicon glyphicon-pencil"></i></g:link>
                    <g:link action="delete" id="${auditLogEventInstance.id}"><i
                            class="glyphicon glyphicon-remove"></i></g:link>
                </td>
            </tr>
        </g:each>
        </tbody>
    </table>
    <div>
        <bs:paginate total="${auditLogEventInstanceCount}" />
    </div>
</section>

</body>

</html>
