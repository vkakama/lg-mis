package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EventTagController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond EventTag.list(params), model:[eventTagInstanceCount: EventTag.count()]
    }

    def show(EventTag eventTagInstance) {
        respond eventTagInstance
    }

    def create() {
        respond new EventTag(params)
    }

    @Transactional
    def save(EventTag eventTagInstance) {
        if (eventTagInstance == null) {
            notFound()
            return
        }

        if (eventTagInstance.hasErrors()) {
            respond eventTagInstance.errors, view:'create'
            return
        }

        eventTagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'eventTag.label', default: 'EventTag'), eventTagInstance.id])
                redirect eventTagInstance
            }
            '*' { respond eventTagInstance, [status: CREATED] }
        }
    }

    def edit(EventTag eventTagInstance) {
        respond eventTagInstance
    }

    @Transactional
    def update(EventTag eventTagInstance) {
        if (eventTagInstance == null) {
            notFound()
            return
        }

        if (eventTagInstance.hasErrors()) {
            respond eventTagInstance.errors, view:'edit'
            return
        }

        eventTagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EventTag.label', default: 'EventTag'), eventTagInstance.id])
                redirect eventTagInstance
            }
            '*'{ respond eventTagInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EventTag eventTagInstance) {

        if (eventTagInstance == null) {
            notFound()
            return
        }

        eventTagInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EventTag.label', default: 'EventTag'), eventTagInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'eventTag.label', default: 'EventTag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
