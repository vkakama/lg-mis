package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogVideoPlaybackController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        params.sort = 'dateCreated'
        params.order = 'desc'
        respond LogVideoPlayback.list(params), model:[logVideoPlaybackInstanceCount: LogVideoPlayback.count()]
    }

    def show(LogVideoPlayback logVideoPlaybackInstance) {
        respond logVideoPlaybackInstance
    }

    def create() {
        respond new LogVideoPlayback(params)
    }

    @Transactional
    def save(LogVideoPlayback logVideoPlaybackInstance) {
        if (logVideoPlaybackInstance == null) {
            notFound()
            return
        }

        if (logVideoPlaybackInstance.hasErrors()) {
            respond logVideoPlaybackInstance.errors, view:'create'
            return
        }

        logVideoPlaybackInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logVideoPlayback.label', default: 'LogVideoPlayback'), logVideoPlaybackInstance.id])
                redirect logVideoPlaybackInstance
            }
            '*' { respond logVideoPlaybackInstance, [status: CREATED] }
        }
    }

    def edit(LogVideoPlayback logVideoPlaybackInstance) {
        respond logVideoPlaybackInstance
    }

    @Transactional
    def update(LogVideoPlayback logVideoPlaybackInstance) {
        if (logVideoPlaybackInstance == null) {
            notFound()
            return
        }

        if (logVideoPlaybackInstance.hasErrors()) {
            respond logVideoPlaybackInstance.errors, view:'edit'
            return
        }

        logVideoPlaybackInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'LogVideoPlayback.label', default: 'LogVideoPlayback'), logVideoPlaybackInstance.id])
                redirect logVideoPlaybackInstance
            }
            '*'{ respond logVideoPlaybackInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(LogVideoPlayback logVideoPlaybackInstance) {

        if (logVideoPlaybackInstance == null) {
            notFound()
            return
        }

        logVideoPlaybackInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'LogVideoPlayback.label', default: 'LogVideoPlayback'), logVideoPlaybackInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logVideoPlayback.label', default: 'LogVideoPlayback'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def exportToCSV(){
        String query = """
            SELECT lv.username,u.`name`,u.lg_uuid as mm_uid,lv.video_title,lv.date_created,lv.action_event,lv.current_time_in_seconds,
            lv.time_played FROM log_video_playback lv
            INNER JOIN `user` u on lv.username = u.username 
        """
        DataExporter.exportToCsvByQuery("LogvideoPlayback.csv",query.toString(),response,request)
    }
}
