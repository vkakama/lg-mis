package com.omnitech.mis

import org.apache.commons.io.IOUtils
import org.openxdata.images.FileNameHashUtils

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MediaResourceController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond MediaResource.list(params), model:[mediaResourceInstanceCount: MediaResource.count()]
    }

    def show(MediaResource mediaResourceInstance) {
        respond mediaResourceInstance
    }

    def create() {
        respond new MediaResource(params)
    }

    @Transactional
    def save(MediaResource mediaResourceInstance) {
        if (mediaResourceInstance == null) {
            notFound()
            return
        }

        def video = request.getFile('resourceUrl2')
        if(!video.empty){
            String originalFileName = video.originalFilename + new Date().format("yyyy-MM-dd_hh-mm-ss") + ".mp4"
            String generatedPath = FileNameHashUtils.getHash(originalFileName)
            generatedPath += originalFileName
            log.info(generatedPath)
            def uploadPath = grailsApplication.config.uploadFolder as String
            File destnFile = new File(uploadPath + generatedPath)
            if (!destnFile.parentFile.exists()) {
                destnFile.parentFile.mkdirs()
            }
            video.transferTo(destnFile)
            mediaResourceInstance.resourceUrl = generatedPath

        }
        if (mediaResourceInstance.hasErrors()) {
            respond mediaResourceInstance.errors, view:'create'
            return
        }

        mediaResourceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mediaResource.label', default: 'MediaResource'), mediaResourceInstance.title])
                redirect mediaResourceInstance
            }
            '*' { respond mediaResourceInstance, [status: CREATED] }
        }
    }

    def edit(MediaResource mediaResourceInstance) {
        respond mediaResourceInstance
    }

    @Transactional
    def update(MediaResource mediaResourceInstance) {
        if (mediaResourceInstance == null) {
            notFound()
            return
        }

        def video = request.getFile('resourceUrl2')
        if(!video.empty){
            String originalFileName = video.originalFilename + new Date().format("yyyy-MM-dd_hh-mm-ss") + ".mp4"
            String generatedPath = FileNameHashUtils.getHash(originalFileName)
            generatedPath += originalFileName
            log.info(generatedPath)
            def uploadPath = grailsApplication.config.uploadFolder as String
            File destnFile = new File(uploadPath + generatedPath)
            if (!destnFile.parentFile.exists()) {
                destnFile.parentFile.mkdirs()
            }
            video.transferTo(destnFile)
            mediaResourceInstance.resourceUrl = generatedPath

        }

        if (mediaResourceInstance.hasErrors()) {
            respond mediaResourceInstance.errors, view:'edit'
            return
        }


        mediaResourceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'MediaResource.label', default: 'MediaResource'), mediaResourceInstance.title])
                redirect mediaResourceInstance
            }
            '*'{ respond mediaResourceInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(MediaResource mediaResourceInstance) {

        if (mediaResourceInstance == null) {
            notFound()
            return
        }

        mediaResourceInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'MediaResource.label', default: 'MediaResource'), mediaResourceInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mediaResource.label', default: 'MediaResource'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def showResourceUrl(){
        MediaResource resource = MediaResource.get(params.id)
        if (resource) {
            def path = resource.resourceUrl
            if (path) {
                def uploadPath = grailsApplication.config.uploadFolder as String
                def file = new File(uploadPath + path)
               /* if (file.exists()) {
//                    response.setHeader "Content-disposition", "attachment; filename=${file.name}.mp4"
//                    response.setHeader("Content-Length", file.length())
                    response.setContentType("application/octet-stream")
                    IOUtils.copy(file.newDataInputStream(), response.outputStream)
                    response.outputStream.flush()
                }*/
                render(file:file,contentType: 'video/mp4')
            }
        }
    }
}
