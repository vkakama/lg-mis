package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserEventGroupController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def userEventGroupService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond EventTag.list(params), model:[userEventGroupInstanceCount: EventTag.count()]
    }

    def show(EventTag userEventGroupInstance) {
        respond userEventGroupInstance
    }

    def create() {
        respond new EventTag(params)
    }

    @Transactional
    def save(EventTag userEventGroupInstance) {
        if (userEventGroupInstance == null) {
            notFound()
            return
        }
        def branches = params.branches instanceof String ? [params.branches] : params.branches
        def usersFromBranches = []
        if(branches){
            def listOfBranches = branches.collect { Branch.get(it) }
            if(!listOfBranches.isEmpty()){
                usersFromBranches = listOfBranches.users?.flatten()
            }
        }

        userEventGroupInstance = userEventGroupService.addUsersToGroup(userEventGroupInstance,usersFromBranches)

        if (userEventGroupInstance.hasErrors()) {
            respond userEventGroupInstance.errors, view:'create'
            return
        }

        userEventGroupInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userEventGroup.label', default: 'EventTag'), userEventGroupInstance.name])
                redirect userEventGroupInstance
            }
            '*' { respond userEventGroupInstance, [status: CREATED] }
        }
    }

    def edit(EventTag userEventGroupInstance) {
        respond userEventGroupInstance
    }

    @Transactional
    def update(EventTag userEventGroupInstance) {
        if (userEventGroupInstance == null) {
            notFound()
            return
        }

        if (userEventGroupInstance.hasErrors()) {
            respond userEventGroupInstance.errors, view:'edit'
            return
        }

        userEventGroupInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'EventTag.label', default: 'EventTag'), userEventGroupInstance.name])
                redirect userEventGroupInstance
            }
            '*'{ respond userEventGroupInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(EventTag userEventGroupInstance) {

        if (userEventGroupInstance == null) {
            notFound()
            return
        }

        userEventGroupInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'EventTag.label', default: 'EventTag'), userEventGroupInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userEventGroup.label', default: 'EventTag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Transactional
    def importFromUsrEventToGroups(){
        UserEvent.list().each {
            Event event = it.event
            def group = EventTag.findByName(event.title)?:new EventTag(name: event.title)
            group.addToUsers(it.user)
            group.event = it.event
            group.save(flush: true,failOnError: true)
            it.delete(flush: true)
        }
        redirect(action: 'index')
    }
}
