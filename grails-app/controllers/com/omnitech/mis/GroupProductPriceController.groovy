package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class GroupProductPriceController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond GroupProductPrice.list(params), model:[groupProductPriceInstanceCount: GroupProductPrice.count()]
    }

    def show(GroupProductPrice groupProductPriceInstance) {
        respond groupProductPriceInstance
    }

    def create() {
        respond new GroupProductPrice(params)
    }

    @Transactional
    def save(GroupProductPrice groupProductPriceInstance) {
        if (groupProductPriceInstance == null) {
            notFound()
            return
        }

        if (groupProductPriceInstance.hasErrors()) {
            respond groupProductPriceInstance.errors, view:'create'
            return
        }

        groupProductPriceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'groupProductPrice.label', default: 'GroupProductPrice'), groupProductPriceInstance.id])
                redirect groupProductPriceInstance
            }
            '*' { respond groupProductPriceInstance, [status: CREATED] }
        }
    }

    def edit(GroupProductPrice groupProductPriceInstance) {
        respond groupProductPriceInstance
    }

    @Transactional
    def update(GroupProductPrice groupProductPriceInstance) {
        if (groupProductPriceInstance == null) {
            notFound()
            return
        }

        if (groupProductPriceInstance.hasErrors()) {
            respond groupProductPriceInstance.errors, view:'edit'
            return
        }

        groupProductPriceInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'GroupProductPrice.label', default: 'GroupProductPrice'), groupProductPriceInstance.id])
                redirect groupProductPriceInstance
            }
            '*'{ respond groupProductPriceInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(GroupProductPrice groupProductPriceInstance) {

        if (groupProductPriceInstance == null) {
            notFound()
            return
        }

        groupProductPriceInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'GroupProductPrice.label', default: 'GroupProductPrice'), groupProductPriceInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'groupProductPrice.label', default: 'GroupProductPrice'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
