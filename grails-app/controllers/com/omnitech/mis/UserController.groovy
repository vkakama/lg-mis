package com.omnitech.mis

import org.springframework.web.multipart.commons.CommonsMultipartFile

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

/**
 * UserController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def userService

	def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

	def list(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond User.list(params), model:[userInstanceCount: User.count()]
    }

    def chps(Integer max){
        params.max = Math.min(max ?: 100, 100)
        def users = User.findAllWhoareCHPs(params)
        respond users,model: [userInstanceCount: users.totalCount]
    }

    def show(User userInstance) {
        respond userInstance
    }

    def showChp(User userInstance){
        respond userInstance
    }

    def create() {
        respond new User(params) ,model:[roles:Role.list()]

    }

    @Transactional
    def save(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true
        updateRoles(userInstance)

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userInstance.label', default: 'User'), userInstance.username])
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def createChp(){
        respond new User(params),model: [roles: Role.findAllByAuthority("ROLE_CHP")]
    }

    @Transactional
    def saveChp(User userInstance){

        if (userInstance == null) {
            notFound()
            return
        }
        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        UserProductGroup userProductGroup =  UserProductGroup.findByName("Default")
        userInstance.userProductGroup = userProductGroup

        userInstance.save flush:true

        params.roles = [Role.findByAuthority("ROLE_CHP").id]
        updateRoles(userInstance)
        new UserTag(tag: Tag.findByName("All Users"), user: userInstance).save(flush: true)


        redirect(action: 'chps')
    }

    @Transactional
    def updateChps(User userInstance){
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        if(!userInstance.userProductGroup){
            UserProductGroup userProductGroup =  UserProductGroup.findByName("Default")
            userInstance.userProductGroup = userProductGroup
        }

        userInstance.save flush:true

        updateRoles(userInstance)

        redirect action: 'chps'
    }

    def editChp(User userInstance){
        respond userInstance,model:[roles: Role.list()]
    }

    def edit(User userInstance) {
        respond userInstance,model:[roles:Role.list()]
    }

    @Transactional
    def update(User userInstance) {
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        updateRoles(userInstance)
        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    private updateRoles(User user){
        UserRole.removeAll(user)

        List roleIds = params.roles instanceof String ? [params.roles] : params.roles
        roleIds?.each { roleId ->
            def role = Role.get(roleId)

            UserRole.create(user,role,true)
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInstance.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
    def uploadContactsFile(){

    }

    @Transactional
    def importUsersFrmCSV(){
        CommonsMultipartFile f = request.getFile('userFile')
        if (!f || f.empty) {
            flash.message = 'file cannot be empty'
            redirect action: 'index'
            return
        }

        try {
            userService.processContacts(f.inputStream.text)
            flash.message = 'file uploaded successfully'
        } catch (Throwable ex) {
            flash.message = "$ex.message"
            log.error("Error while importing questions", ex)
        }

        redirect action: 'index'

    }

}
