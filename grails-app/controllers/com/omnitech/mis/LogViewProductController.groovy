package com.omnitech.mis

import fuzzycsv.FuzzyCSV
import org.codehaus.groovy.grails.orm.hibernate.cfg.GrailsDomainBinder

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogViewProductController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond LogViewProduct.list(params), model:[logViewProductInstanceCount: LogViewProduct.count()]
    }

    def show(LogViewProduct logViewProductInstance) {
        respond logViewProductInstance
    }

    def create() {
        respond new LogViewProduct(params)
    }

    @Transactional
    def save(LogViewProduct logViewProductInstance) {
        if (logViewProductInstance == null) {
            notFound()
            return
        }

        if (logViewProductInstance.hasErrors()) {
            respond logViewProductInstance.errors, view:'create'
            return
        }

        logViewProductInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logViewProduct.label', default: 'LogViewProduct'), logViewProductInstance.id])
                redirect logViewProductInstance
            }
            '*' { respond logViewProductInstance, [status: CREATED] }
        }
    }

    def edit(LogViewProduct logViewProductInstance) {
        respond logViewProductInstance
    }

    @Transactional
    def update(LogViewProduct logViewProductInstance) {
        if (logViewProductInstance == null) {
            notFound()
            return
        }

        if (logViewProductInstance.hasErrors()) {
            respond logViewProductInstance.errors, view:'edit'
            return
        }

        logViewProductInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'LogViewProduct.label', default: 'LogViewProduct'), logViewProductInstance.id])
                redirect logViewProductInstance
            }
            '*'{ respond logViewProductInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(LogViewProduct logViewProductInstance) {

        if (logViewProductInstance == null) {
            notFound()
            return
        }

        logViewProductInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'LogViewProduct.label', default: 'LogViewProduct'), logViewProductInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logViewProduct.label', default: 'LogViewProduct'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def exportToCSV(){
       DataExporter.exportToCSV("ProductLogsCSV.csv","log_view_product",response,request)
    }
}
