package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogViewVideoController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond LogViewVideo.list(params), model:[logViewVideoInstanceCount: LogViewVideo.count()]
    }

    def show(LogViewVideo logViewVideoInstance) {
        respond logViewVideoInstance
    }

    def create() {
        respond new LogViewVideo(params)
    }

    @Transactional
    def save(LogViewVideo logViewVideoInstance) {
        if (logViewVideoInstance == null) {
            notFound()
            return
        }

        if (logViewVideoInstance.hasErrors()) {
            respond logViewVideoInstance.errors, view:'create'
            return
        }

        logViewVideoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logViewVideo.label', default: 'LogViewVideo'), logViewVideoInstance.id])
                redirect logViewVideoInstance
            }
            '*' { respond logViewVideoInstance, [status: CREATED] }
        }
    }

    def edit(LogViewVideo logViewVideoInstance) {
        respond logViewVideoInstance
    }

    @Transactional
    def update(LogViewVideo logViewVideoInstance) {
        if (logViewVideoInstance == null) {
            notFound()
            return
        }

        if (logViewVideoInstance.hasErrors()) {
            respond logViewVideoInstance.errors, view:'edit'
            return
        }

        logViewVideoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'LogViewVideo.label', default: 'LogViewVideo'), logViewVideoInstance.id])
                redirect logViewVideoInstance
            }
            '*'{ respond logViewVideoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(LogViewVideo logViewVideoInstance) {

        if (logViewVideoInstance == null) {
            notFound()
            return
        }

        logViewVideoInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'LogViewVideo.label', default: 'LogViewVideo'), logViewVideoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logViewVideo.label', default: 'LogViewVideo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def exportToCSV(){
        String query  = """
                SELECT vd.username,u.`name`,u.lg_uuid as mm_uid,vd.video_title,vd.date_created FROM `log_view_video` vd
                INNER JOIN `user` u on vd.username = u.username 
            """
        DataExporter.exportToCsvByQuery("Log_view_videos.csv",query.toString(),response,request)
    }
}
