package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserPromotionGroupController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def userPromotionGroupService

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond UserPromotionGroup.list(params), model:[userPromotionGroupInstanceCount: UserPromotionGroup.count()]
    }

    def show(UserPromotionGroup userPromotionGroupInstance) {
        respond userPromotionGroupInstance
    }

    def create() {
        respond new UserPromotionGroup(params)
    }

    @Transactional
    def save(UserPromotionGroup userPromotionGroupInstance) {
        if (userPromotionGroupInstance == null) {
            notFound()
            return
        }

        def branches = params.branches instanceof String ? [params.branches] : params.branches
        def usersFromBranches = []
        if(branches){
            def listOfBranches = branches.collect { Branch.get(it) }
            if(!listOfBranches.isEmpty()){
                usersFromBranches = listOfBranches.users?.flatten()
            }
        }
        userPromotionGroupInstance = userPromotionGroupService.addUsersToGroup(userPromotionGroupInstance,usersFromBranches)

        if (userPromotionGroupInstance.hasErrors()) {
            respond userPromotionGroupInstance.errors, view:'create'
            return
        }

        userPromotionGroupInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userPromotionGroup.label', default: 'UserPromotionGroup'), userPromotionGroupInstance.name])
                redirect userPromotionGroupInstance
            }
            '*' { respond userPromotionGroupInstance, [status: CREATED] }
        }
    }

    def edit(UserPromotionGroup userPromotionGroupInstance) {
        respond userPromotionGroupInstance
    }

    @Transactional
    def update(UserPromotionGroup userPromotionGroupInstance) {
        if (userPromotionGroupInstance == null) {
            notFound()
            return
        }

        if (userPromotionGroupInstance.hasErrors()) {
            respond userPromotionGroupInstance.errors, view:'edit'
            return
        }

        userPromotionGroupInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserPromotionGroup.label', default: 'UserPromotionGroup'), userPromotionGroupInstance.name])
                redirect userPromotionGroupInstance
            }
            '*'{ respond userPromotionGroupInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UserPromotionGroup userPromotionGroupInstance) {

        if (userPromotionGroupInstance == null) {
            notFound()
            return
        }

        userPromotionGroupInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserPromotionGroup.label', default: 'UserPromotionGroup'), userPromotionGroupInstance.name])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userPromotionGroup.label', default: 'UserPromotionGroup'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
