package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ProductTagController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ProductTag.list(params), model:[productTagInstanceCount: ProductTag.count()]
    }

    def show(ProductTag productTagInstance) {
        respond productTagInstance
    }

    def create() {
        respond new ProductTag(params)
    }

    @Transactional
    def save(ProductTag productTagInstance) {
        if (productTagInstance == null) {
            notFound()
            return
        }

        if (productTagInstance.hasErrors()) {
            respond productTagInstance.errors, view:'create'
            return
        }

        productTagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'productTag.label', default: 'ProductTag'), productTagInstance.id])
                redirect productTagInstance
            }
            '*' { respond productTagInstance, [status: CREATED] }
        }
    }

    def edit(ProductTag productTagInstance) {
        respond productTagInstance
    }

    @Transactional
    def update(ProductTag productTagInstance) {
        if (productTagInstance == null) {
            notFound()
            return
        }

        if (productTagInstance.hasErrors()) {
            respond productTagInstance.errors, view:'edit'
            return
        }

        productTagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ProductTag.label', default: 'ProductTag'), productTagInstance.id])
                redirect productTagInstance
            }
            '*'{ respond productTagInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ProductTag productTagInstance) {

        if (productTagInstance == null) {
            notFound()
            return
        }

        productTagInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ProductTag.label', default: 'ProductTag'), productTagInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'productTag.label', default: 'ProductTag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
