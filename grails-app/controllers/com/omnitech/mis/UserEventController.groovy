package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserEventController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def eventService

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond UserEvent.list(params), model:[userEventInstanceCount: UserEvent.count()]
    }

    def show(UserEvent userEventInstance) {
        respond userEventInstance
    }

    def create() {
        respond new UserEvent(params)
    }

    @Transactional
    def save(UserEvent userEventInstance) {
        if (userEventInstance == null) {
            notFound()
            return
        }

        def branches = params.branches instanceof String ? [params.branches] : params.branches
        def usersFromView = []
        if(branches){
            def listOfBranches = branches.collect { Branch.get(it) }
            if(!listOfBranches.isEmpty()){
                usersFromView = listOfBranches.users?.flatten()
            }
        }

        if(params.users){
            usersFromView.addAll(params.users.collect{User.get(it)})
        }

        def eventInstance = userEventInstance.event

        eventInstance = eventService.addUserEvents(eventInstance, usersFromView)

        if (eventInstance.hasErrors()) {
            respond eventInstance.errors, view:'create'
            return
        }

        eventInstance.save flush:true

        redirect(action: 'index')
    }

    def edit(UserEvent userEventInstance) {
        respond userEventInstance
    }

    @Transactional
    def update(UserEvent userEventInstance) {
        if (userEventInstance == null) {
            notFound()
            return
        }

        if (userEventInstance.hasErrors()) {
            respond userEventInstance.errors, view:'edit'
            return
        }

        userEventInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserEvent.label', default: 'UserEvent'), userEventInstance.id])
                redirect userEventInstance
            }
            '*'{ respond userEventInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UserEvent userEventInstance) {

        if (userEventInstance == null) {
            notFound()
            return
        }

        userEventInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserEvent.label', default: 'UserEvent'), userEventInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userEvent.label', default: 'UserEvent'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
