package com.omnitech.mis

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class TagController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def userGroupService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Tag.list(params), model: [tagInstanceCount: Tag.count()]
    }

    def show(Tag tagInstance) {
        respond tagInstance
    }

    def create() {
        respond new Tag(params)
    }

    @Transactional
    def save(Tag tagInstance) {
        if (tagInstance == null) {
            notFound()
            return
        }
        def usersFrmView = params.users? (params.users instanceof String ? [params.users] : params.users as List):[]
        def branches = params.branches instanceof String ? [params.branches] : params.branches as List

        def usersFromBranches = []
        if (branches) {
            def listOfBranches = branches.collect { Branch.get(it) }
            if (!listOfBranches.isEmpty()) {
                usersFromBranches = listOfBranches.users?.flatten()
            }
        }

        usersFrmView.addAll(usersFromBranches)

        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view: 'create'
            return
        }

        tagInstance.save flush: true

        if (usersFrmView) {
            usersFrmView.each {
                new UserTag(tag: tagInstance, user: it).save(flush: true)
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'tag.label', default: 'Group'), tagInstance.name])
                redirect tagInstance
            }
            '*' { respond tagInstance, [status: CREATED] }
        }
    }

    def edit(Tag tagInstance) {
        respond tagInstance, model: [users: tagInstance.userTags.user]
    }

    @Transactional
    def update(Tag tagInstance) {
        if (tagInstance == null) {
            notFound()
            return
        }

        def usersFrmView = params.users instanceof String ? [params.users] : params.users as List
        def branches = params.branches instanceof String ? [params.branches] : params.branches
        if (branches) {
            def listOfBranches = branches.collect { Branch.get(it) }
            if (!listOfBranches.isEmpty()) {
                usersFrmView.addAll(listOfBranches.users?.flatten())
            }
        }

        UserTag.findAllByTag(tagInstance).each {
            it.delete(flush: true)
        }


        if (tagInstance.hasErrors()) {
            respond tagInstance.errors, view: 'edit'
            return
        }

        tagInstance.save flush: true, failOnError: true
        if (usersFrmView) {
            usersFrmView.each {
                new UserTag(tag: tagInstance, user: it).save(flush: true)
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Tag.label', default: 'Group'), tagInstance.name])
                redirect tagInstance
            }
            '*' { respond tagInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Tag tagInstance) {

        if (tagInstance == null) {
            notFound()
            return
        }

        tagInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Tag.label', default: 'Group'), tagInstance.name])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'tag.label', default: 'Group'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
