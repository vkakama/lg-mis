package com.omnitech.mis

import grails.converters.JSON
import grails.transaction.Transactional
import org.springframework.util.Assert

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class UserProductGroupController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def userProductGroupService

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond UserProductGroup.list(params), model: [userProductGroupInstanceCount: UserProductGroup.count()]
    }

    def show(UserProductGroup userProductGroupInstance) {
        respond userProductGroupInstance
    }

    def create() {
        respond new UserProductGroup(params), model: [products: Product.list(sort: 'name', order: 'asc')]
    }

    @Transactional
    def save(UserProductGroup userProductGroupInstance) {
        if (userProductGroupInstance == null) {
            notFound()
            return
        }
        def branches = params.branches instanceof String ? [params.branches] : params.branches
        def usersFromBranches = []
        if (branches) {
            def listOfBranches = branches.collect { Branch.get(it) }
            if (!listOfBranches.isEmpty()) {
                usersFromBranches = listOfBranches.users?.flatten()
            }
        }
        userProductGroupInstance = userProductGroupService.addUsersToGroup(userProductGroupInstance, usersFromBranches)
        if (userProductGroupInstance.hasErrors()) {
            respond userProductGroupInstance.errors, view: 'create'
            return
        }

        userProductGroupInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userProductGroup.label', default: 'UserProductGroup'), userProductGroupInstance.name])
                redirect userProductGroupInstance
            }
            '*' { respond userProductGroupInstance, [status: CREATED] }
        }
    }

    def edit(UserProductGroup userProductGroupInstance) {
        respond userProductGroupInstance
    }

    @Transactional
    def update(UserProductGroup userProductGroupInstance) {
        if (userProductGroupInstance == null) {
            notFound()
            return
        }

        if (userProductGroupInstance.hasErrors()) {
            respond userProductGroupInstance.errors, view: 'edit'
            return
        }

        userProductGroupInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserProductGroup.label', default: 'UserProductGroup'), userProductGroupInstance.name])
                redirect userProductGroupInstance
            }
            '*' { respond userProductGroupInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UserProductGroup userProductGroupInstance) {

        if (userProductGroupInstance == null) {
            notFound()
            return
        }

        userProductGroupInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserProductGroup.label', default: 'UserProductGroup'), userProductGroupInstance.name])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Transactional
    def saveUserProductGroup() {
        println params
        def jsonObject = request.JSON as Map
        log.info(jsonObject)
        Assert.notEmpty jsonObject, "The request must not be empty"

        def userProductGroup = new UserProductGroup(jsonObject)
        def users = jsonObject?.users?.collect{User.get(it)}
        if(jsonObject.branches){
            def branches = jsonObject?.branches?.collect{Branch.get(it)}
            branches.each {users.addAll(it.users)}
        }
        if(jsonObject.tags){
            def tags = jsonObject?.tags?.collect{Tag.get(it)}
            tags.each {tag->users.addAll(tag.userTags.collect {it.user})}
        }

        if (jsonObject.id) {
//            update
            def frmDb = UserProductGroup.get(jsonObject.id)
            frmDb.groupProductPrices.collect().each { frmDb.removeFromGroupProductPrices(it) }
            userProductGroup.groupProductPrices.each {
                it.name = userProductGroup.name
                it.chpGroup = frmDb
                if(!it.retailPrice)it.retailPrice = -1
                if(!it.wholeSalePrice)it.wholeSalePrice = -1
                it.save(flush: true)
                frmDb.addToGroupProductPrices(it)

                frmDb.save(flush: true)
            }

            users.each{
                it.userProductGroup = frmDb
                it.save(flush: true)
            }
        } else {
            userProductGroup.groupProductPrices.each {
                it.name = userProductGroup.name
                if(!it.retailPrice)it.retailPrice = -1
                if(!it.wholeSalePrice)it.wholeSalePrice = -1
                it.save(flush: true)
                userProductGroup.addToGroupProductPrices(it)
            }
            userProductGroup.save(flush: true)

            users.each{
                it.userProductGroup = userProductGroup
                it.save(flush: true)
            }
        }


        def status = [code: "200", status: "Calculator saved"]
        log.info("User Product Group ${userProductGroup.name} successfully saved...")
        render status as JSON
    }

    def loadUserproductGroup() {
        if (params.id) {
            def group = UserProductGroup.get(params.id)
            def productPrices = group.groupProductPrices.collect {
                ['name': it.name, 'product': it.product, 'wholeSalePrice': it.wholeSalePrice, 'retailPrice': it.retailPrice]
            }
            def asJson = ['id': group.id, 'name': group.name, 'productPrices': productPrices]

            render asJson as JSON
        } else {
            def status = [code: "404", status: "Not Found"]
            render status as JSON
        }
    }

    def loadProducts(){
        def products = Product.list().collect {
            ['id':it.id,'name':it.name,'wholeSalePrice':it.wholeSalePrice,'retailPrice':it.retailPrice]
        }

        render products as JSON
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userProductGroup.label', default: 'UserProductGroup'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
