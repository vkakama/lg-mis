package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class CalculatorVariableController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond CalculatorVariable.list(params), model:[calculatorVariableInstanceCount: CalculatorVariable.count()]
    }

    def show(CalculatorVariable calculatorVariableInstance) {
        respond calculatorVariableInstance
    }

    def create() {
        respond new CalculatorVariable(params)
    }

    @Transactional
    def save(CalculatorVariable calculatorVariableInstance) {
        if (calculatorVariableInstance == null) {
            notFound()
            return
        }

        if (calculatorVariableInstance.hasErrors()) {
            respond calculatorVariableInstance.errors, view:'create'
            return
        }

        calculatorVariableInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'calculatorVariable.label', default: 'CalculatorVariable'), calculatorVariableInstance.id])
                redirect calculatorVariableInstance
            }
            '*' { respond calculatorVariableInstance, [status: CREATED] }
        }
    }

    def edit(CalculatorVariable calculatorVariableInstance) {
        respond calculatorVariableInstance
    }

    @Transactional
    def update(CalculatorVariable calculatorVariableInstance) {
        if (calculatorVariableInstance == null) {
            notFound()
            return
        }

        if (calculatorVariableInstance.hasErrors()) {
            respond calculatorVariableInstance.errors, view:'edit'
            return
        }

        calculatorVariableInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'CalculatorVariable.label', default: 'CalculatorVariable'), calculatorVariableInstance.id])
                redirect calculatorVariableInstance
            }
            '*'{ respond calculatorVariableInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(CalculatorVariable calculatorVariableInstance) {

        if (calculatorVariableInstance == null) {
            notFound()
            return
        }

        calculatorVariableInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'CalculatorVariable.label', default: 'CalculatorVariable'), calculatorVariableInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'calculatorVariable.label', default: 'CalculatorVariable'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
