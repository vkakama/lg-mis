package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogViewFaqController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond LogViewFaq.list(params), model:[logViewFaqInstanceCount: LogViewFaq.count()]
    }

    def show(LogViewFaq logViewFaqInstance) {
        respond logViewFaqInstance
    }

    def create() {
        respond new LogViewFaq(params)
    }

    @Transactional
    def save(LogViewFaq logViewFaqInstance) {
        if (logViewFaqInstance == null) {
            notFound()
            return
        }

        if (logViewFaqInstance.hasErrors()) {
            respond logViewFaqInstance.errors, view:'create'
            return
        }

        logViewFaqInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logViewFaq.label', default: 'LogViewFaq'), logViewFaqInstance.id])
                redirect logViewFaqInstance
            }
            '*' { respond logViewFaqInstance, [status: CREATED] }
        }
    }

    def edit(LogViewFaq logViewFaqInstance) {
        respond logViewFaqInstance
    }

    @Transactional
    def update(LogViewFaq logViewFaqInstance) {
        if (logViewFaqInstance == null) {
            notFound()
            return
        }

        if (logViewFaqInstance.hasErrors()) {
            respond logViewFaqInstance.errors, view:'edit'
            return
        }

        logViewFaqInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'LogViewFaq.label', default: 'LogViewFaq'), logViewFaqInstance.id])
                redirect logViewFaqInstance
            }
            '*'{ respond logViewFaqInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(LogViewFaq logViewFaqInstance) {

        if (logViewFaqInstance == null) {
            notFound()
            return
        }

        logViewFaqInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'LogViewFaq.label', default: 'LogViewFaq'), logViewFaqInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logViewFaq.label', default: 'LogViewFaq'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def exportToCSV(){
        DataExporter.exportToCSV("LogviewFaq.csv","log_view_faq",response,request)
    }
}
