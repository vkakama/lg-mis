package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FaqController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond Faq.list(params), model:[faqInstanceCount: Faq.count()]
    }

    def show(Faq faqInstance) {
        respond faqInstance
    }

    def create() {
        respond new Faq(params)
    }

    @Transactional
    def save(Faq faqInstance) {
        if (faqInstance == null) {
            notFound()
            return
        }

        if (faqInstance.hasErrors()) {
            respond faqInstance.errors, view:'create'
            return
        }

        faqInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'faq.label', default: 'Faq'), faqInstance.question])
                redirect faqInstance
            }
            '*' { respond faqInstance, [status: CREATED] }
        }
    }

    def edit(Faq faqInstance) {
        respond faqInstance
    }

    @Transactional
    def update(Faq faqInstance) {
        if (faqInstance == null) {
            notFound()
            return
        }

        if (faqInstance.hasErrors()) {
            respond faqInstance.errors, view:'edit'
            return
        }

        faqInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Faq.label', default: 'Faq'), faqInstance.question])
                redirect faqInstance
            }
            '*'{ respond faqInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Faq faqInstance) {

        if (faqInstance == null) {
            notFound()
            return
        }

        faqInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Faq.label', default: 'Faq'), faqInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'faq.label', default: 'Faq'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
