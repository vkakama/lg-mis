package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LogViewFaqCategoryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond LogViewFaqCategory.list(params), model:[logViewFaqCategoryInstanceCount: LogViewFaqCategory.count()]
    }

    def show(LogViewFaqCategory logViewFaqCategoryInstance) {
        respond logViewFaqCategoryInstance
    }

    def create() {
        respond new LogViewFaqCategory(params)
    }

    @Transactional
    def save(LogViewFaqCategory logViewFaqCategoryInstance) {
        if (logViewFaqCategoryInstance == null) {
            notFound()
            return
        }

        if (logViewFaqCategoryInstance.hasErrors()) {
            respond logViewFaqCategoryInstance.errors, view:'create'
            return
        }

        logViewFaqCategoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'logViewFaqCategory.label', default: 'LogViewFaqCategory'), logViewFaqCategoryInstance.id])
                redirect logViewFaqCategoryInstance
            }
            '*' { respond logViewFaqCategoryInstance, [status: CREATED] }
        }
    }

    def edit(LogViewFaqCategory logViewFaqCategoryInstance) {
        respond logViewFaqCategoryInstance
    }

    @Transactional
    def update(LogViewFaqCategory logViewFaqCategoryInstance) {
        if (logViewFaqCategoryInstance == null) {
            notFound()
            return
        }

        if (logViewFaqCategoryInstance.hasErrors()) {
            respond logViewFaqCategoryInstance.errors, view:'edit'
            return
        }

        logViewFaqCategoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'LogViewFaqCategory.label', default: 'LogViewFaqCategory'), logViewFaqCategoryInstance.id])
                redirect logViewFaqCategoryInstance
            }
            '*'{ respond logViewFaqCategoryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(LogViewFaqCategory logViewFaqCategoryInstance) {

        if (logViewFaqCategoryInstance == null) {
            notFound()
            return
        }

        logViewFaqCategoryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'LogViewFaqCategory.label', default: 'LogViewFaqCategory'), logViewFaqCategoryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'logViewFaqCategory.label', default: 'LogViewFaqCategory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def exportToCSV(){
        DataExporter.exportToCSV("LogviewFaqCategory.csv","log_view_faq_category",response,request)
    }
}
