package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class FaqCategoryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond FaqCategory.list(params), model:[faqCategoryInstanceCount: FaqCategory.count()]
    }

    def show(FaqCategory faqCategoryInstance) {
        respond faqCategoryInstance
    }

    def create() {
        respond new FaqCategory(params)
    }

    @Transactional
    def save(FaqCategory faqCategoryInstance) {
        if (faqCategoryInstance == null) {
            notFound()
            return
        }

        if (faqCategoryInstance.hasErrors()) {
            respond faqCategoryInstance.errors, view:'create'
            return
        }

        faqCategoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'faqCategory.label', default: 'FaqCategory'), faqCategoryInstance.name])
                redirect faqCategoryInstance
            }
            '*' { respond faqCategoryInstance, [status: CREATED] }
        }
    }

    def edit(FaqCategory faqCategoryInstance) {
        respond faqCategoryInstance
    }

    @Transactional
    def update(FaqCategory faqCategoryInstance) {
        if (faqCategoryInstance == null) {
            notFound()
            return
        }

        if (faqCategoryInstance.hasErrors()) {
            respond faqCategoryInstance.errors, view:'edit'
            return
        }

        faqCategoryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'FaqCategory.label', default: 'FaqCategory'), faqCategoryInstance.name])
                redirect faqCategoryInstance
            }
            '*'{ respond faqCategoryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(FaqCategory faqCategoryInstance) {

        if (faqCategoryInstance == null) {
            notFound()
            return
        }

        faqCategoryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'FaqCategory.label', default: 'FaqCategory'), faqCategoryInstance.name])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'faqCategory.label', default: 'FaqCategory'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
