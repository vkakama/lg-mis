package com.omnitech.mis

import grails.converters.JSON
import groovy.json.JsonOutput

class HomeController {

    def index(){
        render(view: "index")
    }

    def queryProductData() {
        def query = LogViewProduct.createCriteria().list(){
            projections{
                property("branch")
                property("logDate")
                property("categoryName")
                property("productName")
                count("productName","count")
                groupProperty("productName")
                groupProperty("categoryName")
                groupProperty("branch")
                groupProperty("logDate")
            }
        }
      def finalResult = []
        query.collect { item ->
            def results = [:]
            results['branch'] = item.getAt(0)
            results['date'] = item.getAt(1)
            results['category'] = item.getAt(2)
            results['product'] = item.getAt(3)
            results['count'] = item.getAt(4)

            finalResult << results
        }
        render([contentType:"application/json", text: finalResult as JSON])

    }


    def queryFaqData(){

        def query = LogViewFaq.createCriteria().list {
            projections {
                property("branch")
                property("logDate")
                property("categoryName")
                property("question")
                count("question","count")
                groupProperty("question")
                groupProperty("categoryName")
                groupProperty("branch")
                groupProperty("logDate")
            }
        }

        def finalResult = []
        query.collect { item ->
            def results = [:]
            results['branch'] = item.getAt(0)
            results['date'] = item.getAt(1)
            results['category'] = item.getAt(2)
            results['question'] = item.getAt(3)
            results['count'] = item.getAt(4)

            finalResult << results
        }
        render([contentType:"application/json", text: finalResult as JSON])
    }
}
