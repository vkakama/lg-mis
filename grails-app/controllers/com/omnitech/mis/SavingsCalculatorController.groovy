package com.omnitech.mis

import grails.converters.JSON
import org.springframework.util.Assert

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SavingsCalculatorController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SavingsCalculator.list(params), model:[savingsCalculatorInstanceCount: SavingsCalculator.count()]
    }

    def show(SavingsCalculator savingsCalculatorInstance) {
        respond savingsCalculatorInstance
    }

    def create() {
        respond new SavingsCalculator(params)
    }

    @Transactional
    def save(SavingsCalculator savingsCalculatorInstance) {
        if (savingsCalculatorInstance == null) {
            notFound()
            return
        }

        if (savingsCalculatorInstance.hasErrors()) {
            respond savingsCalculatorInstance.errors, view:'create'
            return
        }

        savingsCalculatorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'savingsCalculator.label', default: 'SavingsCalculator'), savingsCalculatorInstance.id])
                redirect savingsCalculatorInstance
            }
            '*' { respond savingsCalculatorInstance, [status: CREATED] }
        }
    }

    def edit(SavingsCalculator savingsCalculatorInstance) {
        respond savingsCalculatorInstance
    }

    @Transactional
    def update(SavingsCalculator savingsCalculatorInstance) {
        if (savingsCalculatorInstance == null) {
            notFound()
            return
        }

        if (savingsCalculatorInstance.hasErrors()) {
            respond savingsCalculatorInstance.errors, view:'edit'
            return
        }

        savingsCalculatorInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SavingsCalculator.label', default: 'SavingsCalculator'), savingsCalculatorInstance.id])
                redirect savingsCalculatorInstance
            }
            '*'{ respond savingsCalculatorInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SavingsCalculator savingsCalculatorInstance) {

        if (savingsCalculatorInstance == null) {
            notFound()
            return
        }

        savingsCalculatorInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SavingsCalculator.label', default: 'SavingsCalculator'), savingsCalculatorInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'savingsCalculator.label', default: 'SavingsCalculator'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def loadProducts(){
        def products = Product.list().collect {
            ['id':it.id,'name':it.name]
        }

        render products as JSON
    }

    def loadVariables(){
        if(params.id){
            def calculator = SavingsCalculator.get(params.id)
            def variables = calculator.variables
            def asJson = variables.collect {
                ['text':it.text,'variable':it.variable,'type':it.type,
                'formular':it.formular,'orderOfDisplay':it.orderOfDisplay]
            }

            def cal = ['id':calculator.id,'title':calculator.title,'notes':calculator.notes,'variables':asJson]

            render cal as JSON
        }else{
            def status = [code:"404",status: "Not Found"]
            render status as JSON
        }
    }

    @Transactional
    def saveCalculator(){
        println params
        def jsonObject = request.JSON as Map
        log.info(jsonObject)
        Assert.notEmpty jsonObject,"The request must not be empty"
        def title = jsonObject.title
        def notes = jsonObject.notes
        def products = jsonObject.products?.collect{Product.get(it)}
        def variables = jsonObject.variables?.collect{new CalculatorVariable(formular: it.formular,
                orderOfDisplay: it.orderOfDisplay,variable: it.variable,text: it.text,type: it.type)}

        def calculator = new SavingsCalculator(title: title,notes: notes).save(flush: true)

        variables.each {
            it.calculator = calculator
            it.save(flush: true)
        }

        products.each {
            it.savingsCalculator = calculator
            it.save(flush: true)
        }

        def status = [code:"200",status: "Calculator saved"]
        log.info("Calculator ${title} successfully saved...")
        render status as JSON
    }
}
