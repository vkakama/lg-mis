package com.omnitech.mis

import com.omnitech.lgprotobuf.ClientRequest
import com.omnitech.lgprotobuf.ServerResponse
import org.h2.tools.Server

/**
 * Created by user on 6/8/2017.
 */
class WireController {

    def wireService

    def index() {
        try {
            def request = ClientRequest.ADAPTER.decode(request.inputStream)

            log.info("New client Request:${request.request} User[$request.username]")

            def user = wireService.findChpUser(request.username)

            if (!request.username || !user) {
                log.error("Username Missing: $request")
                sendError("Username or Telephone($request.username) Not Found")
            } else {
                processRequest(request, user)
            }
        } catch (Exception x) {
            log.error("Error occurred while processing wire request(${request})", x)
            sendError(x.message)

        }
    }

    private flush() { response.outputStream.flush() }

    private sendError(String message) {
        def sr = new ServerResponse.Builder().status(ServerResponse.STATUS.FAILURE)
                                             .message(message)
                                             .build()

        ServerResponse.ADAPTER.encode(response.outputStream, sr)
        flush()

    }


    private processRequest(ClientRequest request, User user) {
        switch (request.request) {
            case ClientRequest.RequestType.CATEGORY_LIST:
                categories(request, user)
                break
            case ClientRequest.RequestType.EVENT_LIST:
                events(request, user)
                break
            case ClientRequest.RequestType.FAQ_LIST:
                faqCategories(request, user)
                break
            case ClientRequest.RequestType.SAVING_CALCULATOR_LIST:
                savingCalculatorList(request, user)
                break
            case ClientRequest.RequestType.AUTHENTICATE:
                sendUserInfo(user)
                break
            case ClientRequest.RequestType.UPLOAD_PRODUCT_LOG:
                processProductLogs(request, user)
                break
            case ClientRequest.RequestType.UPLOAD_FAQ_LOG:
                processFaqLogs(request, user)
                break
            case ClientRequest.RequestType.UPLOAD_FAQ_CATEGORY_LOG:
                processFaqCategoryLogs(request, user)
                break
            case ClientRequest.RequestType.VIEW_VIDEO_LOG:
                processViewVideoLogs(request,user)
                break
            case ClientRequest.RequestType.VIDEO_PLAY_BACK:
                processVideoPlayBackLogs(request,user)
                break
            case ClientRequest.RequestType.MEDIA_RESOURCE_LIST:
                resources(request,user)
                break
            default:
                notFound()

        }
    }

    private def sendUserInfo(User user) {
        def userInfo = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
                                                   .user_id(user.id)
                                                   .username(user.username)
                                                   .human_name(user.name)
                                                   .message("Success")
                                                   .build()

        ServerResponse.ADAPTER.encode(response.outputStream, userInfo)
        flush()
    }

    private def savingCalculatorList(ClientRequest clientRequest, User user) {
        def calculatorListMsg = wireService.buildSavingCalculators(user)

        def serverResponse = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
                                                         .message("Success")
                                                         .saving_calculator_list(calculatorListMsg)
                                                         .build()

        ServerResponse.ADAPTER.encode(response.outputStream, serverResponse)
        flush()
    }

    private def events(ClientRequest request, User user) {
        def eventListMsg = wireService.buildEvents(user)

        def serverResponse = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
                                                         .message("Success")
                                                         .event_list(eventListMsg)
                                                         .build()

        ServerResponse.ADAPTER.encode(response.outputStream, serverResponse)
        flush()
    }

    private def resources(ClientRequest request, User user) {
        def resourceListMsg = wireService.buildResources(user)

        def serverResponse = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
        .message("Success")
        .resource_list(resourceListMsg)
        .build()
        ServerResponse.ADAPTER.encode(response.outputStream,serverResponse)
        flush()
    }


    private categories(ClientRequest request, User user) {

        def categoryListMsg = wireService.buildProductCategories(user)

        def serverResponse = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
                                                         .message("Success")
                                                         .product_category_list(categoryListMsg)
                                                         .build()

        ServerResponse.ADAPTER.encode(response.outputStream, serverResponse)
        flush()
    }

    private faqCategories(ClientRequest request, User user) {
        def faqCategories = wireService.buildFaqCategories(user)

        def serverResponse = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
                                                         .message("Success")
                                                         .faq_category_list(faqCategories)
                                                         .build()

        ServerResponse.ADAPTER.encode(response.outputStream, serverResponse)
        flush()
    }

    private processProductLogs(ClientRequest request, User user) {
        wireService.processProductLogs(request, user)
        sendSuccess()

    }

    private processFaqLogs(ClientRequest request, User user) {
        wireService.processFaqLogs(request, user)
        sendSuccess()

    }

    private processFaqCategoryLogs(ClientRequest request, User user) {
        wireService.processFaqCategoryLogs(request, user)
        sendSuccess()

    }

    private void sendSuccess() {
        def serverResponse = new ServerResponse.Builder().status(ServerResponse.STATUS.SUCCESS)
                                                         .message("Success")
                                                         .build()
        ServerResponse.ADAPTER.encode(response.outputStream, serverResponse)
        flush()

    }

    private notFound() {
        sendError("Unknown request")
    }


    def processVideoPlayBackLogs(ClientRequest clientRequest, User user) {
        wireService.processVideoPlaybackLogs(clientRequest,user)
        sendSuccess()
    }

    def processViewVideoLogs(ClientRequest clientRequest, User user) {
        wireService.processViewVideoLogs(clientRequest,user)
        sendSuccess()
    }


}
