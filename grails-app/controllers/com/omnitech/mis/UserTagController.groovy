package com.omnitech.mis



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class UserTagController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond UserTag.list(params), model:[userTagInstanceCount: UserTag.count()]
    }

    def show(UserTag userTagInstance) {
        respond userTagInstance
    }

    def create() {
        respond new UserTag(params)
    }

    @Transactional
    def save(UserTag userTagInstance) {
        if (userTagInstance == null) {
            notFound()
            return
        }

        if (userTagInstance.hasErrors()) {
            respond userTagInstance.errors, view:'create'
            return
        }

        userTagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userTag.label', default: 'UserTag'), userTagInstance.id])
                redirect userTagInstance
            }
            '*' { respond userTagInstance, [status: CREATED] }
        }
    }

    def edit(UserTag userTagInstance) {
        respond userTagInstance
    }

    @Transactional
    def update(UserTag userTagInstance) {
        if (userTagInstance == null) {
            notFound()
            return
        }

        if (userTagInstance.hasErrors()) {
            respond userTagInstance.errors, view:'edit'
            return
        }

        userTagInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'UserTag.label', default: 'UserTag'), userTagInstance.id])
                redirect userTagInstance
            }
            '*'{ respond userTagInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(UserTag userTagInstance) {

        if (userTagInstance == null) {
            notFound()
            return
        }

        userTagInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'UserTag.label', default: 'UserTag'), userTagInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userTag.label', default: 'UserTag'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
