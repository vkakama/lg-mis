package com.omnitech.mis

import grails.transaction.Transactional
import org.apache.commons.io.IOUtils
import org.openxdata.images.FileNameHashUtils
import org.openxdata.images.ImageUtil

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class ProductController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]
    def productService

    def index(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        respond Product.list(params), model: [productInstanceCount: Product.count()]
    }

    def show(Product productInstance) {
        respond productInstance
    }

    def create() {
        respond new Product(params)
    }

    @Transactional
    def save(Product productInstance) {
        if (productInstance == null) {
            notFound()
            return
        }
        def tags = params.tags instanceof String ? [params.tags] : params.tags
        def image = request.getFile('image')
        if (!image.empty) {
            def imageBytes = ImageUtil.resizeImageAsJPG(image.bytes, 600)

            String originalFileName = image.originalFilename + new Date().format("yyyy-MM-dd_hh-mm-ss") + ".jpg"
            String generatedPath = FileNameHashUtils.getHash(originalFileName)
            generatedPath += originalFileName
            log.debug(generatedPath)

            def uploadPath = grailsApplication.config.uploadFolder as String
            productInstance.imagePath = generatedPath
            File destnFile = new File(uploadPath + generatedPath)
            if (!destnFile.parentFile.exists()) {
                destnFile.parentFile.mkdirs()
            }
            destnFile.bytes = imageBytes
        }

        productInstance.validate()

        if (productInstance.hasErrors()) {
            respond productInstance.errors, view: 'create'
            return
        }

        productInstance.save flush: true, failOnError: true

        if (tags) {
            tags.each {
                Tag tag = Tag.get(it)
                new ProductTag(tag: tag, product: productInstance).save(flush: true)
            }
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'product.label', default: 'Product'), productInstance.name])
                redirect productInstance
            }
            '*' { respond productInstance, [status: CREATED] }
        }
    }

    def edit(Product productInstance) {
        respond productInstance, model: [tags: productInstance.productTags.tag]
    }

    @Transactional
    def update(Product productInstance) {
        if (productInstance == null) {
            notFound()
            return
        }

        def image = request.getFile('image')
        if (!image.empty) {
            def imageBytes = ImageUtil.resizeImageAsJPG(image.bytes, 600)

            String originalFileName = image.originalFilename + new Date().format("yyyy-MM-dd_hh-mm-ss") + ".jpg"
            String generatedPath = FileNameHashUtils.getHash(originalFileName)
            generatedPath += originalFileName
            log.debug(generatedPath)

            def uploadPath = grailsApplication.config.uploadFolder as String
            productInstance.imagePath = generatedPath
            File destnFile = new File(uploadPath + generatedPath)
            if (!destnFile.parentFile.exists()) {
                destnFile.parentFile.mkdirs()
            }
            destnFile.bytes = imageBytes
        }

        if (productInstance.hasErrors()) {
            respond productInstance.errors, view: 'edit'
            return
        }

        productInstance.save flush: true

        ProductTag.findAllByProduct(productInstance).each { it.delete(flush: true) }

        def tags = params.tags instanceof String ? [params.tags] : params.tags
        if (tags) {
            tags.each {
                Tag tag = Tag.get(it)
                new ProductTag(tag: tag, product: productInstance).save(flush: true)
            }
        }


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Product.label', default: 'Product'), productInstance.name])
                redirect productInstance
            }
            '*' { respond productInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Product productInstance) {

        if (productInstance == null) {
            notFound()
            return
        }

        productInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Product.label', default: 'Product'), productInstance.name])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'product.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }

    def showImg() {
        Product product = Product.get(params.id)
        if (product) {
            def path = product.imagePath
            if (path) {
                def uploadPath = grailsApplication.config.uploadFolder as String
                def file = new File(uploadPath + path)
                if (file.exists()) {
                    IOUtils.copy(file.newDataInputStream(), response.outputStream)
                    response.outputStream.flush()
                }
            }
        }
    }

    def resizeImg() {
        def uploadPath = grailsApplication.config.uploadFolder as String
        def folder = new File(uploadPath)
        folder.eachDirRecurse { fold ->
            fold.eachFile { file ->
                println file.canonicalPath
                if (!file.isDirectory()) {
                    def imageBytes = ImageUtil.resizeImageAsJPG(file.bytes, 600)
                    file.bytes = imageBytes
                }
            }
        }
        redirect action: 'index'
    }
}
