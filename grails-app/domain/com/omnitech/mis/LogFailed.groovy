package com.omnitech.mis

class LogFailed {

    String id

    String type
    String msg
    String errorMsg

    Date dateCreated
    Date lastUpdated


    static mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
        type nullable: true
        msg type: 'text', nullable: true
        errorMsg type: 'text', nullable: true
    }
}
