package com.omnitech.mis

/**
 * LogViewFaq
 * A domain class describes the data object and it's mapping to the database
 */
class LogViewFaq {

    /* Default (injected) attributes of GORM */
    String id
    String userId
    String username
    String branch
    String groups
    Date   logDate


    String question
    String questionId
    String categoryId
    String categoryName


    Date dateCreated
    Date lastUpdated


    static constraints = {
        question nullable: false
        questionId nullable: false
        categoryId nullable: false
        categoryName nullable: false
    }

    static mapping = {
        id generator: 'assigned'
        userId index: 'LogViewFaq_uid_idx'
        username index: 'LogViewFaq_username_idx'
        branch index: 'LogViewFaq_branch_idx'
        logDate index: 'LogViewFaq_logDate_idx'

        questionId index: 'LogViewFaq_questionId_idx'
        categoryId index: 'LogViewFaq_categoryId_idx'
    }


    @Override
    public String toString() {
        return "${question}";
    }
}
