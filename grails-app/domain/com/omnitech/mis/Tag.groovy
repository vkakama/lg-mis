package com.omnitech.mis

/**
 * Tag
 * A domain class describes the data object and it's mapping to the database
 */
class Tag {

    String id
    String name
    Date	dateCreated
    Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
	static	hasMany		= [userTags:UserTag,productTags:ProductTag,eventTags:EventTag]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static	mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
    }

    static	constraints = {
        name nullable: false
    }

    /*
     * Methods of the Domain Class
     */
    @Override	// Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${name}";
    }

/*    def beforeDelete() {
        User.withNewSession {
            User.findAllByTag(this).each {
                it.tag = null
                it.save(flush: true)
            }
        }
    }*/
}
