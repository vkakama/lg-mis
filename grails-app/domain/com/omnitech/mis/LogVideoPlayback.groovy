package com.omnitech.mis

import org.grails.databinding.BindingFormat

/**
 * LogVideoPlayback
 * A domain class describes the data object and it's mapping to the database
 */
class LogVideoPlayback {

    /* Default (injected) attributes of GORM */
    String id
    String userId
    String username
    String videoTitle
    String videoUUID
    String sessionId
    String actionEvent
    Long currentTimeInSeconds
    Date timePlayed
//	Long	version

    /* Automatic timestamping of GORM */
    Date	dateCreated
    Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static	mapping = {
        id generator: 'assigned'
        userId index: 'LogVideoPlayback_userid_idx'
        username index: 'LogVideoPlayback_username_idx'
        videoTitle index: 'LogVideoPlayback_video_title_idx'
        videoUUID index: "LogVideoPlayback_video_uuid_idx"
        sessionId index: 'LogVideoPlayback_sessionid_idx'
        actionEvent index: 'LogVideoPlayback_action_event_idx'
    }

    static	constraints = {
        userId nullable: false
        username nullable: false
        videoTitle nullable: false
        videoUUID nullable: false
        sessionId nullable: false
        actionEvent nullable: false
    }

    /*
     * Methods of the Domain Class
     */
    @Override	// Override toString for a nicer / more descriptive UI
    String toString() {
        return "${videoTitle}"
    }
}
