package com.omnitech.mis

import org.springframework.web.multipart.commons.CommonsMultipartFile

class User {

    transient springSecurityService

    String id
    String lgUuid
    String username
    String password
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired

    Date dateCreated
    Date lastUpdated

    String name
    Branch branch
    UserProductGroup userProductGroup
    String telephone



    static auditable = false


    static transients = ['springSecurityService']


    static mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32, class: String)
        password column: 'password'
    }

    static constraints = {
        username blank: false, unique: true
        password blank: false, password: true
        name nullable: true
        branch nullable: true
        userProductGroup nullable: true
        telephone nullable: true
        lgUuid nullable: true
    }

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this).collect { it.role } as Set
    }

    boolean hasRole(Role r) {
        if (id == null)
            return false
        return authorities.any { r.authority == it.authority }
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService.encodePassword(password)
    }

    @Override
    // Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${name}-${username}";
    }

    static List<User > findAllWhoareCHPs(def params) {
        /*def users = UserRole.findAllByRole(Role.findByAuthority("ROLE_CHP")).collect { it.user } as Set
        users = users.sort { it.username }
        return users*/
       return createCriteria().list(params) {
           isNotNull('branch')
        }
    }

    List<Product> findProducts(ProductCategory productCategory) {
        return UserProductGroup.findUserProductGroups(this).findAll{it.product.category == productCategory}.collect {it.product}
    }

    List<UserProductGroup> findUserGroup(){
        return UserProductGroup.createCriteria().list {
            users{
                eq 'username',username
            }
        }
    }

    List<Tag> findAllTags() {
        return UserTag.findAllByUser(this)*.tag
    }

    String getCommaSeparatedTags() {
        def tags = findAllTags()
        if (!tags) return 'N/A'
        return tags*.name.unique().join(',')
    }
}
