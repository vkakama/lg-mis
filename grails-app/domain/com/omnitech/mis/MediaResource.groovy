package com.omnitech.mis

import org.grails.databinding.BindingFormat

/**
 * MediaResource
 * A domain class describes the data object and it's mapping to the database
 */
class MediaResource {

    /* Default (injected) attributes of GORM */
    String id
    String title
    String description
    String resourceUrl
    String resourceType
    @BindingFormat('yyyy-MM-dd')
    Date expiryDate

    /* Automatic timestamping of GORM */
    Date	dateCreated
    Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static	mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
        type: 'text'
    }

    static	constraints = {
        title nullable: false
        description nullable: true
        resourceUrl nullable: true
        resourceType nullable: false,inList: ['Video','Audio']
    }

    /*
     * Methods of the Domain Class
     */
    @Override	// Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${title}";
    }
}
