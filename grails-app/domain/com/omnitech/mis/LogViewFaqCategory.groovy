package com.omnitech.mis

class LogViewFaqCategory {

    String id
    String userId
    String username
    String branch
    String groups
    Date   logDate


    String categoryId
    String categoryName


    Date dateCreated
    Date lastUpdated


    static constraints = {
        categoryId nullable: false
        categoryName nullable: false
    }

    static mapping = {
        id generator: 'assigned'

        userId index: 'LogViewFaqCategory_uid_idx'
        username index: 'LogViewFaqCategory_username_idx'
        branch index: 'LogViewFaqCategory_branch_idx'
        logDate index: 'LogViewFaqCategory_logDate_idx'

        categoryId index: 'LogViewFaqCategory_categoryId_idx'
    }

    @Override
    public String toString() {
        return "${categoryName}";
    }
}
