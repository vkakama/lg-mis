package com.omnitech.mis
/**
 * Product
 * A domain class describes the data object and it's mapping to the database
 */
class Product {

    String id
    String productId
    String name
    String description
    //todo add buying price
    BigDecimal wholeSalePrice
    BigDecimal retailPrice
    String videoUrl
    String imagePath
    Date dateCreated
    Date lastUpdated
    SavingsCalculator savingsCalculator

    static belongsTo = [category: ProductCategory]
    // tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
    static hasMany = [promotions: Promotion, productGroupPrices: GroupProductPrice, productTags: ProductTag]
    // tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
        description type: 'text'
        savingsCalculator ignoreNotFound: true
    }

    static constraints = {
        name nullable: false
        productId unique: true
        description nullable: false
        videoUrl nullable: true
        imagePath nullable: true
        wholeSalePrice(scale: 1)
        retailPrice(scale: 1)
        savingsCalculator nullable: true
    }

/*
     * Methods of the Domain Class
     */

    @Override
    // Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${name}";
    }

    static def findAllByTag(Tag tag) {
        return createCriteria().list {
            productTags {
                eq 'tag', tag
            }
        }
    }

    Promotion getRunningPromotion() {
//        there will always be one running product promotion
        promotions.find {
            it.endDate.after(new Date())
        }
    }

    static def userProducts(User user) {
        def users = [user]
        return createCriteria().list() {
            productGroups {
                users {
                    eq 'username', user.username
                }
            }
        }
    }

    def findProductPrice(UserProductGroup userProductGroup) {
        this.productGroupPrices.find { it.chpGroup == userProductGroup }
    }

}
