package com.omnitech.mis

class LogViewProduct {

    String id
    String userId
    String username
    String branch
    String groups
    Date   logDate

    String categoryId
    String categoryName
    String productId
    String productName


    Date dateCreated
    Date lastUpdated


    static constraints = {
        categoryId nullable: false
        categoryName nullable: false
        productName nullable: false
        productId nullable: false
    }


    static mapping = {
        id generator: 'assigned'

        userId index: 'LogViewProduct_uid_idx'
        username index: 'LogViewProduct_username_idx'
        branch index: 'LogViewProduct_branch_idx'
        logDate index: 'LogViewProduct_logDate_idx'

        categoryId index: 'LogViewProduct_categoryId_idx'
        productId index: 'LogViewProduct_productId_idx'
    }


    @Override
    public String toString() {
        return "${productName}";
    }
}
