package com.omnitech.mis

import org.grails.databinding.BindingFormat

/**
 * Event
 * A domain class describes the data object and it's mapping to the database
 */
class Event {

    String id
    String title
    String description
    String venue
    String host
    @BindingFormat('yyyy-MM-dd')
    Date expiryDate
    @BindingFormat('yyyy-MM-dd')
    Date notificationDate
    @BindingFormat('yyyy-MM-dd')
    Date startDate

    /* Automatic timestamping of GORM */
    Date dateCreated
    Date lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
    static hasMany = [userEvents: UserEvent, eventTags: EventTag]
    // tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
    }

    static constraints = {
        title nullable: false
        description nullable: true
        venue nullable: true
        expiryDate(validator: { val, obj ->
            val?.after(obj.startDate-1)
        })
        startDate(min: new Date()-1)
        notificationDate(min: new Date()-1)
    }

    /*
     * Methods of the Domain Class
     */

    @Override
    // Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${title}";
    }

    def hasUser(User user) {
        return !(userEvents.findAll { it.user == user }.isEmpty())
    }

    def getUsers() {
        return userEvents.collect { it.user }
    }

    static def findUserEvents(User user) {
        def tags = UserTag.findAllByUser(user)
        return createCriteria().list {
            eventTags {
                'in'("id", tags*.id)
            }
            ge 'expiryDate',new Date()
        }
    }

    EventTag getUserEventGroup(User user) {
        return eventTags.find { it.hasUser(user) }
    }
}
