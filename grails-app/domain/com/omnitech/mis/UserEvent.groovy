package com.omnitech.mis

/**
 * UserEvent
 * A domain class describes the data object and it's mapping to the database
 */
class UserEvent {

    String id
    Date	dateCreated
    Date	lastUpdated

	static	belongsTo	= [event:Event,user:User]	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static	hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static	mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
    }

    static	constraints = {
        event unique: 'user'
    }

    /*
     * Methods of the Domain Class
     */
    @Override	// Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${user}"
    }
}
