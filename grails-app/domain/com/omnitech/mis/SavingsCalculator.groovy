package com.omnitech.mis

/**
 * SavingsCalculator
 * A domain class describes the data object and it's mapping to the database
 */
class SavingsCalculator {

    String id
    String title
    String notes
    Date	dateCreated
    Date	lastUpdated

//	static	belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
	static	hasMany		= [variables:CalculatorVariable,products:Product]	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static	mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
    }

    static	constraints = {
        title nullable: false
        notes nullable: true
    }

    /*
     * Methods of the Domain Class
     */
    @Override	// Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${title}";
    }
}
