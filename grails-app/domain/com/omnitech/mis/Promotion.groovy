package com.omnitech.mis

import org.grails.databinding.BindingFormat

/**
 * Promotion
 * A domain class describes the data object and it's mapping to the database
 */
class Promotion {

    String id
    String promotionName
    Date dateCreated
    Date lastUpdated
    @BindingFormat('yyyy-MM-dd')
    Date startDate
    @BindingFormat('yyyy-MM-dd')
    Date endDate
    String description

    static belongsTo = [product: Product]
    // tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static	hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
    static hasMany = [userPromotionGroups: UserPromotionGroup]
    // tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static	mappedBy	= []	// specifies which property should be used in a mapping

    static mapping = {
        id(generator: "com.omnitech.mis.utils.MyIdGenerator", type: "string", length: 32)
    }

    static constraints = {
        promotionName nullable: false
        description nullable: true
        endDate(validator: { val, obj ->
            val?.after(obj.startDate-1)
        })
        startDate(min: new Date()-1)
    }

    /*
     * Methods of the Domain Class
     */

    @Override
    // Override toString for a nicer / more descriptive UI
    public String toString() {
        return "${promotionName}"
    }

    UserPromotionGroup getUserPromotionGroup(User user) {
        return userPromotionGroups.find { it.hasUser(user) }
    }
}
